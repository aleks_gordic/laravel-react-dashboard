<?php

return [

    's3' => [
        'enable'    => true,
        'version'   => 'latest',
        'bucket'    => env('MEDIA_BUCKET'),
        'region'    => 'ap-southeast-2',
    ],

];
