<?php

return [
    'isEnabled' => !empty(env('CENTRIX_LOGIN')) && !empty(env('CENTRIX_PASSW')) && !empty(env('CENTRIX_USERID')) && !empty(env('CENTRIX_USERKEY')),
    'auth'        => [
        'login'    => env('CENTRIX_LOGIN', ''),
        'password' => env('CENTRIX_PASSW', ''),
    ],
    'credentials' => [
        'SubscriberID' => env('CENTRIX_SUBID', ''),
        'UserID'       => env('CENTRIX_USERID', ''),
        'UserKey'      => env('CENTRIX_USERKEY', ''),
    ],
    'location'    => env('CENTRIX_LOCATION', 'https://ws.centrix.co.nz/v16/Consumers.svc'),
    'WSDL'        => env('CENTRIX_WSDL', 'https://ws.centrix.co.nz/v16/Consumers.svc?singlewsdl'),
    'action'      => 'http://centrix.co.nz/IConsumers/GetCreditReportProducts',

    'centrixId'   => env('SEND_CENTRIX_ID', false),
    'subCode'     => env('CENTRIX_SUBCODE', null),
    'countries'   => ['AU', 'NZ']
];