<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Http\Models\ApiSessions\ApiSessions;
use App\Http\Models\Transactions\Seen;
use App\Http\Models\Transactions\Transactions;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateOldDataNZ extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data-nz';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command migrate old data from NZ DB into new Portal 3.1 DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->transactionsNsmstoken();
        $this->apiSessions();
    }

    protected function transactionsNsmstoken(){

        $records = self::tableName('transactions')
            ->get();

        foreach ($records as $record){
            $tr = new Transactions();
            $tr->category = 'remote-verification';
            $tr->module = 'identity-flow-self-guided';
            $tr->token = $record->token;
            $tr->transactionId = Helper::generateTransactionId();
            $tr->original_token = $record->original_token;
            $tr->reference = $record->reference;
            $tr->subcode = $record->subcode;
            $tr->username = 'NA';

            $tr->identify_type = $record->identify_type;

            $tr->flow_type = $record->flow_type;
            $tr->instruction_type = 'SELF_GUIDED';
            $tr->country = $record->country;
            $tr->confidence = $record->confidence;
            $tr->centrix = $record->centrix;
            $tr->joint_session = 0;
            $tr->message_template = 'RETAIL';
            $tr->user_status = $record->status === 'COMPLETED' ? 'completed' : 'pending';

            $tr->status = $record->status;

            $tr->activated_at = $record->activated_at;
            $tr->seen_at = null;
            $tr->completed_at = $record->status === 'COMPLETED' ? $record->updated_at : null;
            $tr->created_at = $record->created_at;
            $tr->updated_at = $record->updated_at;

            $tr->save();
            //----------
            $seens = new Seen();
            $seens->tId = $tr->id;
            $seens->user_id = 2;
            $seens->save();
            //----------
            $cards = self::tableName('transaction_cards')->where('tId',$record->id)->first();
            if($cards){
                $cards_input = [
                    'session' => $cards->session,
                    'front_card' => $cards->front_card,
                    'back_card' => $cards->back_card,
                    'face_photo' => $cards->face_photo,
                    'asf' => $cards->asf,
                    'edited' => null,
                    'approved' => $record->status === 'COMPLETED',
                    'created_at' => $cards->created_at,
                    'updated_at' => $cards->updated_at,
                ];
                $tr->cards()->create($cards_input);
            }
            //-----
            $contact = self::tableName('transaction_contact_details')->where('tId',$record->id)->first();
            if($contact){
                $contact_inputs = [
                    'name'                 => null,
                    'phone'                => $contact->phone,
                    'email'                => null,
                    'residential_address'  => $contact->residential_address,
                    'previous_address'     => $contact->previous_address,
                    'created_at'           => $contact->created_at,
                    'updated_at'           => $contact->updated_at,
                ];

                $tr->contactDetails()->create($contact_inputs);
            }
            //----------------
            $userDetails = self::tableName('transaction_user_details')->where('tId',$record->id)->first();
            if($userDetails){
                $userDetail_inputs = [
                    'firstName'        => $userDetails->firstName,
                    'lastName'         => $userDetails->lastName,
                    'middleName'       => $userDetails->middleName,
                    'dateOfBirth'      => $userDetails->dateOfBirth,
                    'dateOfExpiry'     => $userDetails->dateOfExpiry,
                    'licenceNumber'    => $userDetails->licenceNumber,
                    'versionNumber'    => $userDetails->versionNumber,
                    'passportNumber'   => $userDetails->passportNumber,
                    'created_at'       => $userDetails->created_at,
                    'updated_at'       => $userDetails->updated_at,
                ];

                $tr->userDetails()->create($userDetail_inputs);
            }
            //-------------------------
            $info = self::tableName('transaction_info')->where('tId',$record->id)->first();
            if($info){
                $info_inputs = [
                    'os' => $info->os,
                    'device' => $info->device,
                    'browser' => $info->browser,
                    'isp' => $info->isp,
                    'ip' => $info->ip,
                    'ip_location' => $info->ip_location,
                    'geo_location' => $info->geolocation,
                    'vpn' => $info->vpn,
                    'created_at' => $info->created_at,
                    'updated_at' => $info->updated_at,
                ];
                $tr->info()->create($info_inputs);
            }
            //--------------------------
            //$statistics = self::tableName('transaction_statistics')->where('tId',$record->id)->first();
            $statistics_inputs = [
                'review_terms' => null,
                'capture_id' => null,
                'review_data' => null,
                'liveness' => null,
                'created_at' => null,
                'updated_at' => null,
            ];

            $tr->statistics()->create($statistics_inputs);

            //--------------------------
            $faceScan = self::tableName('transaction_face_scan')->orderBy('created_at', 'DESC')->where('tId',$record->id)->first();
            if($faceScan){
                $faceScan_inputs = [
                    'session'              => $faceScan->session,
                    'confidence'           => $faceScan->confidence,
                    'liveness'             => $faceScan->liveness,
                    'features'             => $faceScan->features,
                    'liveness_assessments' => null,
                    'video'                => $faceScan->video,
                    'face'                 => $faceScan->face,
                    'created_at'           => $faceScan->created_at,
                    'updated_at'           => $faceScan->updated_at,
                ];

                $tr->faceScan()->create($faceScan_inputs);
            }
            //-------------------
            $livenes_counts = self::tableName('transaction_liveness')->orderBy('created_at', 'DESC')->where('tId',$record->id)->get();
            if($livenes_counts && !empty($livenes_counts)){
                foreach ($livenes_counts as $livenes) {
                    $tr->liveness()->create([
                        'token'       => $livenes->token,
                        'result'      => $livenes->result,
                        'spoof_risk'  => $livenes->spoof_risk,
                        'actions'     => $livenes->actions,
                        'video'       => $livenes->video,
                        'created_at'  => $livenes->created_at,
                        'updated_at'  => $livenes->updated_at
                    ]);
                }
            }
            //-------------------
            $url = self::tableName('transaction_urls')->orderBy('created_at', 'DESC')->where('tId',$record->id)->first();
            if($url){
                $url_inputs = [
                    'return_url' => $url->return_url,
                    'cancel_url' => $url->cancel_url,
                    'expiry'     => now(),
                    'created_at' => $url->created_at,
                    'updated_at' => $url->updated_at,
                ];
                $tr->urls()->create($url_inputs);
            }
            //----------
            $centrixSmartId = self::tableName('transaction_centrix_smart_id')->orderBy('created_at', 'DESC')->where('tId',$record->id)->first();
            if($centrixSmartId){

                $tr->centrixSmartId()->create([
                    'IsVerified'            => $centrixSmartId->IsVerified,
                    'IsNameVerified'        => $centrixSmartId->IsNameVerified,
                    'IsDateOfBirthVerified' => $centrixSmartId->IsDateOfBirthVerified,
                    'IsAddressVerified'     => $centrixSmartId->IsAddressVerified,
                    'created_at'            => $centrixSmartId->created_at,
                    'updated_at'            => $centrixSmartId->updated_at,
                ]);

                $smartDatas = self::tableName('transaction_centrix_smart_id_data')->where('sId',$centrixSmartId->id)->get();
                if($smartDatas && !empty($smartDatas)){
                    foreach ($smartDatas as $smartData) {
                        DB::table('transaction_centrix_smart_id_data')->insert([
                            'sId'                    => $tr->centrixSmartId->id,
                            'DataSourceName'         => $smartData->DataSourceName,
                            'DataSourceDescription'  => $smartData->DataSourceDescription,
                            'NameMatchStatus'        => $smartData->NameMatchStatus,
                            'DateOfBirthMatchStatus' => $smartData->DateOfBirthMatchStatus,
                            'AddressMatchStatus'     => $smartData->AddressMatchStatus,
                            'created_at'             => $smartData->created_at,
                            'updated_at'             => $smartData->updated_at,
                        ]);
                    }

                }
            }
            //----------
            $centrixLicence = self::tableName('transaction_centrix_licence')->orderBy('created_at', 'DESC')->where('tId',$record->id)->first();
            if($centrixLicence){
                $tr->centrixLicence()->create([
                    'NumberAndVersion'                 => $centrixLicence->NumberAndVersion,
                    'IsDriverLicenceVerified'          => $centrixLicence->IsDriverLicenceVerified,
                    'IsDriverLicenceLastNameMatched'   => $centrixLicence->IsDriverLicenceLastNameMatched,
                    'IsDriverLicenceFirstNameMatched'  => $centrixLicence->IsDriverLicenceFirstNameMatched,
                    'IsDriverLicenceMiddleNameMatched' => $centrixLicence->IsDriverLicenceMiddleNameMatched,
                    'IsDriverLicenceDateOfBirthMatched'=> $centrixLicence->IsDriverLicenceDateOfBirthMatched,
                    'IsSuccess'                        => $centrixLicence->IsSuccess,
                    'IsDriverLicenceVerifiedAndMatched'=> $centrixLicence->IsDriverLicenceVerifiedAndMatched,
                    'created_at'                       => $centrixLicence->created_at,
                    'updated_at'                       => $centrixLicence->updated_at,
                ]);
            }
            //--------
            $centrixPassport = self::tableName('transaction_centrix_passport')->orderBy('created_at', 'DESC')->where('tId',$record->id)->first();
            if($centrixPassport){
                $tr->centrixPassport()->create([
                    'IsPassportVerified' => $centrixPassport->IsPassportVerified,
                    'IsSuccess'          => $centrixPassport->IsSuccess,
                    'created_at'         => $centrixPassport->created_at,
                    'updated_at'         => $centrixPassport->updated_at,
                ]);
            }
            //---------
            $centrixPep = self::tableName('transaction_centrix_pep')->orderBy('created_at', 'DESC')->where('tId',$record->id)->first();
            if($centrixPep){
                $tr->centrixPep()->create([
                    'IsSuccess'                     => $centrixPep->IsSuccess,
                    'InternationalWatchlistIsClear' => $centrixPep->InternationalWatchlistIsClear,
                    'ResponseDetails'               => $centrixSmartId->ResponseDetails ?? '{}',
                    'created_at'                    => $centrixPep->created_at,
                    'updated_at'                    => $centrixPep->updated_at,
                ]);
            }
            //--------
            $centrixResidential = self::tableName('transaction_centrix_residential')->orderBy('created_at', 'DESC')->where('tId',$record->id)->first();
            if($centrixResidential){
                $tr->centrixResidential()->create([
                    'IsSuccess'                         => $centrixResidential->IsSuccess,
                    'Message'                           => $centrixResidential->Message,
                    'ResidentialVerified'               => $centrixResidential->ResidentialVerified,
                    'ThirdPartyMatchType'               => $centrixResidential->ThirdPartyMatchType,
                    'ThirdPartyMatchTypeDescription'    => $centrixResidential->ThirdPartyMatchTypeDescription,
                    'Surname'                           => $centrixResidential->Surname,
                    'FirstName'                         => $centrixResidential->FirstName,
                    'MiddleName'                        => $centrixResidential->MiddleName,
                    'DateOfBirth'                       => $centrixResidential->DateOfBirth,
                    'ThirdPartyNameMatch'               => $centrixResidential->ThirdPartyNameMatch,
                    'ThirdPartyNameMatchDescription'    => $centrixResidential->ThirdPartyNameMatchDescription,
                    'SurnameMatch'                      => $centrixResidential->SurnameMatch,
                    'FirstNameMatch'                    => $centrixResidential->FirstNameMatch,
                    'FirstNameInitialMatch'             => $centrixResidential->FirstNameInitialMatch,
                    'MiddleNameMatch'                   => $centrixResidential->MiddleNameMatch,
                    'MiddleNameInitialMatch'            => $centrixResidential->MiddleNameInitialMatch,
                    'DateOfBirthMatch'                  => $centrixResidential->DateOfBirthMatch,
                    'ThirdPartyAddressMatch'            => $centrixResidential->ThirdPartyAddressMatch,
                    'ThirdPartyAddressMatchDescription' => $centrixResidential->ThirdPartyAddressMatchDescription,
                    'AddressLine1'                      => $centrixResidential->AddressLine1,
                    'Suburb'                            => $centrixResidential->Suburb,
                    'State'                             => $centrixResidential->State,
                    'Postcode'                          => $centrixResidential->Postcode,
                    'DeliveryPointID'                   => $centrixResidential->DeliveryPointID,
                    'UnitNoMatch'                       => $centrixResidential->UnitNoMatch,
                    'StreetNoMatch'                     => $centrixResidential->StreetNoMatch,
                    'StreetNameMatch'                   => $centrixResidential->StreetNameMatch,
                    'StreetTypeMatch'                   => $centrixResidential->StreetTypeMatch,
                    'SuburbMatch'                       => $centrixResidential->SuburbMatch,
                    'StateMatch'                        => $centrixResidential->StateMatch,
                    'PostcodeMatch'                     => $centrixResidential->PostcodeMatch,
                    'HomePhoneNo'                       => $centrixResidential->HomePhoneNo,
                    'MobilePhoneNo'                     => $centrixResidential->MobilePhoneNo,
                    'PhoneNumberMatch'                  => $centrixResidential->PhoneNumberMatch,
                    'MobileNumberMatch'                 => $centrixResidential->MobileNumberMatch,
                    'EmailAddress'                      => $centrixResidential->EmailAddress,
                    'created_at'                        => $centrixResidential->created_at,
                    'updated_at'                        => $centrixResidential->updated_at,
                ]);
            }

            if($tr->status == 'COMPLETED' && (!$tr->cards || !$tr->contactDetails || !$tr->userDetails)){
                $tr->status = 'PENDING';
                $tr->save();
            }
        }

        $this->info("transactions - Stored");
    }

    protected function apiSessions(){
        $records = self::tableName('api_sessions')->get();

        //Delete existing data

        //
        foreach ($records as $record){

            $files = [];
            $apiSessions = new ApiSessions();
            $apiSessions->tId = 1;
            $apiSessions->type = $record->type;
            $apiSessions->token = $record->token;
            $apiSessions->session_id = $record->session_id;
            $apiSessions->host = 'https://api.idkit.io';
            $apiSessions->files = !(empty($files)) ? json_encode($files) : null;
            $apiSessions->created_at = $record->created_at;
            $apiSessions->updated_at = $record->updated_at;

            $apiSessions->save();
        }

        $this->info("api_sessions - Stored");
    }
    //Helpers
    protected static function tableName($table){
        return DB::connection('oldMysql')->table($table);
    }

    protected static function formatAsf($asf){
        if(empty($asf)) return null;
        $newAsf = [];

        if(Helper::isJson($asf)){
            $asf = json_decode($asf, true);
        }

        foreach ($asf as $card) {
            if(isset($card['Key'])){
                $key                 = preg_replace('/\s/i', '', lcfirst($card['Key']));
                $value               = $card['Value'];
                $newAsf[$key] = $value == 'Pass' ? true : false;
            }
        }

        return json_encode($newAsf);
    }

    protected static function getFileName($type, $fullurl){
        $host = config('orbitsdk.api.url');

        $url = $host . config('orbitsdk.api.images_path');

        if ($type !== 'VID') {
            $url .= $type !== 'PASSPORT' ? 'ORBIT_ID_ANZ' : 'ORBIT_MRZ';
            $url .= '/' . $type . '/' . config('orbitsdk.api.token') . '/';
        } else {
            $url .= 'ORBIT_FR/FR/' . config('orbitsdk.api.token') . '/';
        }

        //return str_replace($url, '', $fullurl);
        return $fullurl;
    }

    protected static function location($ip)
    {
        $file = file_get_contents('http://api.ipstack.com/' . $ip . '?access_key=0a3ac3f97b1b2fcb1a25d10e4dca4306&format=1');
        $json = json_decode($file, true);

        if(empty($json['latitude']) || empty($json['longitude'])) return null;

        $ip_location = $json['latitude'].','.$json['longitude'];

        $ipLoc = (array)self::reverseGeocode($ip_location)->results;

        if(!empty($ipLoc)){
            $ipLoc = $ipLoc[count($ipLoc)-3]->formatted_address;
            $res = json_encode([
                'loc' => $ipLoc,
                'coordinates' => $ip_location,
            ]);

        }else{
            $res = json_encode([
                'coordinates' =>$ip_location,
            ]);
        }


        return $res;
    }

    public static function reverseGeocode($cordinates){
        if(Helper::validateLoc($cordinates)){
            $file = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyC_4BJLpINXrnzuAS7QJiyK7199U3Bm4W0&latlng='.$cordinates.'&sensor=true');
            $json = json_decode($file);
            //return response()->json($json);
            return $json;
        }else{
            return [
                'results' => []
            ];
        }

    }
}
