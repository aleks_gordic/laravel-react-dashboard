<?php

namespace App\Console\Commands;

use App\Http\Models\Transactions\Transactions;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class exportId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exportId {tId*}' ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for exporting all the raw ids';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO: change the path of images
        $ids = $this->argument('tId');

        if(in_array('all',$ids)){
            $msg = 'exporting all the Ids';
            $transactions = Transactions::whereHas('cards', function ($query) {

                $query->where('front_card', '!=', null)->orWhere('front_card', '!=', '');

            })->with('clientId:tId,front_card,back_card')->get();
            $this->info($msg);
        }else{
            $msg = 'exporting the files for following ids';
            $this->info($msg);
            //$this->info(implode(',',$ids));
            $transactions = Transactions::whereIn('transactionId', $ids)->with('clientId:tId,front_card,back_card')->get();
        }

        try {

            foreach ($transactions as $transaction){
                $clientId = $transaction['cards'];
                $front = str_replace('/crop/','/',$clientId['front_card']);
                $back = str_replace('/crop/','/',$clientId['back_card']);

                $this->copyToLocal($transaction->transactionId, $front, true);
                if(!empty($back)){
                    $this->copyToLocal($transaction->transactionId, $back);
                }


                $this->info('copied Id with transaction '.$transaction->transactionId);
            }

            $this->info('Preparing for zipping...');
            $this->zipFiles();
            $this->info('Please open');
            $this->info(route('export'));

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * Copy file to local public disk
     * @param $transactionId
     * @param $url
     * @param $isFront
     * @return string
     */
    protected function copyToLocal($transactionId, $url, $isFront = false) {

        try {

            $content = file_get_contents($url);

            $path = parse_url($url, PHP_URL_PATH);
            $prefix = ($isFront) ? 'front' : 'back';
            $filename = 'ids/'.$transactionId.'_'.$prefix.'_'.basename($path);
            Storage::disk('export')->put($filename, $content);

            $url = Storage::disk('export')->url($filename);

        } catch (\Exception $e) {
            /*$response['status'] = 'error';
            $response['message'] = $e->getMessage();*/
            $url = '';
        }

        return $url;
    }

    /**
     * Zip the public/export/ids
     *
     * @return mixed
     */
    protected function zipFiles(){

        try {

            Storage::disk('export')->delete('download.zip');

            // create a list of files that should be added to the archive.
            $files = Storage::disk('export')->files('ids');
            // define the name of the archive and create a new ZipArchive instance.
            $archiveFile = public_path() . '/export/download.zip';
            $archive = new ZipArchive();

            // check if the archive could be created.
            if ($archive->open($archiveFile, ZipArchive::CREATE )) {
                // loop through all the files and add them to the archive.
                foreach ($files as $file) {
                    $archive->addFile(public_path().'/export/'.$file, basename($file));
                }

                $archive->close();

                Storage::disk('export')->deleteDirectory('ids');


            } else {
                throw new Exception("zip file could not be created: " . $archive->getStatusString());
            }

        } catch (\Exception $e) {

        }


    }

}
