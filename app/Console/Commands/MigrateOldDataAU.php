<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\Http\Models\ApiSessions\ApiSessions;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Transactions\Seen;
use App\Http\Models\Transactions\Transactions;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class MigrateOldDataAU extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data-au';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command migrate old data from AU DB into new Portal 3.1 DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->transactionsNsmstoken();
        $this->apiSessions();
    }

    protected function transactionsNsmstoken(){

        $records = self::tableName('transactions')
            ->join('smstoken', 'transactions.id', '=', 'smstoken.tId')
            ->select(
                'transactions.*',
                'smstoken.number',
                'smstoken.reference',
                'smstoken.status as smsStatus',
                'smstoken.expiry')
            ->get();

        foreach ($records as $record){
            $tr = new Transactions();
            $tr->category = 'remote-verification';
            $tr->module = 'identity-flow-self-guided';
            $tr->token = Helper::generateToken();
            $tr->transactionId = $record->transactionId;
            $tr->original_token = $record->session;
            $tr->reference = $record->reference;
            $tr->subcode = null;
            $tr->username = 'NA';

            $tr->identify_type = (strpos($record->captureType, 'LICENCE') !== false) ? 'AUS_AUTO_DRIVERLICENCE' : 'PASSPORT';

            $tr->flow_type = ($record->captureType == 'selfie') ? 'ALTERNATE' : 'MAIN';
            $tr->instruction_type = 'SELF_GUIDED';
            $tr->country = 'AU';
            $tr->confidence = 0;
            $tr->centrix = 0;
            $tr->joint_session = 0;
            $tr->message_template = 'RETAIL';
            $tr->user_status = $record->smsStatus;

            if($record->status == 'TERMINATED'){
                $tr->status = 'CANCELLED';
            }elseif($record->status == 'INITIATED'){
                $tr->status = 'INPROGRESS';
            }else{
                $tr->status = $record->status;
            }

            $tr->activated_at = null;
            $tr->seen_at = null;
            $tr->completed_at = $record->completed_at;
            $tr->created_at = $record->created_at;
            $tr->updated_at = $record->updated_at;

            $tr->save();
            //----------
            $seens = new Seen();
            $seens->tId = $tr->id;
            $seens->user_id = 2;
            $seens->save();
            //----------
            $contact_inputs = [
                'phone' => str_replace('+', '', $record->number) ?? null,
            ];
            //----------
            $clientId = self::tableName('transaction_client_id')->where('tId',$record->id)->first();
            $cards_input = [
                'session' => null,
                'front_card' => null,
                'back_card' => null,
                'face_photo' => null,
                'asf' => null,
                'edited' => null,
                'approved' => 1,
                'created_at' => null,
                'updated_at' => null,
            ];


            if(!empty($clientId)){

                $front = $clientId->front;
                $back  = $clientId->back;

                $type = (strpos($front, 'AUS_AUTO_DRIVERLICENCE') !== false) ? 'AUS_AUTO_DRIVERLICENCE' : 'PASSPORT';

                $cards_input['front_card'] = !empty($front) ? self::getFileName($type, $front) : null;
                $cards_input['back_card']  = !empty($back) ? self::getFileName($type, $back) : null;
                $cards_input['asf']        = self::formatAsf($clientId->AsfCheck);
                $cards_input['created_at'] = $clientId->created_at;
                $cards_input['updated_at'] = $clientId->updated_at;
                if(!empty($front)){
                    $sessionParts = explode('/', $cards_input['front_card']);
                    $cards_input['session'] = $sessionParts[0];
                }
            }
            //----------------
            $personalDetails = self::tableName('transaction_personal_details')->where('tId',$record->id)->first();

            if(!empty($personalDetails)){
                $photo = $personalDetails->photo;

                if(!empty($photo)){
                    $type = (strpos($photo, 'AUS_AUTO_DRIVERLICENCE') !== false) ? 'AUS_AUTO_DRIVERLICENCE' : 'PASSPORT';
                    $cards_input['face_photo'] = self::getFileName($type, $photo);
                }
                $contact_inputs['phone'] = str_replace('+', '',  $personalDetails->mobile);
                $contact_inputs['residential_address'] = str_replace('+', '',  $personalDetails->address);
                $contact_inputs['created_at'] = $personalDetails->created_at;
                $contact_inputs['updated_at'] = $personalDetails->updated_at;

                //---
                $userDetail_inputs = [
                    'firstName'        => $personalDetails->firstName ?? 'NA',
                    'lastName'         => $personalDetails->lastName ?? 'NA',
                    'middleName'       => $personalDetails->middleName,
                    'dateOfBirth'      => $personalDetails->dateOfBirth,
                    'dateOfExpiry'     => $personalDetails->dateOfExpiry,
                    'licenceNumber'    => $personalDetails->licenceNumber,
                    'versionNumber'    => $personalDetails->licenceVersion,
                    'passportNumber'   => $personalDetails->passportNumber,
                    'created_at'       => $personalDetails->created_at,
                    'updated_at'       => $personalDetails->updated_at,
                ];

                $tr->userDetails()->create($userDetail_inputs);

            }

            $tr->cards()->create($cards_input);

            //-----------------------
            $info = self::tableName('transaction_info')->where('tId',$record->id)->first();

            $info_inputs = [
                'os' => null,
                'device' => null,
                'browser' => null,
                'isp' => null,
                'ip' => null,
                'ip_location' => null,
                'geo_location' => null,
                'vpn' => null,
                'created_at' => null,
                'updated_at' => null,
            ];

            if(!empty($info)){
                $devise = explode(',', $info->devise);
                $info_inputs['os'] = $devise[1];
                $info_inputs['device'] = ($devise[0] == 'iOS') ? 'iPhone' : 'unknown';
                $info_inputs['browser'] = $info->browser;
                $info_inputs['isp'] = $info->network;
                $info_inputs['ip'] = $info->ipAddress;
                //$info_inputs['ip_location'] = self::location($info->ipAddress);
                //$info_inputs['geo_location'] = $info->ipAddress;
                $info_inputs['vpn'] = $info->vpn;
                $info_inputs['created_at'] = $info->created_at;
                $info_inputs['updated_at'] = $info->updated_at;

                $tr->info()->create($info_inputs);
            }

            //--------------------------
            $statistics = self::tableName('transaction_statistics')->where('tId',$record->id)->first();
            $statistics_inputs = [
                'review_terms' => null,
                'capture_id' => null,
                'review_data' => null,
                'liveness' => null,
                'created_at' => null,
                'updated_at' => null,
            ];

            if(!empty($statistics)){
                $statistics_inputs['review_terms'] = $statistics->review_terms_ms;
                $statistics_inputs['capture_id'] = $statistics->capture_id_ms;
                $statistics_inputs['review_data'] = $statistics->review_data_ms;
                $statistics_inputs['liveness'] = $statistics->liveness_ms;
                $statistics_inputs['created_at'] = $statistics->created_at;
                $statistics_inputs['updated_at'] = $statistics->updated_at;

                $tr->statistics()->create($statistics_inputs);
            }
            //--------------------------
            $faceScans = self::tableName('transaction_face_scan_data')->orderBy('created_at', 'DESC')->where('tId',$record->id)->get()->toArray();

            $faceScan_inputs = [
                'session' => null,
                'confidence' => 0,
                'liveness' => 0,
                'features' => null,
                'liveness_assessments' => null,
                'video' => null,
                'face' => null,
                'created_at' => null,
                'updated_at' => null,
            ];

            if(!empty($faceScans)){

                $fr = $faceScans[0];
                $video = $fr->video;

                if(Helper::isJson($video)){

                    $obj = json_decode($video, true);
                    $faceScan_inputs['video'] = self::getFileName('VID', str_replace('//IMG','/IMG', $obj['smile']));
                    $faceScan_inputs['face']  = self::getFileName('VID', str_replace('//IMG','/IMG', $obj['turnHead']));

                }else{

                    $video = str_replace('https://api.idkit.io/','../../', $fr->video);
                    $faceScan_inputs['video'] = Utils::getFileName('VID', $video);
                    $faceScan_inputs['face'] = str_replace('B.webm', 'face.jpg', $faceScan_inputs['video']);

                }

                $sessionParts = explode('/', $faceScan_inputs['face']);
                $faceScan_inputs['session'] = $sessionParts[0];

                $faceScan_inputs['confidence'] = $fr->confidence == 'Pass';
                $faceScan_inputs['liveness']   = $fr->liveness;
                $faceScan_inputs['features']   = $fr->features;
                $faceScan_inputs['created_at'] = $fr->created_at;
                $faceScan_inputs['updated_at'] = $fr->updated_at;

                $tr->faceScan()->create($faceScan_inputs);

                //-----Insert into liveness
                foreach ($faceScans as $faceScan){
                    if(!Helper::isJson($faceScan->video)){
                        $liveness_inputs = [
                            'token' => null,
                            'result' => 0,
                            'spoof_risk' => 1,
                            'actions' => '',
                            'video' => '',
                        ];

                        $actions = ['Smile, Turn head right', 'Smile, Turn head left'];

                        $video = str_replace('https://api.idkit.io/','../../', $faceScan->video);
                        $liveness_inputs['video'] = Utils::getFileName('VID', $video);

                        $sessionParts = explode('/', $liveness_inputs['video']);
                        $liveness_inputs['token'] = md5(sha1(bcrypt($sessionParts[0])));
                        $liveness_inputs['actions'] = Arr::random($actions);
                        $liveness_inputs['result'] = $faceScan->liveness;
                        $liveness_inputs['spoof_risk'] = !$faceScan->liveness;
                        $tr->liveness()->create($liveness_inputs);
                    }
                }
            }
            //--------------------------

            $url_inputs = [
                'return_url' => route('mobileStatus', [ 'status' => 'completed']),
                'cancel_url' => route('mobileStatus', [ 'status' => 'cancelled']),
                'expiry'     => $record->expiry,
                'created_at' => $tr->created_at,
                'updated_at' => $tr->updated_at,
            ];
            $tr->urls()->create($url_inputs);
            //----------
            $tr->contactDetails()->create($contact_inputs);

        }

        $this->info("transactions - Stored");
    }
    protected function apiSessions(){
        $records = self::tableName('api_sessions')->get();

        //Delete existing data

        //
        foreach ($records as $record){

            $files = [];
            if($record->filepath) {
                $files[] = str_replace('../../', '', $record->filepath);
            }
            if($record->video) {
                $files[] = str_replace('../../', '', $record->video);
            }

            $apiSessions = new ApiSessions();
            $apiSessions->tId = 1;
            $apiSessions->type = $record->type;
            $apiSessions->token = $record->token;
            $apiSessions->session_id = $record->session_id;
            $apiSessions->host = 'https://api.idkit.io';
            $apiSessions->files = !(empty($files)) ? json_encode($files) : null;
            $apiSessions->created_at = $record->created_at;
            $apiSessions->updated_at = $record->updated_at;

            $apiSessions->save();
        }

        $this->info("api_sessions - Stored");
    }


    //Helpers
    protected static function tableName($table){
        return DB::connection('oldMysql')->table($table);
    }

    protected static function formatAsf($asf){
        if(empty($asf)) return null;
        $newAsf = [];

        if(Helper::isJson($asf)){
            $asf = json_decode($asf, true);
        }

        foreach ($asf as $card) {
            if(isset($card['Key'])){
                $key                 = preg_replace('/\s/i', '', lcfirst($card['Key']));
                $value               = $card['Value'];
                $newAsf[$key] = $value == 'Pass' ? true : false;
            }
        }

        return json_encode($newAsf);
    }

    protected static function getFileName($type, $fullurl){
        $host = config('orbitsdk.api.url');

        $url = $host . config('orbitsdk.api.images_path');

        if ($type !== 'VID') {
            $url .= $type !== 'PASSPORT' ? 'ORBIT_ID_ANZ' : 'ORBIT_MRZ';
            $url .= '/' . $type . '/' . config('orbitsdk.api.token') . '/';
        } else {
            $url .= 'ORBIT_FR/FR/' . config('orbitsdk.api.token') . '/';
        }

        //return str_replace($url, '', $fullurl);
        return $fullurl;
    }

    protected static function location($ip)
    {
        $file = file_get_contents('http://api.ipstack.com/' . $ip . '?access_key=0a3ac3f97b1b2fcb1a25d10e4dca4306&format=1');
        $json = json_decode($file, true);

        if(empty($json['latitude']) || empty($json['longitude'])) return null;

        $ip_location = $json['latitude'].','.$json['longitude'];

        $ipLoc = (array)self::reverseGeocode($ip_location)->results;

        if(!empty($ipLoc)){
            $ipLoc = $ipLoc[count($ipLoc)-3]->formatted_address;
            $res = json_encode([
                'loc' => $ipLoc,
                'coordinates' => $ip_location,
            ]);

        }else{
            $res = json_encode([
                'coordinates' =>$ip_location,
            ]);
        }


        return $res;
    }

    public static function reverseGeocode($cordinates){
        if(Helper::validateLoc($cordinates)){
            $file = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyC_4BJLpINXrnzuAS7QJiyK7199U3Bm4W0&latlng='.$cordinates.'&sensor=true');
            $json = json_decode($file);
            //return response()->json($json);
            return $json;
        }else{
            return [
                'results' => []
            ];
        }

    }
}
