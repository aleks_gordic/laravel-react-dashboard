<?php

namespace App\Console\Commands;

use App\Http\Models\Transactions\Transactions;
use DB;
use Illuminate\Console\Command;

class TimeoutJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:timeout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check transaction timeout';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = config('app.timeout');
        if ($time) {
            $tr = Transactions::where('status', 'INPROGRESS')
                ->where('created_at', '<=', DB::raw('DATE_SUB(NOW(), INTERVAL ' . $time . ' MINUTE)'));
            $count = $tr->count();
            $tr->first()->forceDelete();

            $this->info($count . ' updateded');
        }
    }
}
