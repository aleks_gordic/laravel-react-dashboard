<?php

namespace App\Console\Commands;

use App\Http\Models\Portal\Core\Category;
use App\Http\Models\Portal\Core\Module;
use App\Http\Models\Portal\Core\Permission;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class seedsForNewUserManagement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'role-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeds db for new Role Management';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //clean
        Permission::truncate();
        $mods = ['rate','delete-records','view-passed-records','export-report','back-forward-buttons'];
        Module::whereIn('slug',$mods)->delete();

        $cats = ['business-livestock','motor-direct-intermediary'];
        foreach ($cats as $cat){
            $category = Category::where('slug', $cat)->first();
            Module::where('category_id',$category->id)->delete();
        }
        Category::whereIn('slug',$cats)->delete();

        //$users = User::all();

        //Seeding
        User::where('username','ocradmin')->update([
            'role' => 'admin'
        ]);
        User::where('username','!=','ocradmin')->update([
            'role' => 'manager'
        ]);
        $management = Category::where('slug','management')->first();
        DB::table('modules')->insert([
            'title'    => 'Role Management',
            'category_id'    => $management->id,
            'slug'    => 'role',
            'active'     => true,
        ]);
        //----Permissions only modules--
        // Sub modules -> Pending transaction
        $pending = Module::where('slug', 'pending-transactions')->first();
        DB::table('modules')->insert([
            'title'    => 'Delete Records',
            'slug'     => 'delete-records',
            'parent_id' => $pending->id,
            'active'   => true,
        ]);
        DB::table('modules')->insert([
            'title'    => 'Export Records',
            'slug'     => 'export-records',
            'parent_id' => $pending->id,
            'active'   => true,
        ]);

        //Sub modules -> Customer Repository
        $customerRepo = Module::where('slug', 'customer-repository')->first();
        DB::table('modules')->insert([
            'title'    => 'Delete Records',
            'slug'     => 'delete-records',
            'parent_id' => $customerRepo->id,
            'active'   => true,
        ]);
        DB::table('modules')->insert([
            'title'    => 'Export Records',
            'slug'     => 'export-records',
            'parent_id' => $customerRepo->id,
            'active'   => true,
        ]);
        DB::table('modules')->insert([
            'title'    => 'View Passed Records',
            'slug'     => 'view-passed-records',
            'parent_id' => $customerRepo->id,
            'active'   => true,
        ]);
        DB::table('modules')->insert([
            'title'    => 'Back/Forward Buttons',
            'slug'     => 'back-forward-buttons',
            'parent_id' => $customerRepo->id,
            'active'   => true,
        ]);

        //--------------
        DB::table('roles')->insert([
            'name'    => 'Administrator',
            'slug'    => 'admin',
        ]);
        DB::table('roles')->insert([
            'name'    => 'Manager',
            'slug'    => 'manager',
        ]);
    }
}
