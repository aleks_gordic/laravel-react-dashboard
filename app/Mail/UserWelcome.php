<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserWelcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.notifications.transaction-status')
            ->with([
                'type' => 'cancelled',
                'columns' => [
                    'transactionId' => 'Transaction ID',
                    'reference'     => 'Reference',
                    'name'          => 'Name',
                    'contact'       => 'Contact',
                ],
                'data' => [
                    'transactionId' => 'trWdshH',
                    'reference'     => 'CRN000045',
                    'name'          => 'Xantosh',
                    'contact'       => '474000888',
                ],
                'flags' => [
                    'Face' => false,
                    'Live' => true,
                    'Doc'  => false,
                    'Data' => true,
                ],
            ]);
    }
}
