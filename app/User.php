<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Schema;
use Laravel\Passport\HasApiTokens;
use stdClass;
use Validator;
use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
        'dial_code',
        'mobile',
        'role',
        'avatar',
        'active',
        'mobile_verified_at',
        'last_signin_at'
    ];

    use HasApiTokens, Notifiable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Transactions seen by user
     * @return Illuminate\Database\Eloquent\Model
     */
    public function transactions()
    {
        return $this->belongsToMany('App\Http\Models\Transactions\Transactions', 'transaction_seens', 'user_id', 'tId');
    }


    /**
     * user role
     * @return Illuminate\Database\Eloquent\Model
     */
    public function role()
    {
        return $this->hasOne('App\Http\Models\Portal\Core\Role', 'role_id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
