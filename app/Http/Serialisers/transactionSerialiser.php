<?php
namespace App\Http\Serialisers;

use App\Http\Controllers\Portal\PortalController;
use App\Http\Models\Centrix\Centrix;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class TransactionSerialiser implements SerialiserInterface
{

    /*
    |--------------------------------------------------------------------------
    | TransactionSerialiser
    |--------------------------------------------------------------------------
    |
    | This class handles serializing transactions data for export. Export buttons are on admin portal.
    |
    */

    protected $type;
    /**
     * TransactionSerialiser constructor.
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * Here actual record of exported data is managed
     * @param $data
     * @return array
     */
    public function getData($data)
    {
        $row = [];

        $row[] = strval(date('d/m/Y, H:i', strtotime($data->created_at)));
        $row[] = $data->transactionId;
        $row[] = $data->reference ? $data->reference : 'NA';

        $row[] = $data->status ?? 'Pending';
        $row[] = $data->username ?? 'NA';//username

        $row[] = $data->identify_type;
        $row[] = $data->instruction_type;

        if($data->userDetails){
            $row[] = $data->userDetails->firstName? ucfirst(strtolower($data->userDetails->firstName)):null ?? 'NA';
            $row[] = $data->userDetails->lastName ? ucfirst(strtolower($data->userDetails->lastName)):null ?? 'NA';
            $row[] = $data->userDetails->dateOfBirth ?? 'NA';
        }else{
            $row[] = 'NA';
            $row[] = 'NA';
            $row[] = 'NA';
        }

        if(config('centrix.isEnabled')){

            if ($data->country == 'AU') {
                $overall = false;
                if ($data->centrixLicence) {
                    $overall = Centrix::boolean($data->centrixLicence->IsDriverLicenceVerified);
                } else if ($data->centrixPassport) {
                    $overall = Centrix::boolean($data->centrixPassport->IsPassportVerified);
                }
                $row[] = ($overall) ? 'Pass' : 'Fail';
            }else{
                if ($data->centrixSmartId){
                    $row[] = (Centrix::boolean($data->centrixSmartId->IsVerified)) ? 'Pass' : 'Fail';
                }else{
                    $row[] = 'NA';
                }
            }
        }else{
            $row[] = 'NA';
        }

        if($data->faceScan && $data->flow_type == 'MAIN'){
            $row[] = (Centrix::boolean($data->faceScan->confidence)) ? 'Pass' : 'Fail' ;
            $row[] = (Centrix::boolean($data->liveness[count($data->liveness) - 1]->result)) ? 'Pass' : 'Fail';
        }elseif($data->faceScan && $data->flow_type != 'MAIN'){
            $row[] = (Centrix::boolean($data->faceScan->confidence)) ? 'Pass' : 'Fail' ;
            $row[] = 'Fail';
        }else{
            $row[] = 'NA';
            $row[] = 'NA';
        }


        if ($data->cards){
            $asf = json_decode($data->cards->asf, true);
            $row[] = (isset($asf['overall']) && $asf['overall']) ? 'Pass' : 'Fail';
        }else{
            $row[] = 'NA';
        }

        //mobile & email
        if($data->contactDetails){
            $row[] = $data->contactDetails->phone ?? 'NA';
            $row[] = $data->contactDetails->email ?? 'NA';
        }else{
            $row[] = 'NA';
            $row[] = 'NA';
        }


        $row[] = strval(date('d/m/Y, H:i', strtotime($data->created_at)));//Send date

        //Activate date
        if($data->statistics){
            $row[] = strval(date('d/m/Y, H:i', strtotime($data->statistics->created_at)));
            //Fetch Statistics
            $portalController  = new PortalController();
            $stats = $portalController->prepareTransactionRawStatistics($data);

            foreach ($stats as $item) {
                $row[] = trim($item);
            }

        }else{
            $row[] = 'NA';
            $row[] = 'NA';
            $row[] = 'NA';
            $row[] = 'NA';
            $row[] = 'NA';
            $row[] = 'NA';
        }


        $row[] = ($data->flow_type != 'MAIN') ? count($data->liveness) : 0;//liveness attempt

        if($data->info){
            $row[] = ($data->info) ? $data->info->os.', '.$data->info->device.', '.$data->info->browser : 'NA'; //device compatibility
        }else{
            $row[] = 'NA';
        }


        return $row;
    }


    /**
     * Here the column titles of excel files can be set.
     * @return array
     */
    public function getHeaderRow()
    {

        $headers = [
            'Date',
            'Transaction ID',
            'Reference',

            'Status',
            'User',
            'Capture type',
            'Flow',

            'First Name',
            'Last Name',
            'Date of Birth',

            'Data Match',
            'Face Match',
            'Liveness',
            'ID Tampering',

            'Mobile',
            'Email',

            'Send Date',
            'Activate Date',

            'Activate Time Taken',
            'Review Terms Finish Time Taken',
            'Capture ID Finish Time Taken',
            'Review Data Finish Time Taken',
            'Capture Liveness Finish Time Taken',

            'Total Process Time',
            'Liveness Attempts',
            'Device Compatibility',
        ];

        if(!config('centrix.isEnabled')){
            unset($headers['Data Match']);
        }

        return $headers;
    }


}
