<?php

namespace App\Http\Middleware;

use Closure;

class CheckTransaction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

       if(!session()->has('transId')){
           if ($request->wantsJson()) {
               return response()->json([
                   'status' => 'error',
                   'msg'    => '401 Unauthorized.',
               ], 401);
           }

           return response('', 401);
       }

        return $next($request);
    }
}
