<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/api/mobile/upload/video',
        '/api/mobile/data/store',
        '/api/mobile/update/*',
        '/api/mobile/upload/*',
        '/api/mobile/data/*',
        //'/portal/login',
        '/portal/logout',
        '/api/portal/user/*',
        '/api/portal/rate/*',
        '/api/portal/transaction/*',
        '/api/portal/invitation/*',
        '/api/portal/permission/update-bulk',
        '/api/portal/dashboard/usageReport',
        '/api/portal/dashboard/analyticsReport',
        '/api/portal/dashboard/incompleteReport',
    ];


}
