<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ipCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //If the user is logged in, then
        //Redirect based on role
        //else redirect to login page

        $allowed_ips = [
            '122.56.59.57',
            '120.151.149.213',
            '197.25.200.129',
            '154.111.180.233',
            '120.154.23.74',
            '122.59.179.251',
            '121.212.166.48',
            '14.102.160.26',
            '103.82.188.100',
        ];

        $ip = $this->ip();

        // If the Visitor's IP is not among whitelist
        // then,Block him from accessing portal
        if (!in_array($ip, $allowed_ips) && env('APP_ENV') !== 'local') {
            Auth::logout();
            abort('404', 'There is no accessible page at this address.');
        } else {
            return $next($request);
        }
    }

}
