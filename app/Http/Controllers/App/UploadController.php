<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Http\Models\ApiSessions\ApiSessions;
use App\Http\Models\Liveness\Liveness;
use App\Http\Models\Orbitsdk\AwsS3;
use App\Http\Models\Orbitsdk\Orbitsdk;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Transactions\Transactions;
use App\Http\Requests\Upload\Image;
use App\Http\Requests\Upload\Selfie;
use App\Http\Requests\Upload\Video;
use \Illuminate\Http\Request;

class UploadController extends Controller
{
    /**
     * @var Transactions
     */
    public $transaction;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->transaction = $request->transaction->orderBy('created_at', 'desc')->first();
            return $next($request);
        });
    }

    /**
     * Make a request to upload images
     *
     * @param Image $request
     * @return \Illuminate\Http\Request
     */
    public function image(Image $request, Orbitsdk $api)
    {
        if ($request->idType === 'ID_CARD') {
            $req1            = new Request();
            $req1->idType    = $request->idType;
            $req1->frontFile = $request->frontFile;
            $res1            = $api->uploadImageFile($req1);

            if ($res1['status'] !== 'success') {
                return $res1;
            }

            $faceRes  = $api->getCardInformation($res1['session'], false);
            $cardInfo = $faceRes['CardInfo'];

            $req2            = new Request();
            $req2->idType    = $request->idType;
            $req2->frontFile = $request->backFile;
            $res2            = $api->uploadImageFile($req2);

            if ($res2['status'] !== 'success') {
                return $res2;
            }

            $files[] = $res1['file'][0];
            $files[] = str_replace('../../', '', $cardInfo['FaceImage']);
            $files[] = $res2['file'][0];

            if (env('APP_ENV') == 'local' || !config('aws.s3.enable')) {

                $cards['front_card'] = Utils::getFileName('PASSPORT', '../../' . $files[0]);
                $cards['face_photo'] = Utils::getFileName('PASSPORT', '../../' . $files[1]);

            } else {

                $cards['front_card'] = $files[0];
                $cards['face_photo'] = $files[1];

                //Upload in S3 bucket
                AwsS3::storeObject($files[0]);
                AwsS3::storeObject($files[1]);

            }

            if ($this->transaction->cards) {
                $this->transaction->cards()->update($cards);
            } else {
                $this->transaction->cards()->create($cards);
            }

            $response = $res2;

        } else {
            $response = $api->uploadImageFile($request);
            if ($response['status'] !== 'success') {
                return $response;
            }

            $files = $response['file'];
        }

        $request->transaction->update([
            'identify_type' => $request->idType,
        ]);

        return $this->save($response['session'], $request->idType, $files);
    }

    /**
     * Make a request to upload images
     *
     * @param Selfie $request
     * @return \Illuminate\Http\Request
     */
    public function selfie(Selfie $request, Orbitsdk $api)
    {
        $request->transaction->update([
            'flow_type' => 'ALTERNATE',
        ]);
        $response = $api->uploadSelfieImageFile($request);
        if ($response['status'] !== 'success') {
            return $response;
        }

        $files = [];
        foreach ($response['file'] as $file) {
            $files[] = str_replace('../../', '', $file);
        }

        // Store face scan video
        if (env('APP_ENV') == 'local' || !config('aws.s3.enable')) {

            $faceScan = [
                'video' => Utils::getFileName('VID', $response['file'][0]),
                'face'  => Utils::getFileName('VID', $response['file'][1]),
            ];

        } else {

            //Upload in S3 bucket
            AwsS3::storeObject(str_replace('../../', '', $response['file'][0]));
            AwsS3::storeObject(str_replace('../../', '', $response['file'][1]));

            $faceScan = [
                'video' => str_replace('../../', '', $response['file'][0]),
                'face'  => str_replace('../../', '', $response['file'][1]),
            ];
        }

        if ($this->transaction->faceScan) {
            $this->transaction->faceScan()->update($faceScan);
        } else {
            $this->transaction->faceScan()->create($faceScan);
        }

        // Get & Store centrix
        $userDetails    = $this->transaction->userDetails;
        $contactDetails = $this->transaction->contactDetails;
        $address        = json_decode($contactDetails->residential_address, true);

        if (config('centrix.isEnabled')) {

            $data = [
                'dateOfBirth' => $userDetails->dateOfBirth,
                'expiryDate'  => $userDetails->dateOfExpiry,
                'cardType'    => $this->transaction->identify_type,
                'firstName'   => $userDetails->firstName,
                'lastName'    => $userDetails->lastName,
                'middleName'  => $userDetails->middleName,
                'address'     => $address,
                'countryCode' => $this->transaction->country,
                'transaction' => $request->transaction,
            ];

            if ($this->transaction->identify_type == 'PASSPORT') {
                $data['passportNumber'] = $userDetails->passportNumber;
            } else {
                $data['licenceNumber']  = $userDetails->licenceNumber;
                $data['licenceVersion'] = $userDetails->versionNumber;
            }

            $data           = (object) $data;
            $dataController = new DataController();
            $store          = $dataController->storeCentrix($data);

            if (!$store || (isset($store['status']) && $store['status'] == 'error')) {
                return response()->json([
                    'status' => 'error',
                    'type'   => 'centrix',
                    'msg'    => (isset($store['status'])) ? $store['msg'] : 'Failed to store centrix.',
                ]);
            }
        }

        $request->transaction->update([
            //'confidence' => $response['confidence'],
            'status' => 'COMPLETED',
        ]);

        return $this->save($response['session'], 'SELFIE', $files, true);
    }

    /**
     * Make a request to upload video
     *
     * @param Video $request
     * @return \Illuminate\Http\Request
     */
    public function video(Request $request, Orbitsdk $api)
    {
        $liveness = null;

        if (isset($request->blob)) {
            $response = $api->uploadVideoFile($request);
        } else {
            $response = $api->uploadVideoFromID($request);
            $liveness = new Liveness($request->id);
            $data     = $liveness->get();
        }

        if ($response['status'] !== 'success') {
            return $response;
        }

        $files   = [];
        $files[] = str_replace('../../', '', $response['file']);

        // User face image
        $filename = explode('/', $response['file']);
        $filename = $filename[count($filename) - 1];
        $face     = str_replace($filename, 'face.jpg', $response['file']);
        // Store face scan video
        if (env('APP_ENV') == 'local' || !config('aws.s3.enable')) {
            $faceScan = [
                'video' => Utils::getFileName('VID', $response['file']),
                'face'  => Utils::getFileName('VID', $face),
            ];
        } else {
            $video = str_replace('../../', '', $response['file']);
            $face  = str_replace(basename($video), '', $video);

            //Upload in S3 bucket
            AwsS3::storeObject($video);
            AwsS3::storeObject($face . 'face.jpg');

            $faceScan = [
                'video' => $video,
                'face'  => $face . 'face.jpg',
            ];
        }

        if ($this->transaction->faceScan) {
            $this->transaction->faceScan()->update($faceScan);
        } else {
            $this->transaction->faceScan()->create($faceScan);
        }

        $d = $this->transaction->liveness()->first();

        $result = isset($request->success);

        if (isset($request->id)) {
            if (!$this->transaction->liveness || ($this->transaction->liveness && $d['token'] !== $request->id)) {
                // Store face scan video
                if (isset($data) && !empty($data)) {
                    $lr        = $request->lr;
                    $turn      = $lr == true ? 'left' : 'right';
                    $breakDown = $data['data'];
                    $result    = $breakDown[$turn] == true;
                    //$result =  $data['status'];
                }

                $this->transaction->liveness()->create([
                    'token'      => $request->id,
                    'result'     => $result,
                    'spoof_risk' => !$result,
                    'actions'    => $request->actions,
                    'video'      => $faceScan['video'],
                ]);
            }
        }

        return $this->save($response['session'], 'VID', $files, false, $result);
    }

    /**
     * Save the session
     *
     * @param string $session
     * @param string $idType
     * @param array $files
     * @param bool $sendRedirect
     * @param bool $liveness
     * @return \Illuminate\Http\Request
     */
    private function save($session, $idType, $files = null, $sendRedirect = false, $liveness = false)
    {

        $urls  = $this->transaction->urls()->first();
        $token = md5(sha1(bcrypt($session)));

        $apiSession             = new ApiSessions();
        $apiSession->tId        = $this->transaction->id;
        $apiSession->type       = $idType;
        $apiSession->token      = $token;
        $apiSession->session_id = $session;
        $apiSession->host       = config('orbitsdk.api.url');

        if ($files) {
            $apiSession->files = json_encode($files);
        }

        $apiSession->save();

        if (!$apiSession) {
            return response()->json(['status' => 'error', 'msg' => 'Oops, something wrong, please try again.'], 500);
        }
        $res = ['status' => 'success', 'token' => $token];

        if ($sendRedirect) {
            $res['redirectTo'] = $urls->return_url;
        }

        if ($liveness) {
            $res['liveness'] = $liveness;
        }

        return response()->json($res);
    }
}
