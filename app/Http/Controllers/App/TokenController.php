<?php

namespace App\Http\Controllers\App;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Models\Liveness\Token as LivenessToken;
use App\Http\Models\Transactions\Transactions;
use App\Http\Requests\Token\Token;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Jenssegers\Agent\Agent;
use \Illuminate\Http\Request;

class TokenController extends Controller
{
    /**
     * Check the trsansaction ID
     *
     * @param Token $request
     * @return \Illuminate\Http\Request
     */
    public function create(Token $request)
    {
        if (Helper::transactionIdExists($request->transactionId)) {
            return response()->json([
                'status'  => 'error',
                'session' => 'This Transaction Id already exists.',
            ]);
        }
        //$token = self::generateToken();
        $transactionId = $request->transactionId;
        $token         = md5(sha1($transactionId));

        $transaction = new Transactions();

        $transaction->category  = 'remote-verification';
        $transaction->module    = 'identity-flow-self-guided';
        $transaction->reference = $request->reference ?? null;
        $transaction->username  = User::where('active', true)->where('role', 'admin')->first()->username ?? null;

        $transaction->transactionId  = $transactionId;
        $transaction->token          = $token;
        $transaction->original_token = $request->transactionId;

        $transaction->user_status = 'waiting';

        $transaction->save();

        //$liveness = new LivenessToken();
        //$liveness->createSession( $transaction->token);

        //Store
        $url_inputs = [
            'return_url' => $request->return_url ?? route('mobileStatus', ['status' => 'completed']),
            'cancel_url' => $request->cancel_url ?? route('mobileStatus', ['status' => 'cancelled']),
        ];

        $expiry = config('portal.sms_expiry');

        if ($expiry < 1) {
            $expiry               = ceil(60 * $expiry);
            $url_inputs['expiry'] = now()->addMinutes($expiry);
        } else {
            $url_inputs['expiry'] = now()->addHours($expiry);
        }

        $transaction->urls()->create($url_inputs);

        //Store contact details
        if ($request->name) {
            $transaction->contactDetails()->create([
                'name' => $request->name ?? null,
            ]);
        }
        return response()->json([
            'status'  => 'success',
            'session' => $token,
        ]);
    }

    /**
     * Check the trsansaction ID
     *
     * @param string $token
     * @return \Illuminate\Http\Request
     */
    public function check($token, Request $request)
    {
        $session = Transactions::where([
            'token' => $token,
        ])->where('status', '!=', 'FAILED')->orderBy('created_at', 'desc');
        $data = $session->first();

        if (!$session->exists()) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 'error',
                    'msg'    => 'Invalid token.',
                ], 422);
            }

            if (env('ERROR_URL')) {
                return redirect(env('ERROR_URL'));
            }

            return abort(401);
        }

        if ($data->status == 'PENDING') {
            $session->update([
                'status' => 'INPROGRESS',
            ]);
        }

        $agent = new Agent();
        $info  = self::clientInfo();
        if (!array_key_exists('isp', $info)) {
            $info = ['isp' => 'unknown'];
        }

        $browser  = $agent->browser();
        $device   = $agent->device();
        $platform = $agent->platform();
        if ($platform) {
            $platform .= ' ' . substr($agent->version($platform), 0, 2);
        }

        $info = [
            'device'      => !empty($device) ? $device : 'unknown',
            'os'          => !empty($platform) ? $platform : 'unknown',
            'browser'     => !empty($browser) ? $browser : 'unknown',
            'isp'         => $info['isp'],
            'ip'          => self::ip(),
            'vpn'         => self::vpn(),
            'ip_location' => isset($info['lat']) ? $info['lat'] . ',' . $info['lon'] : '',
        ];

        if (!$data->info) {
            $data->info()->create($info);
        } else {
            $data->info()->update($info);
        }

        $phone = '';

        if ($data->contactDetails) {
            $contact = $data->contactDetails()->first();
            $phone   = $contact->phone;
        }

        session()->put([
            '__token' => $data->token,
            '__phone' => $phone,
        ]);

        if ($request->wantsJson()) {
            return response()->json([
                'status' => 'success',
            ]);
        }

        return redirect(Helper::getSiteUrl());
    }

    public function view()
    {
        //reset
        $session = Transactions::where([
            'token' => session('__token'),
        ])->where('status', '!=', 'FAILED')->orderBy('created_at', 'desc');
        $data  = $session->first();
        $cards = $data->cards;
        if ($data->instruction_type == 'GUIDED') {
            if ($cards) {
                $cards->update(['approved' => 0]);
            }
        }
        //Reset flow
        request()->transaction->update([
            'flow_type' => 'MAIN',
        ]);

        //Create if not already
        $clientinfo = $data->info;
        if (!$clientinfo) {

            $ispinfo = self::clientInfo();
            if (!array_key_exists('isp', $ispinfo)) {
                $ispinfo = ['isp' => 'unknown'];
            }

            $info = [
                'device'      => !empty($device) ? $device : 'unknown',
                'os'          => !empty($platform) ? $platform : 'unknown',
                'browser'     => !empty($browser) ? $browser : 'unknown',
                'isp'         => $ispinfo['isp'],
                'ip'          => self::ip(),
                'vpn'         => self::vpn(),
                'ip_location' => $ispinfo['lat'] . ',' . $ispinfo['lon'],
            ];

            request()->transaction->info()->create($info);
        }

        return view('app', ['phone' => session('__phone'), 'token' => session('__token')]);
    }

    /**
     * Check VPN
     *
     * @return boolean
     */
    protected static function vpn()
    {
        $api  = file_get_contents('http://api.vpnblocker.net/v2/json/' . self::ip());
        $json = json_decode($api, true);

        if (!$json || !isset($json['host-ip'])) {
            return false;
        }

        return $json && $json['host-ip'];
    }

    /**
     * Get client IP address
     *
     * @return string
     */
    public static function ip()
    {
        $ip = '';
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
        } else {
            $ips = [$_SERVER['REMOTE_ADDR']];
        }
        $ip = $ips[0];
        if (preg_match('#:#i', $ip)) {
            $exp = explode(':', $ip);
            $ip  = $exp[0];
        }

        if (env('APP_ENV') == 'local') {
            return '103.82.188.100'; //Santosh's Ip
        }

        return $ip;
    }

    /**
     * Get client IP address
     *
     * @return string
     */
    public static function clientInfo()
    {
        $response = file_get_contents('http://ip-api.com/json/' . self::ip());
        $data     = json_decode($response, true);

        return $data;
    }
}
