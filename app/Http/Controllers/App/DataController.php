<?php

namespace App\Http\Controllers\App;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Portal\PortalController;
use App\Http\Models\Address\Address;
use App\Http\Models\ApiSessions\ApiSessions;
use App\Http\Models\Centrix\Centrix;
use App\Http\Models\GreenID\GreenID;
use App\Http\Models\Liveness\Token;
use App\Http\Models\Orbitsdk\AwsS3;
use App\Http\Models\Orbitsdk\Orbitsdk;
use App\Http\Models\Orbitsdk\Response;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Portal\Core\Permission;
use App\Http\Models\Transactions\Transactions;
use App\Http\Requests\Data\Store as StoreRequest;
use App\Notifications\TransactionCompleted;
use App\User;
use Illuminate\Http\Request;
use Session;

class DataController extends Controller
{
    /**
     * @var Transactions
     */
    public $transaction;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->transaction = $request->transaction->orderBy('created_at', 'desc')->first();
            return $next($request);
        });
    }

    /**
     * Make a request get card information
     *
     * @param string $token
     * @return Request
     */
    public function cards($token, Orbitsdk $api)
    {
        $apiSession = ApiSessions::where('token', $token)
            ->whereIn('type', config('orbitsdk.api.id_types'))
            ->first();

        if (!$apiSession) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'The token field is invalid.',
            ], 422);
        }

        $result = [
            'status' => 'success',
            'code'   => '200',
            'data'   => [],
        ];

        if (request()->has('back') && request()->input('back') == 'no') {
            $cardInfo = $api->getCardInformation($apiSession->session_id, false);
        } else {
            $cardInfo = $api->getCardInformation($apiSession->session_id);
        }

        if (empty($cardInfo) || $cardInfo['CardInfo']['StatusCode'] != "200") {

            $info   = $cardInfo['CardInfo'];
            $result = [
                'status' => 'error',
                'code'   => $cardInfo['CardInfo']['StatusCode'],
                'msg'    => $cardInfo['CardInfo']['StatusMessage'],
                'data'   => [],
            ];

            return response()->json($result);
        }

        $response = Response::cardInfo($cardInfo);

        if ($response['status']) {
            $result['status'] = $response['status'];
        }

        if ($response['status_code']) {
            $result['code'] = $response['status_code'];
        }

        if ($response['data']) {
            $result['data'] = $response['data'];

            $this->storeExtractedInfo($result['data']);
            $result['data']['asf'] = !empty($response['asf']) ? $response['asf']['overall'] : null;
        }

        $cards = [
            'session' => $apiSession->session_id,
            'asf'     => count($response['asf']) ? json_encode($response['asf']) : '',
        ];

        $identifyType = $this->transaction->identify_type;

        if ($response['data']) {
            $cardType = $response['data']['cardType'];
            $cardType = (strpos($cardType, 'AUS') !== false) ? 'AUS_AUTO_DRIVERLICENCE' : $cardType;
            $cardInfo = $cardInfo['CardInfo'];

            if (env('APP_ENV') == 'local' || !config('aws.s3.enable')) {

                if ($identifyType == 'ID_CARD') {
                    $cards['back_card'] = Utils::getFileName($cardType, $cardInfo['RecognizedFrontImage']);
                } else {
                    $cards['front_card'] = Utils::getFileName($cardType, $cardInfo['RecognizedFrontImage']);
                    $cards['back_card']  = Utils::getFileName($cardType, $cardInfo['RecognizedBackImage']);
                    $cards['face_photo'] = Utils::getFileName($cardType, $cardInfo['FaceImage']);
                }

            } else {

                if ($identifyType == 'ID_CARD') {

                    $cards['back_card'] = str_replace('../../', '', $cardInfo['RecognizedFrontImage']);

                    //Upload in S3 bucket
                    AwsS3::storeObject($cardInfo['RecognizedFrontImage']);
                    AwsS3::storeObject($cardInfo['RecognizedFrontImage'], true);

                } else {
                    $cards['front_card'] = str_replace('../../', '', $cardInfo['RecognizedFrontImage']);
                    $cards['back_card']  = isset($cardInfo['RecognizedBackImage']) && !empty($cardInfo['RecognizedBackImage']) ? str_replace('../../', '', $cardInfo['RecognizedBackImage']) : null;
                    $cards['face_photo'] = str_replace('../../', '', $cardInfo['FaceImage']);

                    //Upload in S3 bucket
                    AwsS3::storeObject($cardInfo['RecognizedFrontImage']);
                    AwsS3::storeObject($cardInfo['RecognizedFrontImage'], true);
                    AwsS3::storeObject($cardInfo['FaceImage']);
                    if (isset($cardInfo['RecognizedBackImage']) && !empty($cardInfo['RecognizedBackImage'])) {
                        AwsS3::storeObject($cardInfo['RecognizedBackImage']);
                        AwsS3::storeObject($cardInfo['RecognizedBackImage'], true);
                    }
                }
            }

        }

        if ($this->transaction->cards) {
            $this->transaction->cards()->update($cards);
        } else {
            $this->transaction->cards()->create($cards);
        }

        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->transaction->statistics->updated_at);
        $ts = $to->diffInSeconds(now());

        $data = [
            'column' => 'capture_id',
            'ts'     => $ts,
            'tId'    => $this->transaction->id,
        ];
        PortalController::updateStatistics($data);

        return response()->json($result);
    }

    /**
     * Make a request to get face scan data
     *
     * @param string $token
     * @param Request $request
     * @param Orbitsdk $api
     * @return Request
     */
    public function face($token, Request $request, Orbitsdk $api)
    {
        //liveness_ms
        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->transaction->statistics->updated_at);
        $ts = $to->diffInSeconds(now());

        $data = [
            'column' => 'liveness',
            'ts'     => $ts,
            'tId'    => $this->transaction->id,
        ];
        PortalController::updateStatistics($data);
        //------
        $apiSession = ApiSessions::where('token', $token)
            ->where('type', 'VID')
            ->first();

        if (!$apiSession) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'The token field is invalid.',
            ], 422);
        }

        $urls   = $this->transaction->urls()->first();
        $result = [
            'status'     => 'success',
            'confidence' => false,
            'liveness'   => false,
        ];

        if ($urls->return_url != route('mobileStatus', ['status' => 'completed'])) {
            $result['redirectTo'] = $urls->return_url;
        }
        $filepath = '';
        if ($this->transaction->cards && $this->transaction->cards->front_card) {

            if (env('APP_ENV') == 'local' || !config('aws.s3.enable')) {
                $filepath = Utils::getBaseUrl($this->transaction->identify_type, true) . $this->transaction->cards->front_card;
            } else {
                $filepath = '../../' . $this->transaction->cards->front_card;
            }

        }

        $seSession = $api->setSessionSide($apiSession->session_id, $filepath);

        $response = Response::faceScan($api->getFaceScanData($apiSession->session_id));

        if ($response) {
            $result['confidence']           = $response['confidence'];
            $result['liveness_assessments'] = $response['liveness_assessments'];
        }

        //check if the features is available
        if (!$response['features'] || empty($response['features'])) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Feature in blank.',
            ]);
        }

        // Store face scan data
        if ($this->transaction->faceScan) {
            $fields = [
                'session'              => $apiSession->session_id,
                'confidence'           => $response['confidence'],
                'features'             => json_encode($response['features']),
                'liveness_assessments' => json_encode($response['liveness_assessments']),
            ];

            if ($this->transaction->faceScan) {
                $this->transaction->faceScan()->update($fields);
            } else {
                $this->transaction->faceScan()->create($fields);
            }

        }

        // Get & Store centrix
        $userDetails    = $this->transaction->userDetails;
        $contactDetails = $this->transaction->contactDetails;
        $address        = json_decode($contactDetails->residential_address, true);

        $data = [
            'dateOfBirth' => $userDetails->dateOfBirth,
            'expiryDate'  => $userDetails->dateOfExpiry,
            'cardType'    => $this->transaction->identify_type,
            'firstName'   => $userDetails->firstName,
            'lastName'    => $userDetails->lastName,
            'middleName'  => $userDetails->middleName,
            'address'     => $address,
            'countryCode' => $this->transaction->country,
            'transaction' => $request->transaction,
        ];

        if ($this->transaction->identify_type == 'PASSPORT') {
            $data['passportNumber'] = $userDetails->passportNumber;
        } else {
            $data['licenceNumber']  = $userDetails->licenceNumber;
            $data['licenceVersion'] = $userDetails->versionNumber;
        }

        $data = (object) $data;

        if (config('app.verification_api') == 'centrix' && Helper::hasCentrix($this->transaction->country)) {

            $store = $this->storeCentrix($data);

            if (!$store || (isset($store['status']) && $store['status'] == 'error')) {
                return response()->json([
                    'status' => 'error',
                    'type'   => 'centrix',
                    'msg'    => (isset($store['status'])) ? $store['msg'] : 'Failed to store centrix.',
                ]);
            }
        }

        if (config('app.verification_api') == 'greenid') {
            $greenid = GreenID::get($data);
            if ($greenid['success']) {
                GreenID::save($greenid['data'], $this->transaction);
            } else {
                return response()->json([
                    'status' => 'error',
                    'msg'    => $greenid['message'],
                ]);
            }
        }

        $this->transaction->contactDetails()->update([
            'residential_address' => $address['fullAddress'],
        ]);

        $input = [
            'confidence'  => $response['confidence'],
            'user_status' => 'completed',
        ];
        if ($this->transaction->instruction_type != 'GUIDED') {
            $input['status'] = 'COMPLETED';
        }
        $this->transaction->update($input);

        // Send Notification
        $this->notifyUser($input['confidence'], $this->transaction);

        /*$tr = \App\Http\Models\Transactions\Transactions::find(42);
        $user = User::where('username',$tr->username)->first();
        if(config('portal.email_notifications') && $user){
        //
        $user->notify(new TransactionCompleted($user, $tr));
        }*/

        return response()->json($result);
    }

    /**
     * Send Email Notification
     *
     * @param Transactions $tr
     * @return
     */
    public function notifyUser($face, $tr)
    {
        $columns = [
            'transactionId' => 'Transaction ID',
            'reference'     => 'Reference',
            'name'          => 'Customer Name',
            'contact'       => 'Contact',
        ];

        // Data
        $data = [
            'transactionId' => $tr->transactionId,
            'reference'     => $tr->reference ? $tr->reference : 'NA',
            'name'          => ($tr->userDetails) ? $tr->userDetails->firstName . ' ' . $tr->userDetails->lastName : $tr->contactDetails->name,
            'contact'       => ($tr->contactDetails->phone) ? $tr->contactDetails->phone : $tr->contactDetails->email,
        ];

        // Flags
        $flags = [
            'Face' => $face,
            'Live' => true,
            'Doc'  => false,
        ];

        $liveness = $tr->liveness()->orderBy('created_at', 'DESC')->first();
        if ($liveness) {
            $liveness = $liveness->toArray();

            if ($liveness && !empty($liveness)) {
                $flags['Live'] = Centrix::boolean($liveness['result']);
            }
        }

        if ($tr->cards && isset($tr->cards->asf)) {
            $asf          = json_decode($tr->cards->asf, true);
            $flags['Doc'] = isset($asf['overall']) ? Centrix::boolean($asf['overall']) : false;
        }

        if (Helper::hasCentrix($tr->country)) {
            if ($tr->centrixSmartId && $tr->centrixSmartId->IsVerified) {
                $flags['Data'] = Centrix::boolean($tr->centrixSmartId->IsVerified);
            }
        }

        $overall = $flags['Face'] && $flags['Live'] && $flags['Doc'];
        if (!Helper::hasCentrix($tr->country)) {
            $type = ($overall) ? 'transaction-pass' : 'transaction-failed';
        } else {

            if ($overall && isset($flags['Data']) && $flags['Data']) {
                $type = 'transaction-pass';
            } elseif ($overall && isset($flags['Data']) && !$flags['Data'] && Helper::hasCentrix($tr->country)) {
                $type = 'transaction-failed-data';
            } else {
                $type = 'transaction-failed';
            }
        }

        if ($type == 'transaction-pass') {
            $child_slug = 'pass-transaction';
        } elseif ($type == 'transaction-failed-data') {
            $child_slug = 'flagged-transaction-data-only';
        } else {
            $child_slug = 'flagged-transaction';
        }
        //---------
        $user = User::where('username', $tr->username)->first();

        if (config('portal.email_notifications') && $user && Permission::hasPermission('email-alerts', $child_slug, $user->id)) {
            $user->notify(new TransactionCompleted($type, $columns, $data, $flags, $tr->reference));
        }
    }

    /**
     * Check type of transaction, guided or self-guided.
     *
     * @param Request $request
     * @return
     */
    public function checkType()
    {
        $transaction = $this->transaction;
        try {

            $instruction_type = $transaction->instruction_type;

            return response()->json([
                'status' => 'success',
                'verify' => $instruction_type == 'GUIDED',
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status'  => 'failed',
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Update user status of mobile screen.
     *
     * @param Request $request
     * @return
     */
    public function updateStatus($status)
    {
        $transaction = $this->transaction;

        // Create Liveness Session
        if ($status == 'faceScanTips') {
            $liveness = new Token();
            $liveness->createSession($this->transaction->token);
        }

        $transaction->update([
            'user_status' => $status,
        ]);

        // avoid refresh--for redo
        if ($status == 'privacy' && $transaction->status == 'Redo') {
            $transaction->update([
                'status' => 'Initiated',
            ]);
        } elseif ($status == 'privacy' && $transaction->status == 'Initiated') {
            $transaction->update([
                'status' => 'InProgress',
            ]);
        }

        //---------------------------------
        //update statistics
        //---------------------------------
        if ($status == 'selectId') {
            //review terms = created at - now()

            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transaction->statistics->created_at);
            $ts = $to->diffInSeconds(now());

            $data = [
                'column' => 'review_terms',
                'ts'     => $ts,
                'tId'    => $transaction->id,
            ];
            PortalController::updateStatistics($data);
        }

        return response()->json(['status' => 'success']);
    }

    /**
     * Check card approval in case of guided flow.
     *
     * @param Request $request
     * @return
     */
    public function checkApproval()
    {
        $transaction = $this->transaction;
        try {

            $cards = $transaction->cards;

            $status = ['pending', 'approved', 'recapture'];

            return response()->json([
                'status' => $status[$cards->approved],
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status'  => 'failed',
                'message' => $e->getMessage(),
            ]);
        }
    }
    /**
     * Make a request to centrix data
     *
     * @param StoreRequest $request
     * @return Request
     */
    public function store(StoreRequest $request)
    {

        if (empty($request->homeAddress) &&
            !($request->addressLine1)) {
            return response()->json([
                'status' => 'error',
                'type'   => 'cards',
                'msg'    => 'Insufficient address information given',
            ]);
        }

        $result = [
            'status' => 'success',
        ];

        if ($request->homeAddress && $request->homeAddress != 'undefined') {

            if (in_array($request->countryCode, config('centrix.countries'))) {
                $address          = new Address($request->homeAddress, $request->countryCode);
                $request->address = $address->get();
            } else {
                $request->address = [
                    'fullAddress' => $request->homeAddress,
                ];
            }

            if (!$request->address) {
                return response()->json([
                    'status' => 'error',
                    'type'   => 'address',
                    'msg'    => 'Invalid address. Please enter the correct address.',
                ]);
            }
        } else {
            $request->address = [
                'fullAddress'  => $request->addressLine1 . ' ' . $request->addressLine2 . ' ' . $request->city . ' ' . $request->suburb . ' ' . $request->postcode . ' ' . $request->country,
                'addressLine1' => $request->addressLine1,
                'addressLine2' => $request->addressLine2 ?? '',
                'city'         => $request->city,
                'suburb'       => $request->suburb,
                'postcode'     => $request->postcode,
            ];
        }

        // Store user details
        $store = $this->storeCardInfo($request);

        if (!$store) {
            return response()->json([
                'status' => 'error',
                'type'   => 'cards',
                'msg'    => 'Failed to store card informations.',
            ]);
        }

        // Store contact details
        $store = $this->storeContactDetails($request);

        if (!$store) {
            return response()->json([
                'status' => 'error',
                'type'   => 'address',
                'msg'    => 'Failed to store addresses.',
            ]);
        }

        // Update info location
        if ($request->location && $request->location != 'null') {

            $this->transaction->info()->update([
                'geolocation' => $request->location,
            ]);
        }

        // Update reverse Geocode
        $this->updateLocation($this->transaction);

        //review_data_ms
        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->transaction->statistics->updated_at);
        $ts = $to->diffInSeconds(now());

        $data = [
            'column' => 'review_data',
            'ts'     => $ts,
            'tId'    => $this->transaction->id,
        ];
        PortalController::updateStatistics($data);
        //update country
        if ($request->countryCode) {
            $this->transaction->country = $request->countryCode;
        }

        $this->transaction->completed_at = now();
        $this->transaction->save();
        //----

        return $result;
    }

    /**
     * Update Location
     *
     * @param Transactions $tr
     *
     */
    protected function updateLocation($tr)
    {
        $info = $tr->info;

        if ($info->ip_location) {
            $ip_location = (Helper::isJson($info->ip_location)) ? json_decode($info->ip_location, true)['coordinates'] : $info->ip_location;
            $ipLoc       = (array) $this->reverseGeocode($ip_location)->results;
            if (!empty($ipLoc)) {
                $ipLoc = $ipLoc[count($ipLoc) - 4]->formatted_address;
                $tr->info->update([
                    'ip_location' => json_encode([
                        'loc'         => $ipLoc,
                        'coordinates' => $ip_location,
                    ]),
                ]);
            } else {
                $tr->info->update([
                    'ip_location' => json_encode([
                        'coordinates' => $ip_location,
                    ]),
                ]);
            }

        }

        if ($info->geolocation) {
            $geolocation = (Helper::isJson($info->geolocation)) ? json_decode($info->geolocation, true)['coordinates'] : $info->geolocation;
            $data        = $this->reverseGeocode($geolocation);
            if (isset($data->results)) {
                $geoLoc = $data->results;

                if (!empty($geoLoc)) {
                    $geoLoc = $geoLoc[count($geoLoc) - 4]->formatted_address;
                    $tr->info->update([
                        'geolocation' => json_encode([
                            'loc'         => $geoLoc,
                            'coordinates' => $geolocation,
                        ]),
                    ]);
                } else {
                    $tr->info->update([
                        'geolocation' => json_encode([
                            'coordinates' => $geolocation,
                        ]),
                    ]);
                }
            }

        }

    }

    /**
     * Store card info.
     *
     * @param StoreRequest $request
     * @return Trasactions
     */
    public function storeCardInfo(StoreRequest $request)
    {
        $userDetails = [
            'firstName'      => $request->firstName,
            'lastName'       => $request->lastName,
            'middleName'     => $request->middleName,
            'dateOfBirth'    => date('Y-m-d', strtotime($request->dateOfBirth)),
            'dateOfExpiry'   => date('Y-m-d', strtotime($request->expiryDate)),
            'licenceNumber'  => $request->licenceNumber,
            'versionNumber'  => $request->licenceVersion,
            'passportNumber' => $request->passportNumber,
            'cardNumber'     => $request->cardNumber,
        ];

        // Save edited fields
        $this->transaction->cards()->update(['edited' => $request->edited]);

        if ($this->transaction->userDetails) {
            return $this->transaction->userDetails()->update($userDetails);
        } else {
            return $this->transaction->userDetails()->create($userDetails);
        }
    }

    /**
     * Store extracted card info.
     *
     * @param array $data
     * @return Trasactions
     */
    public function storeExtractedInfo($data)
    {
        $extracted = [
            'cardType'       => null,
            'licenceType'    => null,
            'firstName'      => null,
            'lastName'       => null,
            'middleName'     => null,
            'dateOfBirth'    => null,
            'expiryDate'     => null,
            'countryOfIssue' => null,
            'address'        => null,
            'street'         => null,
            'city'           => null,
            'state'          => null,
            'zipCode'        => null,
            'licenceNumber'  => null,
            'licenceVersion' => null,
            'cardNumber'     => null,
            'imageQuality'   => null,
        ];

        foreach ($data as $key => $value) {
            if (!empty($value) && array_key_exists($key, $extracted)) {
                $extracted[$key] = $value;
            }
        }

        if ($this->transaction->extracted) {
            return $this->transaction->extracted()->update($extracted);
        } else {
            return $this->transaction->extracted()->create($extracted);
        }
    }

    /**
     * Store card info.
     *
     * @param StoreRequest $request
     * @return Trasactions
     */
    public function storeContactDetails(StoreRequest $request)
    {
        $contactDetails = [
            //'residential_address' => $request->address['fullAddress'],
            'residential_address' => json_encode($request->address),
            'previous_address'    => $request->prevAddress,
        ];

        if ($this->transaction->contactDetails) {
            return $this->transaction->contactDetails()->update($contactDetails);
        } else {
            return $this->transaction->contactDetails()->create($contactDetails);
        }
    }

    /**
     * Store centrix.
     *
     * @param $request
     * @return Trasactions
     */
    public function storeCentrix($request)
    {
        try {

            $IDVerification = false;

            $centrix = Centrix::request($request);
            $default = Centrix::defaultCentrix();

            if ($request->countryCode !== 'AU') {
                $SmartID = $centrix['data']["DataSets"]["SmartID"];
                if ($centrix['success']) {

                    // Check Smart ID
                    if (filter_var($SmartID["IsVerified"], FILTER_VALIDATE_BOOLEAN)) {
                        $IDVerification = true;
                    }
                }

                $request->transaction->update([
                    'centrix' => $IDVerification,
                    'country' => $request->countryCode,
                ]);
            } else {

                $request->transaction->update([
                    'country' => $request->countryCode,
                ]);
            }

            return Centrix::store($centrix['data'], $request);

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['msg']    = $e->getMessage();
            return $response;
        }

    }

    /**
     * Get country by IP Address
     *
     * @return array
     */
    public function country()
    {
        $file = file_get_contents('http://api.ipstack.com/' . TokenController::ip() . '?access_key=0a3ac3f97b1b2fcb1a25d10e4dca4306&format=1');
        $json = json_decode($file);
        return response()->json($json);
    }

    /**
     * Get location by coordinates
     * @param $cordinates
     * @return array
     */
    public function reverseGeocode($cordinates)
    {
        if (Helper::validateLoc($cordinates)) {
            $file = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyC_4BJLpINXrnzuAS7QJiyK7199U3Bm4W0&latlng=' . $cordinates . '&sensor=true');
            $json = json_decode($file);
            //return response()->json($json);
            return $json;
        } else {
            return [
                'results' => [],
            ];
        }

    }
}
