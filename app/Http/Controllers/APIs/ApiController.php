<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Http\Models\Transactions\Transactions;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class ApiController extends Controller
{

    /**
     * Check device
     *
     * @param Request $request
     * @return \Illuminate\Http\Request
     */
    public function device(Request $request)
    {
        $agent = new Agent();

        if (isset($request->userAgent)) {
            $agent->setUserAgent($request->userAgent);
        }

        $supported = true;
        $os        = $agent->platform();
        $osVersion = (int) substr($agent->version($agent->platform()), 0, 2);
        $browser   = $agent->browser();

        if ($os == "iOS") {
            if ($osVersion < 11 || $browser !== "Safari") {
                $supported = false;
            }
        } else if ($os == "Android OS") {
            $userAgent = $request->userAgent;
            if ($browser !== "Chrome" || preg_match('/SamsungBrowser/i', $userAgent)) {
                $supported = false;
            }
        } else {
            $supported = false;
        }

        return response()->json([
            'support' => $supported,
            'os' => $os,
            'osVersion' => $osVersion,
            'browser' => $browser,
        ], 200);
    }

    /**
     * Delete transaction by token
     *
     * @param string $token
     * @return Request
     */
    public function delete($token)
    {
        $tr = Transactions::where('token', $token)->first();
        if (!$tr) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Transaction not found.',
            ], 404);
        }

        $tr->forceDelete();
    }
}