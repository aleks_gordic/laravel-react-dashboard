<?php

namespace App\Http\Controllers\Portal;

use App\Helpers\Helper;
use App\Http\Controllers\APIs\ApiController;
use App\Http\Controllers\App\TokenController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Portal\Core\RoleController;
use App\Http\Models\Centrix\Centrix;
use App\Http\Models\Liveness\Token;
use App\Http\Models\Orbitsdk\AwsS3;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Portal\Core\Permission;
use App\Http\Models\Transactions\Seen;
use App\Http\Models\Transactions\Statistics;
use App\Http\Models\Transactions\Transactions;
use App\Http\Serialisers\TransactionSerialiser;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exporter;

class PortalController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Portal Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles all the api routes and api routes for portal admin
    |
    */

    public function checkIfLoggedIn(){
        if(Auth::check()){
            $response['status'] = 'success';
            //-------------------------------
            $user = Auth::user()->toArray();
            unset($user['email']);
            $user['role'] = $user['role'] ?? 'NA';
            $user['permissions'] = (new RoleController)->getRolePermission($user['role'], false);

            $response['data'] = $user;

            if (request()->session()->has('token')) {
                $session = json_decode(session('token'), true);
                $response['session'] = $session['access_token'];
            }

        }else{
            $response['status'] = 'error';
        }
        return response()->json($response);
    }

    public function getCompletedTransactions( Request $request ){
        //------------------------------------------------
        $inputs = $request->all();
        $inputs['filter'] = array_filter(json_decode($inputs['filter'],true));
        $inputs['query'] = array_filter(json_decode($inputs['query'],true));
        //--------------------------------

        $transactions = Transactions::with([
            'contactDetails:tId,phone,email,name',
            'userDetails:tId,firstName,lastName',
            'liveness',
            'centrixSmartId:tId,IsVerified',
            'centrixLicence',
            'centrixPassport',
            'cards:tId,asf',
            'faceScan'
        ]) ->orderBy('completed_at','desc');

        //----------------------------------------
        //----------------------------------------
        if(!empty($inputs['query']) && isset($inputs['query']['term'])){

            $term = Helper::xss_patch_input($inputs['query']['term']);

            $transactions = $transactions->where(function (Builder $query) use ($term) {
                return $query->where('transactionId', 'like',  '%' . $term . '%')
                    ->orWhere('reference', 'like',  '%' . $term . '%')
                    ->orWhereHas('userDetails', function ($query) use ($term) {
                        $query->where('firstName', 'like',  '%' . $term . '%')->orWhere('lastName', 'like',  '%' . $term . '%');
                    });
            });

        } elseif($inputs['filter'] && !empty($inputs['filter'])){

            foreach ($inputs['filter'] as $k => $v){
                //---//---
                if($v){

                    if($k === 'result'){
                        if($v === 'completed-pass'){

                            $transactions = $transactions->whereHas('faceScan', function ($query) {
                                $query->where('confidence','=', true);
                            })->whereHas('liveness', function ($query) {
                                $query->limit(1)->orderBy('created_at', 'desc')->where('result','=', true);
                            })->whereHas('cards', function ($query) {
                                $query->where('asf->overall','=', true);
                            });

                            // if centrix is enabled
                            if(config('centrix.isEnabled')){
                                $transactions = $transactions->whereHas('centrixSmartId', function ($query) {
                                    $query->where('IsVerified','=', true);
                                });
                            }

                        }else{

                            $transactions = $transactions->whereHas('faceScan', function (Builder $q) {
                                $q->where('confidence','!=', true);
                            })->orWhereHas('liveness', function ($q) {
                                $q->limit(1)->orderBy('created_at', 'desc')->where('result','!=', true);
                            })->orWhereHas('cards', function ($q) {
                                $q->where('asf','LIKE', '%{"overall":false,%');
                            });

                            if(config('centrix.isEnabled')){
                                $transactions = $transactions->orWhereHas('centrixSmartId', function ($q) {
                                    $q->where('IsVerified','!=', true);
                                });
                            }


                        }

                    }

                    if($k == 'dateFrom' || $k == 'dateTo'){

                        if($k == 'dateTo'){
                            $dateTo = Carbon::parse($v)->endOfDay();
                            $transactions = $transactions->whereDate('completed_at','<=',$dateTo);
                        }

                        if($k == 'dateFrom'){
                            $dateFrom = Carbon::parse($v)->format('Y-m-d');
                            $transactions = $transactions->whereDate('completed_at','>=',$dateFrom);
                        }

                    }else{
                        if($k === 'result'){
                            continue;
                        }
                        $transactions = $transactions->where($k,Helper::xss_patch_input($v));
                    }
                }
            }
        }
        //------------------------------------------------
        //------------------------------------------------
        $transactions = $transactions->where('status','=', 'COMPLETED')->get()->toArray();
        $new_data = [];
        foreach ($transactions as $trans){

            $trans['Data'] = false;
            $trans['Doc'] = false;
            $trans['Live'] = false;
            $trans['Face'] = false;
            $trans['username'] = $trans['username'] == env('BASIC_AUTH') ? 'API' : $trans['username'];
            /*$user = Auth::user();
            $ids = $user->transactions->pluck('id')->toArray();
            $trans['seenByUser'] = in_array($trans['id'], $ids) || ($user->role == 'admin');*/

            $trans['seenByUser'] = Seen::where('tId', $trans['id'])->count() > 0;

            // If AU cards
            if ($trans['country'] == 'AU') {
                $overall = false;
                if (isset($trans['centrix_licence']) && isset($trans['centrix_licence']['IsDriverLicenceVerified'])) {
                    $overall = Centrix::boolean($trans['centrix_licence']['IsDriverLicenceVerified']);
                } else if (isset($trans['centrix_passport']) && isset($trans['centrix_passport']['IsPassportVerified'])) {
                    $overall = Centrix::boolean($trans['centrix_passport']['IsPassportVerified']);
                }
                $trans['Data'] = $overall;
            }

            if(Helper::hasCentrix($trans['country'])){
                // If NZ cards : default
                if($trans['centrix_smart_id'] && isset($trans['centrix_smart_id']['IsVerified'])){
                    $trans['Data'] = Centrix::boolean($trans['centrix_smart_id']['IsVerified']);
                }
            }else{
                $trans['Data'] = 'NA';
            }


            if($trans['cards'] && isset($trans['cards']['asf'])){
                $asf = json_decode($trans['cards']['asf'], true);
                $trans['Doc'] = isset($asf['overall']) ? Centrix::boolean($asf['overall']) : false;
            }

            if($trans['face_scan'] && isset($trans['face_scan']['confidence'])){
                $trans['Face'] = Centrix::boolean($trans['face_scan']['confidence']);
            }

            if($trans['liveness'] && !empty($trans['liveness'])){
                $trans['Live'] = Centrix::boolean($trans['liveness'][count($trans['liveness']) - 1]['result']);
            }

            $pd = $trans['user_details'];

            //---
            if (isset($trans['completed_at']) && !empty($trans['completed_at'])) {
                $trans['completed_at'] = date('d/m/Y H:i A', strtotime($trans['completed_at']));
            }

            if (isset($pd['firstName']) && !empty($pd['firstName'])) {
                $trans['user_details']['firstName'] = ucwords(strtolower($pd['firstName']));
            }

            if (isset($pd['middleName']) && !empty($pd['middleName'])) {
                $trans['user_details']['middleName'] = ucwords(strtolower($pd['middleName']));
            }

            if (isset($pd['lastName']) && !empty($pd['lastName'])) {
                $trans['user_details']['lastName'] = ucwords(strtolower($pd['lastName']));
            }
            //----

            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $trans['created_at']);
            //$to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2019-03-15 12:00:00');
            $from = now();

            $diff = $to->diff($from);

            $y = $diff->y;
            $m = $diff->m;
            $d = $diff->d;
            $h = $diff->h;
            $min = $diff->i;

            if($y > 0){
                $time_elapsed = $y.' '.str_plural('year', $y).' '.$m.' '.str_plural('month', $m);
            }else{

                $week = floor($d / 7);
                $remainingDays = $d % 7;

                if($m > 0){
                    $time_elapsed = $m.' '.str_plural('month', $m).' '.$week.' '.str_plural('week', $week);
                }else{

                    if($week > 0) {
                        $time_elapsed = $week.' '.str_plural('week', $week).' '.$remainingDays.' '.str_plural('day', $remainingDays);
                    }else{

                        if($remainingDays > 0){
                            $time_elapsed = $remainingDays.' '.str_plural('day', $remainingDays).' '.$h.' '.str_plural('hour', $h);
                        }else{
                            if($remainingDays > 0){
                                $time_elapsed = $h.' '.str_plural('hour', $h).' '.$min.' '.str_plural('min', $min);
                            }else{
                                $time_elapsed = $min.' '.str_plural('min', $min);
                            }
                        }
                    }


                }

            }


            //$trans['time_elapsed_raw'] = $diff;
            $trans['time_elapsed'] = $time_elapsed;

            if(Helper::hasCentrix($trans['country'])){
                $trans['result'] = $trans['Data'] && $trans['Face'] && $trans['Live'] && $trans['Doc'];
            }else{
                $trans['result'] = $trans['Face'] && $trans['Live'] && $trans['Doc'];
            }


            if(!empty($inputs['filter']['result']) && $inputs['filter']['result'] == 'completed-flagged'){
                $isPass = $trans['Doc'] && $trans['Face'] && $trans['Live'];
                if(Helper::hasCentrix($trans['country'])){
                    $isPass = $isPass && $trans['Data'];
                }
                if(!$isPass){
                    $new_data[] = $trans;
                }
            }else{
                $new_data[] = $trans;
            }

        }

        if (!Permission::hasPermission('customer-repository', 'view-passed-records')){
            $new_data = array_filter($new_data, function ($trans){
                return !$trans['result'];
            });
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $paginator = new LengthAwarePaginator($new_data, count($new_data), 20, $currentPage);

        return response()->json($paginator);
    }

    public function getPendingTransactions( Request $request ) {

        $inputs = $request->all();
        $inputs['filter'] = array_filter(json_decode($inputs['filter'],true));
        $inputs['query'] = array_filter(json_decode($inputs['query'],true));
        //--------------------------------

        $transactions = Transactions::where('status', '!=', 'COMPLETED')->where('status', '!=', 'FAILED')->with([
            'contactDetails:tId,phone,email,name',
        ])->orderBy('created_at','desc');

        //----------------------------------------
        //----------------------------------------
        if(!empty($inputs['query']) && isset($inputs['query']['term'])){

            $term = Helper::xss_patch_input($inputs['query']['term']);

            $transactions = $transactions->where('transactionId', 'like',  '%' . $term . '%');
            $transactions = $transactions->orWhere('reference', 'like',  '%' . $term . '%');

        } elseif($inputs['filter'] && !empty($inputs['filter'])){

            foreach ($inputs['filter'] as $k => $v){
                if($v){
                    if($k == 'dateFrom' || $k == 'dateTo'){

                        if($k == 'dateTo'){
                            $transactions = $transactions->whereDate('created_at','<=',Helper::xss_patch_input($v));
                        }

                        if($k == 'dateFrom'){
                            $transactions = $transactions->whereDate('created_at','>=',Helper::xss_patch_input($v));
                        }

                    }else{
                        $transactions = $transactions->where($k,Helper::xss_patch_input($v));
                    }

                }
            }
        }
        //------------------------------------------------
        //------------------------------------------------

        $transactions = $transactions->paginate(50)->toArray();

        $data = $transactions['data'];
        $new_data = [];
        foreach ($data as $trans){
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $trans['created_at']);
            //$to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2019-03-15 12:00:00');
            $from = now();

            $diff = $to->diff($from);

            $y = $diff->y;
            $m = $diff->m;
            $d = $diff->d;
            $h = $diff->h;
            $min = $diff->i;

            if($y > 0){
                $time_elapsed = $y.' '.str_plural('year', $y).' '.$m.' '.str_plural('month', $m);
            }else{

                $week = floor($d / 7);
                $remainingDays = $d % 7;

                if($m > 0){
                    $time_elapsed = $m.' '.str_plural('month', $m).' '.$week.' '.str_plural('week', $week);
                }else{

                    if($week > 0) {
                        $time_elapsed = $week.' '.str_plural('week', $week).' '.$remainingDays.' '.str_plural('day', $remainingDays);
                    }else{

                        if($remainingDays > 0){
                            $time_elapsed = $remainingDays.' '.str_plural('day', $remainingDays).' '.$h.' '.str_plural('hour', $h);
                        }else{
                            if($remainingDays > 0){
                                $time_elapsed = $h.' '.str_plural('hour', $h).' '.$min.' '.str_plural('min', $min);
                            }else{
                                $time_elapsed = $min.' '.str_plural('min', $min);
                            }
                        }
                    }


                }

            }

            if (isset($trans['created_at']) && !empty($trans['created_at'])) {
                $trans['created_at'] = date('d/m/Y H:i A', strtotime($trans['created_at']));
            }

            //$trans['time_elapsed_raw'] = $diff;
            $trans['time_elapsed'] = $time_elapsed;
            $trans['username'] = $trans['username'] == env('BASIC_AUTH') ? 'API' : $trans['username'];
            $new_data[] = $trans;
        }

        $transactions['data'] = $new_data;

        return response()->json($transactions);
    }

    public function prepareTransactionRawStatistics($result){
        //Activate time = (time when user open sms link) - (time when SMS was sent)
        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $result->statistics->created_at);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $result->created_at);

        $diff = $to->diff($from)->format('%d day %H hour %I min %S sec');
        $diff = str_replace('0 day','',$diff);
        $diff = str_replace('00 hour','',$diff);
        $diff = str_replace('00 min','',$diff);
        $diff = str_replace('00 sec','',$diff);

        $statistics = [
            'activate_time' => $diff
        ];
        //-----|-|--|-----------------
        // Time spend on review terms Screen
        $time = ceil($result->statistics->review_terms);
        $mins = floor($time / 60 % 60);
        $secs = floor($time % 60);
        $sf = '';

        if ($mins) {
            $sf .= $mins.' min ';
        }
        if ($secs) {
            $sf .= $secs.' sec';
        }

        $statistics['review_terms'] = $sf;
        //-----|-|--|-----------------
        // Capture time: time to capture details of a card
        $time = ceil($result->statistics->capture_id);
        $mins = floor($time / 60 % 60);
        $secs = floor($time % 60);
        $sf = '';

        if ($mins) {
            $sf .= $mins.' min ';
        }
        if ($secs) {
            $sf .= $secs.' sec';
        }

        $statistics['capture_id'] = $sf;
        //-----|-|--|-----------------
        // Review data: time taken on review data screen
        $time = ceil($result->statistics->review_data);
        $mins = floor($time / 60 % 60);
        $secs = floor($time % 60);
        $sf = '';

        if ($mins) {
            $sf .= $mins.' min ';
        }
        if ($secs) {
            $sf .= $secs.' sec';
        }

        $statistics['review_data'] = $sf;
        //-----|-|--|-----------------
        //Time taken to capture liveness
        $time = ceil($result->statistics->liveness);
        $mins = floor($time / 60 % 60);
        $secs = floor($time % 60);
        $sf = '';

        if ($mins) {
            $sf .= $mins.' min ';
        }
        if ($secs) {
            $sf .= $secs.' sec';
        }

        $statistics['liveness'] = $sf;
        //-------------------------
        $total_time = ceil($result->statistics->review_terms) +
            ceil($result->statistics->capture_id) +
            ceil($result->statistics->	review_data) +
            ceil($result->statistics->	liveness);
        $mins = floor($total_time / 60 % 60);
        $secs = floor($total_time % 60);
        $sf = '';

        if ($mins) {
            $sf .= $mins.' min ';
        }
        if ($secs) {
            $sf .= $secs.' sec';
        }

        $statistics['total'] = $sf;
        //-------------------------

        return $statistics;
    }

    public function transactionDetails($id){

        try {

            $tr  = Transactions::findOrFail($id);
            if (!$tr) {
                return response()->json([
                    'status' => 'error',
                    'msg'    => 'Transaction not found.',
                ], 404);
            }
            //transaction ids
            $ids = Transactions::where('status', 'COMPLETED')->orderBy('completed_at','desc')->pluck('id')->toArray();

            // Determine status

            $status['Data'] = false;
            $status['Asf'] = false;
            $status['Live'] = false;
            $status['Face'] = false;


            if(Helper::hasCentrix($tr->country)){
                if($tr->centrixSmartId && $tr->centrixSmartId->IsVerified){
                    $status['Data'] = Centrix::boolean($tr->centrixSmartId->IsVerified);
                }
            }else{
                $status['Data'] = 'NA';
            }

            if($tr->cards && isset($tr->cards->asf)){
                $asf = json_decode($tr->cards->asf, true);
                $status['Asf'] = isset($asf->overall) ? Centrix::boolean($asf->overall) : false;
            }

            if($tr->faceScan && isset($tr->faceScan->confidence)){
                $status['Face'] = Centrix::boolean($tr->faceScan->confidence);
            }

            if($tr->liveness && !empty($tr->liveness->toArray())){
                $status['Live'] = Centrix::boolean($tr->liveness[count($tr->liveness ) - 1]->result);
            }

            $transStatus = ($status['Face'] && $status['Live'] && $status['Face'] && $status['Data']) ? 'Completed - Pass' : 'Completed - Flagged';

            //If user does not have the permission to view Passed record, then throw error
            if(!Permission::hasPermission('customer-repository', 'view-passed-records') && $transStatus == 'Completed - Pass'){
                $response['status'] = 'error';
                $response['message'] = 'Permission Denied';
                return response()->json($response);
            }

            $result = [
                'status'     => $transStatus,
                'token'      => $tr->transactionId,
                'flow_type'  => $tr->flow_type,
                'reference'  => $tr->reference,
                'username'   => $tr->username,
                'country'    => $tr->country,
                'id_type'    => (strpos($tr->identify_type, 'LICENCE') !== false) ? 'driver_licence' : strtolower($tr->identify_type),
                'created_at' => $tr->created_at->format('d/m/Y H:i A'),
                'completed_at' => $tr->completed_at->format('d/m/Y H:i A'),
                'details'    => [
                    'card'      => [],
                    'personal'  => [],
                    'extracted' => [],
                    'contact'   => [],
                    'face_scan' => [],
                    'device'    => [],
                ],
            ];

            if(Helper::hasCentrix($tr->country)){
                $result['details']['centrix'] = [
                    'provider'  => 'centrix',
                    'type'      => $tr->country == 'AU' ? 'IDDocumentAU' : 'SmartID',
                    'overall'   => false,
                    'reference' => $tr->token,
                    'result'    => [],
                ];
            }

            if ($tr->cards) {
                $asf                       = json_decode($tr->cards->asf, true);
                $Asf = [];
                if(isset($asf) && isset($asf['overall'])){
                    $n = 0;
                    foreach ($asf as $k => $v){
                        if($k != 'overall'){
                            $Asf['check_'.$n] = $v;
                        }else{
                            $Asf[$k] = $v;
                        }

                        $n++;
                    }
                }

                $edited = $tr->cards->edited;

                $result['details']['card'] = [
                    'asf_check' => [
                        'document_authenticity' => isset($asf) && isset($asf['overall']) ? $asf['overall'] : false,
                        'result'                => $Asf,
                    ],
                    'edited' => (!empty($edited)) ? explode(',', $edited) : [],
                    'id_capture_count' => $tr->sessions()->whereIn('type', ['NZL_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT'])->count()
                ];

                if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){

                    $result['details']['card']['images'] = [
                        'front'     => Utils::getFullURL($tr->token, 'images/cropped/front', $tr->cards->front_card),
                        'back'      => Utils::getFullURL($tr->token, 'images/cropped/back', $tr->cards->back_card),
                        'face'      => Utils::getFullURL($tr->token, 'images/cropped/face', $tr->cards->face_photo),
                        'uncropped' => [
                            'front' => Utils::getFullURL($tr->token, 'images/uncropped/front', $tr->cards->front_card),
                            'back'  => Utils::getFullURL($tr->token, 'images/uncropped/back', $tr->cards->back_card),
                        ],
                    ];

                }else{

                    $front = $tr->cards->front_card;
                    $back  = $tr->cards->back_card;
                    $face = $tr->cards->face_photo;

                    $frontFull = str_replace('/crop/','/', $front);
                    $backFull = ($back) ? str_replace('/crop/','/', $back): null;

                    $result['details']['card']['images'] = [
                        'front'     => strpos($front, 'IMG') !== false ? AwsS3::getObject($front) : Utils::getFullURL($tr->token, 'images/cropped/front', $front),
                        'back'      => strpos($back, 'IMG') !== false ? AwsS3::getObject($back) : Utils::getFullURL($tr->token, 'images/cropped/back', $back),
                        'face'      => strpos($face, 'IMG') !== false ? AwsS3::getObject($face) : Utils::getFullURL($tr->token, 'images/cropped/face', $face),
                        'uncropped' => [
                            'front' => strpos($front, 'IMG') !== false ? AwsS3::getObject($frontFull) : Utils::getFullURL($tr->token, 'images/uncropped/front', $front),
                            'back'  => strpos($back, 'IMG') !== false ? AwsS3::getObject($backFull) : Utils::getFullURL($tr->token, 'images/uncropped/back', $back),
                        ],
                    ];
                }
            }

            if ($tr->extracted) {
                $result['details']['extracted'] = $tr->extracted;

                $ed = $result['details']['extracted'];
                if (isset($ed['dateOfBirth']) && !empty($ed['dateOfBirth'])) {
                    $result['details']['extracted']['dateOfBirth'] = date('d/m/Y', strtotime($ed['dateOfBirth']));
                }

                if (isset($ed['expiryDate']) && !empty($ed['expiryDate'])) {
                    $result['details']['extracted']['expiryDate'] = date('d/m/Y', strtotime($ed['expiryDate']));
                }

                if (isset($ed['firstName']) && !empty($ed['firstName'])) {
                    $result['details']['extracted']['firstName'] = ucwords(strtolower($ed['firstName']));
                }

                if (isset($ed['middleName']) && !empty($ed['middleName'])) {
                    $result['details']['extracted']['middleName'] = ucwords(strtolower($ed['middleName']));
                }

                if (isset($ed['lastName']) && !empty($ed['lastName'])) {
                    $result['details']['extracted']['lastName'] = ucwords(strtolower($ed['lastName']));
                }
            }

            if ($tr->userDetails) {
                $result['details']['personal'] = $tr->userDetails;

                $pd = $result['details']['personal'];

                $expired = false;
                if (!empty($result['details']['personal']['dateOfExpiry'])) {
                    $now     = new DateTime();
                    $date    = new DateTime($result['details']['personal']['dateOfExpiry']);
                    $expired = $now > $date;
                }

                $result['details']['personal']['document_expired'] = $expired;

                $age = 'unknown';
                if (!empty($result['details']['personal']['dateOfBirth'])) {
                    $now  = new DateTime();
                    $date = new DateTime($result['details']['personal']['dateOfBirth']);
                    $age  = $now->diff($date)->y;
                }
                $result['details']['personal']['age'] = $age;

                if (isset($pd['dateOfBirth']) && !empty($pd['dateOfBirth'])) {
                    $result['details']['personal']['dateOfBirth'] = date('d/m/Y', strtotime($pd['dateOfBirth']));
                }

                if (isset($pd['dateOfExpiry']) && !empty($pd['dateOfExpiry'])) {
                    $result['details']['personal']['dateOfExpiry'] = date('d/m/Y', strtotime($pd['dateOfExpiry']));
                }

                if (isset($pd['firstName']) && !empty($pd['firstName'])) {
                    $result['details']['personal']['firstName'] = ucwords(strtolower($pd['firstName']));
                }

                if (isset($pd['middleName']) && !empty($pd['middleName'])) {
                    $result['details']['personal']['middleName'] = ucwords(strtolower($pd['middleName']));
                }

                if (isset($pd['lastName']) && !empty($pd['lastName'])) {
                    $result['details']['personal']['lastName'] = ucwords(strtolower($pd['lastName']));
                }
            }

            if ($tr->contactDetails) {
                $result['details']['contact'] = $tr->contactDetails;

                if(Helper::isJson($result['details']['contact']['residential_address'])){
                    $result['details']['contact']['residential_address'] = json_decode($result['details']['contact']['residential_address'], true)['fullAddress'];
                }
            }

            if ($tr->info) {
                $result['details']['device']        = $tr->info;
                $result['details']['device']['vpn'] = Centrix::boolean($result['details']['device']['vpn']);
                $result['details']['location']['ip'] = (Helper::isJson($result['details']['device']['ip_location'])) ? json_decode($result['details']['device']['ip_location'], true) : ['coordinates'=> $result['details']['device']['ip_location']];
                $result['details']['location']['geolocation'] = (Helper::isJson($result['details']['device']['geolocation'])) ? json_decode($result['details']['device']['geolocation'], true) : ['coordinates'=> $result['details']['device']['geolocation']];
            }

            if ($tr->faceScan) {
                if ($tr->flow_type !== 'MAIN') {
                    $result['details']['face_scan'] = [
                        'confidence' => Centrix::boolean($tr->faceScan->confidence),
                        'features'   => json_decode($tr->faceScan->features, true),
                    ];

                    if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){

                        $result['details']['face_scan']['selfies'] = [
                            'face'      => Utils::getFullURL($tr->token, 'selfie/face', $tr->faceScan->video),
                            'turn_head' => Utils::getFullURL($tr->token, 'selfie/turn_head', $tr->faceScan->face),
                        ];

                    }else{
                        $result['details']['face_scan']['selfies'] = [
                            'face'      => strpos($tr->faceScan->video, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->video) : Utils::getFullURL($tr->token, 'selfie/face', $tr->faceScan->video),
                            'turn_head' => strpos($tr->faceScan->face, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->face) : Utils::getFullURL($tr->token, 'selfie/turn_head', $tr->faceScan->face),
                        ];
                    }

                } else {

                    $liveness = $tr->liveness()->limit(2)->get();
                    $liveness = [
                        'result' => Centrix::boolean($liveness[count($liveness) - 1]->result),
                    ];

                    foreach ($tr->liveness()->limit(2)->get() as $i => $live) {
                        $temp               = [];
                        $temp['result']     = Centrix::boolean($live->result);
                        $temp['spoof_risk'] = Centrix::boolean($live->spoof_risk);
                        $temp['actions']    = $live->actions;

                        if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){
                            $temp['video'] = Utils::getFullURL($tr->token, 'videos/' . $live->id, $live->video);
                        }else{
                            $temp['video']  = strpos($live->video, 'IMG') !== false ? AwsS3::getObject($live->video) : Utils::getFullURL($tr->token, 'videos/' . $live->id, $live->video);
                        }

                        $liveness['attempts'][] = $temp;

                    }

                    $result['details']['face_scan'] = [
                        'confidence' => Centrix::boolean($tr->faceScan->confidence),
                        'liveness'   => $liveness,
                        'features'   => json_decode($tr->faceScan->features, true),
                        'attempts'   => count($tr->liveness) ?? 1
                    ];

                    if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){
                        $result['details']['face_scan']['video'] = Utils::getFullURL($tr->token, 'videos', $tr->faceScan->video);
                        $result['details']['face_scan']['face'] = Utils::getFullURL($tr->token, 'videos/face', $tr->faceScan->face);
                    }else{
                        $result['details']['face_scan']['video'] = strpos($tr->faceScan->video, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->video) : Utils::getFullURL($tr->token, 'videos', $tr->faceScan->video);
                        $result['details']['face_scan']['face'] = strpos($tr->faceScan->face, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->face) : Utils::getFullURL($tr->token, 'videos/face', $tr->faceScan->face);
                    }

                    //assessments
                    if($tr->faceScan->liveness_assessments){
                        $result['details']['face_scan']['liveness_assessments'] = json_decode($tr->faceScan->liveness_assessments, true)['result'];
                    }else{
                        $result['details']['face_scan']['liveness_assessments'] = 'High Risk';
                    }
                }
            }

            if ($tr->country == 'AU' && Helper::hasCentrix($tr->country)) {
                $overall = false;
                if ($tr->centrixLicence) {
                    $overall = Centrix::boolean($tr->centrixLicence->IsDriverLicenceVerified);
                } else if ($tr->centrixPassport) {
                    $overall = Centrix::boolean($tr->centrixPassport->IsPassportVerified);
                }
                $result['details']['centrix']['overall'] = $overall;
            }

            if ($tr->centrixLicence && Helper::hasCentrix($tr->country)) {
                $result['details']['centrix']['result']['driver_licence'] = [
                    'is_verified'             => Centrix::boolean($tr->centrixLicence->IsDriverLicenceVerified),
                    'is_success'              => Centrix::boolean($tr->centrixLicence->IsSuccess),
                    'is_firstname_matched'    => Centrix::boolean($tr->centrixLicence->IsDriverLicenceFirstNameMatched),
                    'is_lastname_matched'     => Centrix::boolean($tr->centrixLicence->IsDriverLicenceLastNameMatched),
                    'is_middlename_matched'   => Centrix::boolean($tr->centrixLicence->IsDriverLicenceMiddleNameMatched),
                    'is_dateofbirth_matched'  => Centrix::boolean($tr->centrixLicence->IsDriverLicenceDateOfBirthMatched),
                    'is_verified_and_matched' => Centrix::boolean($tr->centrixLicence->IsDriverLicenceVerifiedAndMatched),
                ];
            }

            if ($tr->centrixPassport && Helper::hasCentrix($tr->country)) {
                $result['details']['centrix']['result']['passport'] = [
                    'is_verified' => Centrix::boolean($tr->centrixPassport->IsPassportVerified),
                    'is_success'  => Centrix::boolean($tr->centrixPassport->IsSuccess),
                ];
            }

            if ($tr->centrixPep && Helper::hasCentrix($tr->country)) {
                $result['details']['centrix']['result']['pep_watchlist'] = [
                    'is_success'                       => Centrix::boolean($tr->centrixPep->IsSuccess),
                    'international_watchlist_is_clear' => Centrix::boolean($tr->centrixPep->InternationalWatchlistIsClear),
                ];
            }

            if ($tr->centrixPep && $tr->centrixPep->ResponseDetails && Helper::hasCentrix($tr->country) && config('centrix.centrixId')) {
                $result['details']['centrix']['ResponseMeta'] = json_decode($tr->centrixPep->ResponseDetails, true);
            }

            if ($tr->centrixSmartId && Helper::hasCentrix($tr->country)) {
                $result['details']['centrix']['overall']            = Centrix::boolean($tr->centrixSmartId->IsVerified);
                $result['details']['centrix']['result']['smart_id'] = [
                    'is_verified'             => Centrix::boolean($tr->centrixSmartId->IsVerified),
                    'is_name_verified'        => Centrix::boolean($tr->centrixSmartId->IsNameVerified),
                    'is_dateofbirth_verified' => Centrix::boolean($tr->centrixSmartId->IsDateOfBirthVerified),
                    'is_address_verified'     => Centrix::boolean($tr->centrixSmartId->IsAddressVerified),
                ];
            }

            if ($tr->centrixSmartId && Helper::hasCentrix($tr->country) && $tr->centrixSmartId->first()->data) {
                foreach ($tr->centrixSmartId()->first()->data as $data) {
                    $result['details']['centrix']['result']['smart_id_data'][] = [
                        'name'                     => $data->DataSourceName,
                        'description'              => $data->DataSourceDescription,
                        'name_match_status'        => $data->NameMatchStatus,
                        'dateofbirth_match_status' => $data->DateOfBirthMatchStatus,
                        'address_match_status'     => $data->AddressMatchStatus,
                    ];
                }
            }

            if ($tr->centrixResidential && Helper::hasCentrix($tr->country)) {
                $IDR = $tr->centrixResidential()->first();

                $result['details']['centrix']['result']['residential'] = [
                    'is_success'                            => Centrix::boolean($IDR['IsSuccess']),
                    'message'                               => $IDR['Message'],
                    'residential_verified'                  => Centrix::boolean($IDR['ResidentialVerified']),
                    'third_party_match_type'                => $IDR['ThirdPartyMatchType'],
                    'third_party_match_type_description'    => $IDR['ThirdPartyMatchTypeDescription'],
                    'surname'                               => $IDR['Surname'],
                    'first_name'                            => $IDR['FirstName'],
                    'middle_name'                           => $IDR['MiddleName'],
                    'date_of_birth'                         => $IDR['DateOfBirth'],
                    'third_party_name_match'                => $IDR['ThirdPartyNameMatch'],
                    'third_party_name_match_description'    => $IDR['ThirdPartyNameMatchDescription'],
                    'surname_match'                         => Centrix::boolean($IDR['SurnameMatch']),
                    'first_name_match'                      => Centrix::boolean($IDR['FirstNameMatch']),
                    'first_name_initial_match'              => Centrix::boolean($IDR['FirstNameInitialMatch']),
                    'middle_name_match'                     => Centrix::boolean($IDR['MiddleNameMatch']),
                    'middle_name_initial_match'             => Centrix::boolean($IDR['MiddleNameInitialMatch']),
                    'date_of_birth_match'                   => Centrix::boolean($IDR['DateOfBirthMatch']),
                    'third_party_address_match'             => $IDR['ThirdPartyAddressMatch'],
                    'third_party_address_match_description' => $IDR['ThirdPartyAddressMatchDescription'],
                    'address_line1'                         => $IDR['AddressLine1'],
                    'suburb'                                => $IDR['Suburb'],
                    'state'                                 => $IDR['State'],
                    'postcode'                              => $IDR['Postcode'],
                    'delivery_point_id'                     => $IDR['DeliveryPointID'],
                    'unit_no_match'                         => Centrix::boolean($IDR['UnitNoMatch']),
                    'street_no_match'                       => Centrix::boolean($IDR['StreetNoMatch']),
                    'street_name_match'                     => Centrix::boolean($IDR['StreetNameMatch']),
                    'street_type_match'                     => Centrix::boolean($IDR['StreetTypeMatch']),
                    'suburb_match'                          => Centrix::boolean($IDR['SuburbMatch']),
                    'state_match'                           => Centrix::boolean($IDR['StateMatch']),
                    'postcode_match'                        => Centrix::boolean($IDR['PostcodeMatch']),
                    'home_phone_no'                         => $IDR['HomePhoneNo'],
                    'mobile_phone_no'                       => $IDR['MobilePhoneNo'],
                    'phone_number_match'                    => Centrix::boolean($IDR['PhoneNumberMatch']),
                    'mobile_number_match'                   => Centrix::boolean($IDR['MobileNumberMatch']),
                    'email_address'                         => $IDR['EmailAddress'],
                ];
            }

            if($tr->statistics){
                $result['details']['statistics'] = $this->prepareTransactionRawStatistics($tr);
            }else{
                $result['details']['statistics'] = [
                    'activate_time' => "NA",
                    'capture_id' => "NA",
                    'liveness' => "NA",
                    'review_data' => "NA",
                    'review_terms' => "NA",
                    'total' => "NA"
                ];
            }

            // Next and previous transaction ids
            if(!empty($ids) && Permission::hasPermission('customer-repository', 'back-forward-buttons')){
                $currentIndex =  array_search($id, $ids);
                if($currentIndex == 0 && count($ids) > 1){
                    //first element
                    $result['nextTrans'] = null;
                    $result['prevTrans'] = $ids[1];
                }elseif ($currentIndex == 0 && count($ids) === 1){
                    $result['nextTrans'] = null;
                    $result['prevTrans'] = null;
                } elseif ((count($ids) - 1) == $currentIndex){
                    //last element
                    $result['nextTrans'] = $ids[count($ids) - 2];
                    $result['prevTrans'] = null;
                }else{
                    $result['nextTrans'] = $ids[$currentIndex - 1];
                    $result['prevTrans'] = $ids[$currentIndex + 1];
                }
            }else{
                $result['nextTrans'] = null;
                $result['prevTrans'] = null;
            }

            $response['status'] = 'success';
            $response['data'] = $result;

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);
    }

    public function getUserStatus($id){

        try {
            $transaction = Transactions::findOrFail($id);

            $response['status'] = 'success';
            $response['data'] = $transaction->user_status;

        } catch (\Exception $e) {
            $response['status'] = 'error';
            //$response['message'] = $e->getMessage();
            $response['message'] = 'server error';
        }

        return response()->json($response);

    }

    public function validateGuided($id){

        try {
            $statuses = ['COMPLETED', 'FAILED'];
            $count = Transactions::where('id',$id)->whereNotIn('status', $statuses)->count();

            if($count > 0){
                $response['status'] = 'success';
            }else{
                $response['status'] = 'failed';
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            //$response['message'] = $e->getMessage();
            $response['message'] = 'server error';
        }

        return response()->json($response);

    }

    public function updateTransactionGuided(){
        $inputs = request()->all();
        $inputs = Helper::xss_patch_input($inputs);

        $validator = Validator::make($inputs, [
            'id' => 'required',
            'approval' => 'integer',
            'action'=>''
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'status' => 'failed',
                'message' => $validator->getMessageBag()->toArray()
            ]);

        }

        try{

            $transaction = Transactions::find($inputs['id']);

            if($transaction){

                if($inputs['action'] == 'submit'){
                    //
                    $transaction->status = 'COMPLETED';
                }elseif ($inputs['action'] == 'redo'){
                    //
                    $transaction->status = 'REDO';
                    $transaction->user_status = 'waiting';

                }elseif ($inputs['action'] == 'cancel'){
                    //
                    $transaction->status = 'CANCELLED';
                    $transaction->user_status = 'cancelled';
                }

                $transaction->save();

                return response()->json([
                    'status' => 'success',
                ]);
            }else{
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Record not found'
                ]);
            }

        }catch (\Exception $e){
            return response()->json([
                'status' => 'failed',
                'message' => $e->getMessage()
            ]);
        }

    }

    public function getCards($id){

        try {
            $transaction = Transactions::findOrFail($id);
            $response = [
                'status' => 'success',
                'data' => null,
            ];

            if ($transaction->cards) {

                if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){
                    $response['data']['front'] = Utils::getFullURL($transaction->token, 'images/cropped/front', $transaction->cards->front_card);

                    if($transaction->cards->back_card){
                        $response['data']['back'] = Utils::getFullURL($transaction->token, 'images/cropped/back', $transaction->cards->back_card);
                    }
                }else{
                    $front = $transaction->cards->front_card;
                    $response['data']['front'] = strpos($front, 'IMG') !== false ? AwsS3::getObject($front) : Utils::getFullURL($transaction->token, 'images/cropped/front', $front);

                    if($transaction->cards->back_card){
                        $back  = $transaction->cards->back_card;
                        $response['data']['back'] = strpos($back, 'IMG') !== false ? AwsS3::getObject($back) : Utils::getFullURL($transaction->token, 'images/cropped/back', $back);
                    }
                }
            }

        } catch (\Exception $e) {
            $response['status'] = 'failed';
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);

    }

    public function approveCards(Request $request){

        $inputs = $request->all();
        $rules = [
            'id' => 'required',
            'status' => 'required|integer',
        ];
        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            $response['status'] = 'error';
            $response['message'] = 'One or more field values are not valid.';
            return response()->json($response);
        }

        try {
            $transaction = Transactions::findOrFail($inputs['id']);

            $transaction->cards()->update([
                'approved'=> $inputs['status']
            ]);
            $response = [
                'status' => 'success',
            ];


        } catch (\Exception $e) {
            $response['status'] = 'failed';
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);

    }

    public function deleteTransaction($id){

        try {

            $transaction = Transactions::where('id', $id)->first();
            if(
                ($transaction->status == 'COMPLETED' && !Permission::hasPermission('customer-repository', 'delete-records')) ||
                ($transaction->status != 'COMPLETED' && !Permission::hasPermission('pending-transactions', 'delete-records'))
            ){
                $response['status'] = 'error';
                $response['message'] = 'Permission Denied.';
                return response()->json($response);
            }

            $transaction->forceDelete($id);

            $response['status'] = 'success';

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);
    }

    public function myLogout(){
        Auth::logout();
        $response['status'] = 'success';
        return response()->json($response);
    }

    public static function updateStatistics($data)
    {
        $validator = Validator::make($data, [
            'column' => 'required',
            'ts'     => 'required',
            'tId'    => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'failed',
                'message' => $validator->getMessageBag()->toArray(),
            ]);

        }

        try {

            $inputs     = $data;
            $statistics = Statistics::where('tId', $data['tId'])->firstOrFail();

            if (!$statistics) {
                $statistics      = new Statistics();
                $statistics->tId = $data['tId'];
            }

            $statistics->{$inputs['column']} = $inputs['ts'];
            //----------------------------------

            $statistics->save();

            return response()->json([
                'status' => 'success',
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status'  => 'failed',
                'message' => $e->getMessage(),
            ]);
        }

    }

    public function updateTransactions(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'action' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'status' => 'failed',
                'error' => $validator->getMessageBag()->toArray()
            ]);
        }

        try{
            $inputs = Helper::xss_patch_input($request->all());

            $model = Transactions::findOrFail($inputs['id']);

            if($inputs['action'] == 'cancel'){
                $model->status = 'CANCELLED';
                $model->user_status = 'cancelled';
                $model->save();

                return response()->json([
                    'status' => 'success',
                ]);
            }elseif ($inputs['action'] == 'resend'){

                $result =  $this->resendSMS($model->id);
                //-----------------------------------------

                return response()->json($result);
            }

        }catch (\Exception $e){
            return response()->json([
                'status' => 'failed',
                'error' => $e->getMessage()
            ]);
        }
    }

    protected function resendSMS($id){

        try{

            $oldTransaction = Transactions::findOrFail($id);

            // Delete Statistics & info
            $oldTransaction->info()->delete();
            $oldTransaction->statistics()->delete();

            $oldContactDetails = $oldTransaction->contactDetails;
            $oldTransaction->update([
                'status' => 'CANCELLED',
                'user_status' => 'cancelled',
            ]);

            $transaction = new Transactions();
            $transaction->transactionId  = $oldTransaction->transactionId;
            $transaction->token          = $oldTransaction->token;
            $transaction->category       = $oldTransaction->category;
            $transaction->module         = $oldTransaction->module;
            $transaction->instruction_type  = $oldTransaction->instruction_type;
            $transaction->joint_session   = $oldTransaction->joint_session;
            $transaction->username       = $oldTransaction->username;
            $transaction->user_status    = 'waiting';
            $transaction->reference    = $oldTransaction->reference;

            $transaction->original_token = $oldTransaction->original_token;
            $transaction->created_at     = now();
            $transaction->save();

            //$liveness = new Token();
            //$liveness->createSession( $transaction->token);

            $transaction->contactDetails()->create([
                'name' => $oldContactDetails->name ?? null,
                'phone' => $oldContactDetails->phone ?? null,
                'email' => $oldContactDetails->email ?? null,
            ]);

            $url_inputs = [
                'return_url' => $request->return_url ?? route('mobileStatus', [ 'status' => 'completed']),
                'cancel_url' => $request->cancel_url ?? route('mobileStatus', [ 'status' => 'cancelled']),
            ];

            $expiry = config('portal.sms_expiry');

            if($expiry < 1){
                $expiry = ceil(60 * $expiry);
                $url_inputs['expiry'] = now()->addMinutes($expiry);
            }else{
                $url_inputs['expiry'] = now()->addHours($expiry);
            }

            $transaction->urls()->create($url_inputs);


            $applicant = [
                'email' => $oldTransaction->contactDetails->email,
                'mobile' => $oldTransaction->contactDetails->phone,
                'token' => $oldTransaction->token,
                'id' => $transaction->id,
            ];

            if($oldTransaction->contactDetails->email){
                //send email
                $applicant['medium'] = 'email';
            }else{
                //send sms
                $applicant['medium'] = 'sms';
            }

            $response = (new InvitationController)->invite($applicant);

        }catch (\Exception $e){

            $response['status'] = 'failed';
            $response['message'] = 'Failed while resending';

        }

        return $response;
    }

    public function download(Request $request)
    {
        /*$s = new TransactionSerialiser($request->from);
        $trs = Transactions::with([
            'contactDetails',
            'userDetails',
            'liveness',
            'centrixSmartId',
            'centrixLicence',
            'centrixPassport',
            'cards:tId,asf',
            'faceScan',
            'statistics',
            'info',
        ])
            ->orderBy('created_at', 'DESC')->where('status', '!=','COMPLETED')->get();

        foreach ($trs as $tr){
            $data = $s->getData($tr);
            echo '<pre>';
            print_r($s->getHeaderRow());
            echo '</pre>';
            echo '<pre>';
            print_r($data);
            echo '</pre>';
            die('<hr/>here');
        }*/
        $inputs = Helper::xss_patch_input($request->all());

        if (
            ($inputs['from'] == "completed" && !Permission::hasPermission('customer-repository', 'export-records')) ||
            $inputs['from'] != "completed" && !Permission::hasPermission('pending-transactions', 'export-records')
        ){
            return response()->json([
               'status' => 'failed',
               'message' => 'Permission denied.'
            ]);
        }


        $types = [
            'excel' => '.xlsx',
            'csv' => '.csv',
        ];
        $s = new TransactionSerialiser($inputs['from']);

        if($inputs['export'] == 'excel'){
            $excel = Exporter::make("Excel");
        }else{
            $excel = Exporter::make("Csv");
        }


        if ($inputs['from'] == "completed") {
            $excel->load(Transactions::with([
                'contactDetails',
                'userDetails',
                'liveness',
                'centrixSmartId',
                'centrixLicence',
                'centrixPassport',
                'cards:tId,asf',
                'faceScan',
                'statistics',
                'info',
            ])
                ->orderBy('created_at', 'DESC')->where('status', '=','COMPLETED')->get());
        }else if($inputs['from'] == "incomplete"){
            $excel->load(Transactions::with([
                'contactDetails',
                'userDetails',
                'liveness',
                'centrixSmartId',
                'centrixLicence',
                'centrixPassport',
                'cards:tId,asf',
                'faceScan',
                'statistics',
                'info',
            ])->where('status', '!=', 'COMPLETED')->orderBy('created_at', 'DESC')->get());
        }
        $excel->setSerialiser($s);
        $fileName = strval('records_'.date('y-m-d').'_'.$inputs['from']);
        $excel->stream($fileName.$types[$inputs['export']]);
    }

    public function getUserCountry(){
        $file = file_get_contents('http://api.ipstack.com/' . TokenController::ip() . '?access_key=0a3ac3f97b1b2fcb1a25d10e4dca4306&format=1');
        $json = json_decode($file);
        return response()->json($json);
    }
}
