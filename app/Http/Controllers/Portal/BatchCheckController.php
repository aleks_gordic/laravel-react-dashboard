<?php

namespace App\Http\Controllers\Portal;

use App\Http\Models\Portal\Core\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use Excel;

use App\Helpers\Helper;
use App\Http\Models\BatchCheck\BatchCheck;
use App\Http\Models\BatchCheck\BatchSms;
use App\Http\Models\BatchCheck\BatchEmail;
use App\Http\Models\Transactions\Transactions;

class BatchCheckController extends Controller
{

    /**
     * BatchCheckController constructor.
     */
    public function __construct()
    {
        if(Permission::hasPermission('batch-checking')){
            $response['status'] = 'error';
            $response['message'] = 'Permission Denied';
            return response()->json($response);
        }
    }

    public function getDownload($isSms)
    {
        $bSms = $isSms === 'true'? true: false;
        $fileName = 'EmailTemplate.xlsx';
        
        if ($bSms) {
            $fileName = 'SmsTemplate.xlsx';
        }

        return Storage::disk('batch')->download($fileName);
    }

    public function processBatchFile(Request $request) {
        $inputs = $request->all();
        $rules = [
            'filePath' => 'required',
            'isSms' => 'required',
            'batchName' => 'required'
        ];

        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            $response['status'] = 'error';
            $response['message'] = 'One or more field values are not valid.';
            return response()->json($response);
        }

        $filePath = 'excels/' . $inputs['filePath'];
        if(Storage::exists($filePath)){
            $path = Storage::disk('local')->path($filePath);
            $basePath = base_path();
            $subPath = substr($path, strlen($basePath));
            $data = Excel::load($subPath)->get();

            if(!empty($data) && $data->count())
            {
                $batchCheck = new BatchCheck();
                $batchCheck->name = $inputs['batchName'];
                $batchCheck->records = $data->count();
                if ($inputs['isSms']) {
                    $batchCheck->method = 1;
                } else {
                    $batchCheck->method = 0;
                }
                $batchCheck->save();

                foreach ($data->toArray() as $key => $value) {
                    if(!empty($value))
                    { 
                        if ($inputs['isSms']) 
                        {
                            $batchSms = new BatchSms();
                            $batchSms->bId = $batchCheck->id;
                            $batchSms->tId = 0;
                            //$batchSms->region = $value['region'];
                            $batchSms->contact = $value['contact'];
                            $batchSms->reference = $value['reference'];
                            $batchSms->save();
                        } 
                        else 
                        {
                            $batchEmail = new BatchEmail();
                            $batchEmail->bId = $batchCheck->id;
                            $batchEmail->tId = 0;
                            $batchEmail->contact = $value['contact'];
                            $batchEmail->reference = $value['reference'];
                            $batchEmail->save();
                        }
                    }
                }

                $response['status'] = 'success';
                $result = ['batchId' => $batchCheck->id]; 
                $response['data'] = $result;
            }
            else {
                $response['status'] = 'error';
                $response['message'] = 'Can not import excel file.';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Can not find uploaded excel file.';
        }

        return response()->json($response);
    }

    public function upload(Request $request)
    {
        $path = $request->file('excel')->store('excels');
        return basename($path);
    }

    public function getBatches() {
        $batchCheckList = BatchCheck::all();
        foreach ($batchCheckList as $batchCheck) {
            if ($batchCheck->method == 1) {
                $transactions = Transactions::join('batch_sms', function ($join) use ($batchCheck) {
                    $join->on('transactions.id', '=', 'batch_sms.tid')
                         ->where('batch_sms.bid', '=', $batchCheck->id);
                })
                ->select('transactions.status', 'transactions.transactionId', 'batch_sms.*')
                ->get();
                $batchCheck['transactions'] = $transactions;
            } 
            // else {
            //     transactions =Transactions::join('batch_email', function ($join) {
            //         $join->on('transactions.id', '=', 'batch_email.tid')
            //              ->where('batch_email.bid', '=', $batchCheck->id);
            //     })
            //     ->get();
            // }
        }

        Log::debug($batchCheckList);

        $response['status'] = 'success';
        $response['data'] = $batchCheckList;

        return response()->json($response);
    }

    public function getDetails( Request $request ) {
        $inputs = $request->all();
        $inputs['filter'] = array_filter(json_decode($inputs['filter'],true));
        $details = [];

        if(!empty($inputs['filter']) && isset($inputs['filter']['batchId']) && isset($inputs['filter']['isSms'])){
            $batchId = Helper::xss_patch_input($inputs['filter']['batchId']);
            $isSms = Helper::xss_patch_input($inputs['filter']['isSms']);
            
            if ($isSms == 1) {
                $details = BatchSms::where('bId', '=', $batchId);
            } else {
                $details = BatchEmail::where('bId', '=', $batchId);    
            }
            
            $details = $details->paginate(50)->toArray();
        }

        return response()->json($details);
    }
}
