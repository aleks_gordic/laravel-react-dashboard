<?php

namespace App\Http\Controllers\Portal\Core;

use App\Helpers\Helper;
use App\Http\Models\Portal\Core\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use stdClass;

class PermissionController extends Controller
{
    public $data;
    public $settings;

    public function __construct()
    {
        $this->data = new stdClass();
        $this->settings = Permission::getSettings();
        $this->data->input = (object)request()->all();
        $this->data->input = Helper::xss_patch_input($this->data->input);

        $this->data->model = $this->settings->model;
        $this->data->rows = $this->settings->rows;
        $this->data->settings = $this->settings;
    }

    //------------------------------------------------------
    function create()
    {

        /*if (!Permission::check('create-'.$this->settings->prefix)) {
            $error_message = "You don't have permission create";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;
        $response = $model::store();
        return response()->json($response);

    }

    //------------------------------------------------------
    function read($id=null)
    {

        /*if (!Permission::check('read-'.$this->settings->prefix)) {
            $error_message = "You don't have permission read";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;
        if ($id) {
            $item = $model::where('id', $id)->first();
        } else {
            $item = $model::paginate($this->data->rows);
        }

        if ($item) {
            $response['status'] = 'success';
            $response['data'] = $item;
        } else {
            $response['status'] = 'error';
            $response['messages'] = 'Not found';
        }

        return response()->json($response);
    }

    //------------------------------------------------------
    function update(Request $request)
    {
        /*if (!Permission::check('update-'.$this->settings->prefix)) {
            $error_message = "You don't have permission update";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $inputs = $request->all();
        $model = $this->data->model;
        $exists = $model::where('module_id',$inputs['module_id'])->where('role_id',$inputs['role_id']);

        if($exists->first()){
            $exists->update([
                'access' =>$inputs['access']
            ]);
            $response = [
                'status' => 'success',
                'data' => $exists->first()
            ];
        }else{
            $model = $this->data->model;
            $response = $model::store();
        }

        return response()->json($response);
    }

    //------------------------------------------------------
    function delete()
    {
        /*if (!Permission::check('delete-'.$this->settings->prefix)) {
            $error_message = "You don't have permission delete";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        try{
            $model = $this->data->model;
            $model::where('id',$this->data->input->pk)->delete($this->data->input->pk);
            return response()->json(['status'=>'success']);

        }catch (\Exception $e){
            return response()->json(['status'=>'error','message' => 'Error while deleting a record']);
        }

    }
    //-------------------------------
    function updateBulk(){

        $inputs = request()->all();
        $inputs = Helper::xss_patch_input($inputs);

        $user_id = $inputs['user_id'];

        $test = [];
        foreach ($inputs['modules'] as $module){
            $in = [
                'module_id' => $module['id'],
                'access' => ($module['access']) ? 1 : 0,
                'user_id' => $user_id,
            ];

            $model = $this->data->model;

            $response = $model::store($in);
            $test[] = $response;
        }

        return response()->json([
            'status'=>'success',
            'data' => $test
        ]);
    }
}

