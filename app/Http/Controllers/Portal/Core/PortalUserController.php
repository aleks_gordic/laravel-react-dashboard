<?php

namespace App\Http\Controllers\Portal\Core;

use App\Helpers\Helper;
use App\Http\Models\Portal\Core\Module;
use App\Http\Models\Portal\Core\Permission;
use App\Http\Models\Portal\Core\PortalUser;
use App\Http\Models\Transactions\Seen;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DateTime;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Auth\LoginController;

use Illuminate\Support\Facades\Storage;
use stdClass;
use App\Notifications\NewUserCreate; // Or the location that you store your notifications (this is default).

use Illuminate\Notifications\Notifiable;
use PragmaRX\Google2FA\Google2FA;

class PortalUserController extends Controller
{
    /**
     * url will be http://localhost/api/portal/test1
     * Display a listing of the resource.
     * @return Response
     */

    use Notifiable;
    public $data;
    public $settings;

    public function __construct()
    {
        $this->data = new stdClass();
        $this->settings = PortalUser::getSettings();
        $this->data->input = (object)request()->all();
        $this->data->input = Helper::xss_patch_input($this->data->input);

        $this->data->model = $this->settings->model;
        $this->data->rows = $this->settings->rows;
        $this->data->settings = $this->settings;

        if(Permission::hasPermission('user')){
            $response['status'] = 'error';
            $response['message'] = 'Permission Denied';
            return response()->json($response);
        }
    }

    //------------------------------------------------------
    function create()
    {

        /*if (!Permission::check('create-'.$this->settings->prefix)) {
            $error_message = "You don't have permission create";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/
        $request = request()->all();

        $model = $this->data->model;

        $response = $model::store();
        \Log::info($request);
        if ($response['status'] == "success") {
            $this->createNewUser($request);
        }
        return response()->json($response);
    }

    public function createNewUser($request)
    {
        $user = User::where('email', $request['email'])->first();
        $token = str_random(32);

        $user->account_token = $token;
        $user->save();
        $user->notify(new NewUserCreate($token, $user->email, $request['fullName']));
        // Auth::guard('web')->login($user);
    }

    public function getCurrentUser()
    {
        $response['status'] = 'success';
        $user1 = Auth::user()->toArray();
        $user1['permissions'] = (new LoginController)->getUserPermission();
        $response['data'] = $user1;

        return response()->json($response);
    }

    public function resetPassword(Request $request)
    {
        $emailAddress = $request['email'];

        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject('Password Reset');
        });

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse('success')
            : $this->sendResetLinkResponse('failed');
    }

    public function resetTwoStep(Request $request)
    {
        $id = $request['id'];
        $user = User::where('id', $id)->first();
        $user->mobile = null;
        $user->google_secret = null;
        $user->sms_token = null;
        $user->account_token = null;
        $user->save();

        $response['status'] = 'success';
        return response()->json($response);
    }

    public function sendResetLinkResponse($result)
    {
        $response['status'] = $result;
        return response()->json($response);
    }

    //------------------------------------------------------
    function read($id = null)
    {

        /*if (!Permission::check('read-'.$this->settings->prefix)) {
            $error_message = "You don't have permission read";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;
        if ($id) {
            $item = $model::where('id', $id)->first();
            $item->permissions = $this->getUserPermission($id);
        } else {

            $show = request()->input('show');
            if (isset($show) && $show == 'all') {
                $item = $model::whereNotIn('username', [env('BASIC_AUTH')])->orderBy('first_name', 'ASC')->orderBy('last_name', 'ASC')->get();
            } else {
                $item = $model::whereNotIn('username',[env('BASIC_AUTH'), 'ocradmin'])->orderBy('first_name', 'ASC')->orderBy('last_name', 'ASC')->paginate($this->data->rows);
            }
        }

        if ($item) {
            $response['status'] = 'success';
            $response['data'] = $item;
        } else {
            $response['status'] = 'error';
            $response['messages'] = 'Not found';
        }

        return response()->json($response, 200, [], JSON_NUMERIC_CHECK);
    }

    //------------------------------------------------------
    function update()
    {
        /*if (!Permission::check('update-'.$this->settings->prefix)) {
            $error_message = "You don't have permission update";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/
        $model = $this->data->model;
        $inputs = request()->all();
        if(isset($inputs['created_by'])){
            unset($inputs['created_by']);
        }
        if(isset($inputs['created_at'])){
            unset($inputs['created_at']);
        }
        $response = $model::store();
        return response()->json($response);
    }

    //------------------------------------------------------
    function toggleStatus()
    {

        /*if (!Permission::check('update-'.$this->settings->prefix)) {
            $error_message = "You don't have permission update";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;
        if (isset($this->data->input->pk)) {

            $input['id'] = $this->data->input->pk;
            $input['active'] = $this->data->input->state;

            $response = $model::store($input);
            return response()->json($response);

        } else if (is_array($this->data->input->pk)) {
            if (empty($this->data->input->pk)) {
                $response['status'] = 'error';
                $response['messages'] = 'No row selected';
                return response()->json($response);

            }
            foreach ($this->data->input->pk as $id) {
                $input['id'] = $id;
                $input['active'] = $this->data->input->state;

                $model::store($input);
            }

            return response()->json(['status' => 'success']);
        }
    }

    //------------------------------------------------------
    function delete()
    {
        /*if (!Permission::check('delete-'.$this->settings->prefix)) {
            $error_message = "You don't have permission delete";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        try {
            $model = $this->data->model;
            Seen::where('user_id', $this->data->input->pk)->delete();
            $model::where('username', '!=', env('BASIC_AUTH'))->where('username', '!=', 'ocradmin')->where('id', $this->data->input->pk)->delete($this->data->input->pk);
            return response()->json(['status' => 'success']);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Error while deleting a record']);
        }

    }

    //------------------------------------------------------
    function search()
    {
        $model = $this->data->model;
        $list = $model::orderBy("created_at", "ASC");

        $term = trim($this->data->input->q);
        if (!$term || $term == '') {
            return $list->where('id', '!=', Auth::id())->paginate($this->data->rows);
        }

        $list = $list->where(function($query) use ($term) {
            $query->where('email', 'LIKE', '%' . $term . '%')
                  ->orWhere('first_name', 'LIKE', '%' . $term . '%')
                  ->orWhere('last_name', 'LIKE', '%' . $term . '%')
                  ->orWhere('last_name', 'LIKE', '%' . $term . '%')
                  ->orWhere('username', 'LIKE', '%' . $term . '%')
                  ->orWhere('mobile', 'LIKE', '%' . $term . '%')
                  ->orWhere('id', '=', $term)
            ;
        });

        $result = $list->whereNotIn('username', [env('BASIC_AUTH'), 'ocradmin'])->where('id', '!=', Auth::id())->paginate($this->data->rows);
        return $result;
    }

    //------------------------------------------------------
    public function getUserPermission($id)
    {

        $roleController = new RoleController();

        $user = User::find($id);
        $role = $user->role;

        $roleController->getRolePermission($role);

    }

    public function updateSeen($tId)
    {
        try {

            $user = Auth::user();
            if ($user->username != 'ocradmin') {
                $user->transactions()->syncWithoutDetaching([$tId]);
            }

            $response['status'] = 'success';
        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);

    }

    public function twoFA()
    {
        $user = User::find(Auth::id());
        $google2fa = new Google2FA();

        if (isset($user->google_secret)) {
            $qrCodeUrl = $google2fa->getQRCodeUrl(
                'OCR Labs',
                $user->email,
                $user->google_secret
            );
            $response['status'] = 'success';
            $response['qr_code'] = $qrCodeUrl;
            return response()->json($response);
        } else {
            $qr_code = $google2fa->generateSecretKey();
            $user->google_secret = $qr_code;
            $user->save();
            $qrCodeUrl = $google2fa->getQRCodeUrl(
                'OCR Labs',
                $user->email,
                $qr_code
            );
            $response['status'] = 'success';
            $response['qr_code'] = $qrCodeUrl;
            $response['qr_code_key'] = $qr_code;
            $response['email'] = $user->email;
            return response()->json($response);
        }
    }

    public function smsVerify()
    {
        $permissionArray = (new LoginController)->getUserPermission();
        $code = request()->get('code');
        $user = User::find(Auth::id());
        if ($code == $user->sms_token) {
            $response['verify'] = true;
        } else {
            $response['verify'] = false;
        }


        if (request()->get('register') == 'register') {
            \Log::info('register');
            $user->mobile = request()->get('phoneNumber');
            $user->dial_code = request()->get('dial_code');
            if($response['verify']){
                $user->mobile_verified_at = now();
            }
            $user->save();
        }
        $response['user'] = $user;
        $response['user']['permissions'] = $permissionArray['main'];
        $response['user']['other_permissions'] = $permissionArray['other'];
        return response()->json($response);
    }

    public function twoFaVerify()
    {
        $code = request()->get('code');

        $user = User::find(Auth::id());
        $google2fa = new Google2FA();
        $window = 0;
        if (isset($user->google_secret)) {
            $valid = $google2fa->verifyKey($user->google_secret, $code, $window);
            $response['verify'] = $valid;
            return response()->json($response);
        } else {
            $response['verify'] = false;
            return response()->json($response);
        }
    }

    public function setPassword(Request $request)
    {
        $user = Auth::user();
        $permissionArray = (new RoleController)->getRolePermission($user->role, false);

        $password = $request['password'];
        $user->password = Hash::make($password);
        $user->password_status = 1;
        $user->password_created_at = new DateTime();
        $user->save();
        $response['success'] = true;
        $response['user'] = Auth::user();
        $response['user']['permissions'] = $permissionArray;
        return response()->json($response);
    }

    public function getIpAddress(Request $request)
    {
        $response['ip'] = \Request::getClientIp(true);
        \Log::info($response['ip']);
        $response['success'] = true;
        return response()->json($response);
    }

    public function getFields(){
        $fields = config('database.user_meta_fields');

        $new = [];
        foreach ($fields as $field){
            $temp = [
                'label' => $field['name'],
                'key' => str_slug($field['name']),
            ];

            $new[] = $temp;
        }

        return response()->json($new);
    }

    public function downloadBatchSample()
    {
        $fileName = 'BatchUser.xlsx';
        return Storage::disk('batch')->download($fileName);
    }

    public function processBatchUsers() {
        $users = request()->all();

        $res = [
            'status' => 'success'
        ];

        foreach ($users as $user){
            $user['username'] = !empty($user['email']) ? explode('@',strtolower($user['email']))[0] : '';
            $user['fullName'] = $user['first_name'].' '.$user['last_name'];
            $user['password'] = Helper::generateToken();
            $user['role'] = $user['role'] == 'Administrator' ? 'admin': str_slug($user['role']);
            $model = $this->data->model;
            $response = $model::store($user);
            if ($response['status'] == "success") {
                if(env('APP_ENV') !== 'local'){
                    $this->createNewUser($user);
                }
            }else{
                $res['failed'][] = [
                    'email' => $user['email'],
                    'message' => json_encode($response['messages']),
                ];
            }
        }

        return response()->json($res);
    }
}
