<?php
namespace App\Http\Controllers\Portal\Core;

use App\Helpers\Helper;
use App\Http\Models\Portal\Core\Module;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use stdClass;

class ModuleController extends Controller
{
    public $data;
    public $settings;

    public function __construct()
    {
        $this->data = new stdClass();
        $this->settings = Module::getSettings();
        $this->data->input = (object)request()->all();
        $this->data->input = Helper::xss_patch_input($this->data->input);

        $this->data->model = $this->settings->model;
        $this->data->rows = $this->settings->rows;
        $this->data->settings = $this->settings;
    }

    //------------------------------------------------------
    function create() {

        /*if (!Permission::check('create-'.$this->settings->prefix)) {
            $error_message = "You don't have permission create";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;

        $response = $model::store();
        return response()->json($response);

    }

    //------------------------------------------------------
    function read($id=null) {

        /*if (!Permission::check('read-'.$this->settings->prefix)) {
            $error_message = "You don't have permission read";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;
        if($id){
            $item = $model::where('id', $id)->first();
        }else{
            $item = $model::paginate($this->data->rows);
        }

        if ($item) {
            $response['status'] = 'success';
            $response['data'] = $item;
        } else {
            $response['status'] = 'error';
            $response['messages'] = 'Not found';
        }

        return response()->json($response);
    }

    //------------------------------------------------------
    function update()
    {
        /*if (!Permission::check('update-'.$this->settings->prefix)) {
            $error_message = "You don't have permission update";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/
        $model = $this->data->model;
        $response = $model::store();
        return response()->json($response);
    }

    //------------------------------------------------------
    function toggleStatus()
    {

        /*if (!Permission::check('update-'.$this->settings->prefix)) {
            $error_message = "You don't have permission update";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;
        if (isset($this->data->input->pk)) {

            $input['id'] = $this->data->input->pk;
            $input['active'] =$this->data->input->state;

            $response = $model::store($input);
            return response()->json($response);

        } else if (is_array($this->data->input->pk)) {
            if (empty($this->data->input->pk)) {
                $response['status'] = 'error';
                $response['messages'] = 'No row selected';
                return response()->json($response);

            }
            foreach ($this->data->input->pk as $id) {
                $input['id'] = $id;
                $input['active'] =$this->data->input->state;

                $model::store($input);
            }

            return response()->json(['status'=>'success']);
        }
    }

    //------------------------------------------------------
    function delete()
    {
        /*if (!Permission::check('delete-'.$this->settings->prefix)) {
            $error_message = "You don't have permission delete";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        try{
            $model = $this->data->model;
            $model::where('id',$this->data->input->pk)->delete($this->data->input->pk);
            return response()->json(['status'=>'success']);

        }catch (\Exception $e){
            return response()->json(['status'=>'error','message' => 'Error while deleting a record']);
        }

    }

    //------------------------------------------------------
    function search()
    {
        $model = $this->data->model;
        $list = $model::orderBy("created_at", "ASC");
        $term = trim($this->data->input->q);
        $list->where('title', 'LIKE', '%' . $term . '%');
        $list->orWhere('slug', 'LIKE', '%' . $term . '%');
        $list->orWhere('id', '=', $term);
        $result = $list->paginate($this->data->rows);
        return $result;
    }

    //------------------------------------------------------
}
