<?php

namespace App\Http\Controllers\Portal;

use App\Helpers\Helper;
use App\Http\Controllers\App\TokenController;
use App\Http\Controllers\SMS\SMSController;
use App\Http\Controllers\SMS\TransmitsmsAPI;
use App\Http\Models\Liveness\Liveness;
use App\Http\Models\Liveness\Token;
use App\Http\Models\Portal\Core\Permission;
use App\Http\Models\Transactions\Transactions;
use App\Http\Models\BatchCheck\BatchSms;
use App\Http\Models\BatchCheck\BatchEmail;
use Aws\Exception\AwsException;
use Aws\Ses\SesClient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class InvitationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | SMS Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles all the SMS related stuff
    |
     */
    /**
     * SMSController constructor.
     */
    public function __construct()
    {
        //bulk email validation
        Validator::extend("emails", function ($attribute, $value, $parameters) {
            $rules = [
                'email' => 'email',
            ];
            foreach ($value as $email) {
                $data = [
                    'email' => $email,
                ];
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    return false;
                }
            }
            return true;
        });

        //bulk moblie validations
        Validator::extend("mobiles", function ($attribute, $value, $parameters) {
            $rules = [
                'mobile' => 'numeric',
            ];
            foreach ($value as $mobile) {
                $data = [
                    'mobile' => $mobile,
                ];
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    return false;
                }
            }
            return true;
        });
    }

    /**
     * This handles logic for sending SMS from the web page
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function action(Request $request)
    {
        $inputs = $request->all();
        $inputs = Helper::xss_patch_input($inputs);

        //check Permission
        $workflow = $inputs['workflow'];
        if(!Permission::hasPermission($workflow)){
            $response['status'] = 'error';
            $response['message'] = 'Permission Denied';
            return response()->json($response);
        }

        $applicants = $inputs['applicants'];
        Log::debug($applicants);

        //-===============Format Mobile Field=================
        $applicants = array_map(function ($app) {
            $temp         = $app;

            if( $temp['medium'] == 'sms' ){
                $country_code = $temp['country_code'];
                $country_code = str_replace('+', '', $country_code);

                $mobile         = str_replace(' ', '', $temp['mobile']);
                $mobile         = $country_code . $mobile;
                $mobile         = str_replace(' ', '', $mobile);
                $temp['mobile'] = $mobile;
            }

            //--------------------
            $transactionId  = Helper::generateTransactionId();
            $temp['name'] = empty($app['full_name']) ? $transactionId : $app['full_name'];
            $temp['transactionId']   = $transactionId;
            //------------------
            return $temp;
        }, $applicants);

        //-===============Format Mobile Field=================
        if (empty($applicants)) {
            return response()->json([
                'status'  => 'error',
                'message' => 'Please provide the at least one applicant',
            ]);
        }
        //-===============Validate Emails And Mobiles=================

        $mobiles = [];
        $emails  = [];
        foreach ($applicants as $applicant) {
            if ($applicant['medium'] == 'sms') {
                $mobiles[] = $applicant['mobile'];

            } elseif ($applicant['medium'] == 'email') {
                $emails[] = $applicant['email'];
            }
        }

        //-------------------------------------
        $new['emails']  = $emails;
        $new['mobiles'] = $mobiles;

        //-------------------------------------
        $messages = [
            'mobiles' => 'One or mobile numbers are not valid.',
            'emails'  => 'One or emails are not valid.',
        ];
        //-------------------------------------
        $validator = Validator::make($new, [
            'emails'  => 'emails',
            'mobiles' => 'mobiles',
        ], $messages);
        //-------------------------------------

        if ($validator->fails()) {

            $validateErrors = $validator->getMessageBag()->toArray();
            $err            = '';
            if (isset($validateErrors['emails']) && !empty($validateErrors['emails'])) {
                $err = $validateErrors['emails'][0] . '.';
            }

            if (isset($validateErrors['mobiles']) && !empty($validateErrors['mobiles'])) {
                $err = $err . ' ' . $validateErrors['mobiles'][0] . '.';
            }

            return response()->json([
                'status'  => 'failed',
                'message' => $err,
            ]);
        }

        //-===============Validate Emails And Mobiles=================

        //================loop through each and send Invite====================

        $n = 1;
        $res = [];

        $sendToFirst = $applicants[0]['flow_type'] == 'guided';

        if(((int)$inputs['number']) > 0){
            $joint = round(microtime(true) * 1000) ;
        }else{
            $joint = null;
        }

        foreach ($applicants as $applicant) {


            $token = Helper::generateToken();
            $applicant['token'] = $token;

            $transaction                        = new Transactions();
            $transaction->transactionId         = $applicant['transactionId'];
            $transaction->token                 = $token;
            $transaction->category              = $inputs['module'];
            $transaction->module                = $inputs['workflow'];
            $transaction->reference             = $inputs['reference'];
            $transaction->message_template      = $inputs['message_template'];

            $transaction->instruction_type      = strtoupper($applicant['flow_type']);
            $transaction->joint_session         = $joint;
            $transaction->username              = (Auth::check()) ? Auth::user()->username : 'NA';
            $transaction->user_status           = 'waiting';

            $transaction->original_token        = 'abc123-abc123-abc123-abc123';
            $transaction->save();

            $applicant['id'] = $transaction->id;

            $url_inputs = [
                'return_url' => $request->return_url ?? route('mobileStatus', [ 'status' => 'completed']),
                'cancel_url' => $request->cancel_url ?? route('mobileStatus', [ 'status' => 'cancelled']),
            ];

            $expiry = config('portal.sms_expiry');

            if($expiry < 1){
                $expiry = ceil(60 * $expiry);
                $url_inputs['expiry'] = now()->addMinutes($expiry);
            }else{
                $url_inputs['expiry'] = now()->addHours($expiry);
            }

            $transaction->urls()->create($url_inputs);

            $transaction->contactDetails()->create([
                'name' => $applicant['name'] ?? null,
                'phone' => $applicant['mobile'] ?? null,
                'email' => $applicant['email'] ?? null,
            ]);

            if ($applicant['flow_type'] == 'batch_check') {
                if ($applicant['medium'] == 'sms') {
                    BatchSms::where('id', $applicant['batch_id'])->update(['tId' => $transaction->id]);
                }
            }

            if ($sendToFirst && $n > 1) {
                continue;
            }else{
                $res1 = $this->invite($applicant, $inputs['reference']);
                if ($res1['status'] == 'failed') {
                    return response($res1);
                    break;
                }
                $res[] = $res1;
            }

            ++$n;
        }
        //================loop through each and send Invite====================

        $response = [
            'status' => 'success',
        ];

        if($sendToFirst){
            $response['id'] = $res[0]['id'];
        }

        if (env('APP_ENV') == 'local') {
            $response['debug'] = $res;
        }
        return response()->json($response);

    }

    public function invite($applicant, $reference=null){

        try {

            $url = secure_url('/') . "/" . $applicant['token'];
            $name = (isset($applicant['name']) && $applicant['name'] != $applicant['transactionId']) ? $applicant['name'] : null;
            $msg = (new SMSController)->smsText($url, $name, $reference);

            if ($applicant['medium'] === 'sms') {
                $res = $this->sendSMS($msg, $applicant['mobile']);
            } else {
                $res = $this->sendEmail($url, $applicant['email'], $name, $reference);
            }

            if ($res == "sent") {

                $res = [
                    'status' => 'success',
                    'id' => $applicant['id'],
                ];
                if (env('APP_ENV') == 'local') {
                    $res['url'] = $url;
                    $res['token'] = session('__token');
                }

                return $res;

            } else {

                return [
                    'status'  => 'failed',
                    'message' => 'SMS sending failed',
                ];
            }


        } catch (\Exception $e) {
            return [
                'status' => 'failed',
                'error'  => $e->getMessage(),
            ];
        }

    }


    /**
     * Internal method used by above two methods
     * actual logic where it hits SMS gateway
     * @param $msg
     * @param $number
     * @return bool|string
     */
    public function sendSMS($msg, $number)
    {
        $result = (new SMSController)->send($msg, $number);
        if ($result) {
            return "sent";
        }

        return false;
    }

    public function sendEmail($url, $email, $name = null, $reference = null)
    {

        $result = (new SMSController)->sendEmail($url, $email, $name, $reference);
        if ($result) {
            return "sent";
        }

        return false;
    }


    /**
     * API handler for checking the SMS
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function check($id)
    {
        $r = SMSToken::where('id', $id)->first();

        if ($r) {
            return response()->json(['status' => 'success', 'data' => $r]);
        }

        return response()->json(['status' => 'failed']);
    }

    /**
     * This handle the api for
     * changing the final status to finished.
     * when everything is done.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function finish(Request $request)
    {

        $transactionId = Helper::xss_patch_input($request->transactionId);
        $transactions  = Transactions::where('transactionId', $transactionId)->whereNotIn('status', ['Terminated', 'Expired'])->first();

        SMSToken::where('id', $transactions->smsId)->update([
            'status' => 'completed',
        ]);

        return response()->json("done");
    }

    public function statusUpdate(Request $request)
    {
        try {

            $tId    = Helper::xss_patch_input($request->input('transactionId'));
            $status = Helper::xss_patch_input($request->input('status'));

            $transaction = Transactions::where('transactionId', $tId)->first();
            $sms         = SMSToken::find($transaction->smsId);

            $sms->status = $status;
            $sms->save();

            $response['status'] = true;

        } catch (\Exception $e) {
            $response['status']  = false;
            $response['message'] = 'Error in updating status';
        }

        return response()->json($response);
    }

    //--------------
    public function sendAdditional($id){
        $transaction = Transactions::find($id);
        $jointID = $transaction->joint_session;

        $additional_trans = Transactions::where('joint_session', $jointID)->where('id', '!=', $id)->with(['contactDetails'])->get()->toArray();

        $response['status'] = 'success';
        $response['showPopup'] = count($additional_trans) > 0;

        if(count($additional_trans) > 0){
            $data['reference'] = $additional_trans[0]['reference'] ?? 'NA';

            foreach ($additional_trans as $tran){
                $contact = $tran['contact_details'];
                $temp = [];

                $temp['full_name'] = $contact['name'];
                $temp['medium']    = ($contact['email']) ? 'email' : 'sms';
                $temp['token']     = $tran['token'];
                $temp['id']        = null;

                if($contact['email']){
                    $temp['email'] = $contact['email'];
                }else{
                    $temp['mobile'] = $contact['phone'];
                }

                $res = $this->invite($temp);

                if (env('APP_ENV') == 'local') {
                    $temp['url'] = $res['url'];
                }

                unset($temp['token']);
                unset($temp['id']);

                $data['applicants'][] = $temp;
            }

            $response['data'] = $data;

        }

        return response()->json($response);
    }

}
