<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Portal\Core\RoleController;
use App\Http\Models\Portal\Core\Module;
use App\Http\Models\Portal\Core\Permission;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public $redirectTo = '/portal';

    public $maxAttempts = 3;
    public $decayMinutes = 5;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(Request $request)
    {
        return view('portal.app');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }


    public function getUserPermission($id = null)
    {
        $roleController = new RoleController();

        if ($id) {
            $user = User::find($id);
        } else {
            $user = Auth::user();
        }

        $role = $user->role;
        $roleController->getRolePermission($role);

    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);
    }

    protected function authenticated(Request $request, $user)
    {
        if (!$request->ajax()) {
            return response()->redirectTo('/portal');
        }

        //------
        DB::table('oauth_access_tokens')
            ->where('user_id', $user->id)
            ->whereDate('created_at', Carbon::yesterday())
            ->delete();
        //------
        //Uncomment this when external api is needed
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        session(['token' => json_encode([
            'access_token' => $tokenResult->accessToken,
            'expiry' => $tokenResult->token->expires_at
        ])]);


        /*$token = 'not-exists';
        session(['token' => json_encode([
            'access_token' => $token,
            'expiry' => now()->addDay(1)
        ])]);*/
        //------

        if ($user->active) {

            $user = Auth::user();
            $user->last_signin_at = now();
            $user->save();
            //-------

            $response['status'] = 'success';
            $user1 = Auth::user()->toArray();
            $user1['permissions'] = (new RoleController)->getRolePermission($user1['role'], false);
            $response['data'] = $user1;
            $response['session'] = $tokenResult->accessToken;
        } else {
            Auth::logout();
            $response['status'] = 'error';
            $response['message'] = 'User is inactive, hence not allowed to login.';
        }

        return $response;
    }

    protected function sendFailedLoginResponse(Request $request, $trans = 'auth.failed')
    {
        $remainAttempts = 3 - $this->limiter()->attempts($this->throttleKey($request));
        // \Log::info($failedAttempts);
        if ($remainAttempts > 1)
            $errors = ['errors' => ['email' => [trans($trans) . ' ' . $remainAttempts . ' tries remaining.']]];
        else if ($remainAttempts == 1)
            $errors = ['errors' => ['email' => [trans($trans) . ' ' . $remainAttempts . ' try remaining']]];
        else
            $errors = ['errors' => ['email' => [trans($trans) . ' Locked for 5min.']]];
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $user_admin = User::where('email', $request['email'])->first()['role']
            || User::where('username', $request['email'])->first()['role'];

        if (empty($user_admin)) {
            $response['status'] = 'error';
            $response['message'] = "User information is incorrect";
            return response()->json($response);
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]) || (
                $user_admin == 'admin' && Auth::attempt(['username' => $request->email, 'password' => $request->password])
            )) {

            return $this->authenticated($request, Auth::user());
            // return \Redirect::to('/portal');
        } else {
            $this->incrementLoginAttempts($request);
            return $this->sendFailedLoginResponse($request);
        }
    }

    public function myLogout()
    {
        Auth::logout();
        $response['status'] = 'success';
        return response()->json($response);
    }

    public function loginWithToken($token)
    {
        // $token = $request->input('token');
        // $token = Input::get('token');
        $user = User::where('account_token', $token)->first();
        $user->active = 1;
        $user->save();

        Auth::guard('web')->login($user);
        return redirect('/portal');
    }
}
