<?php

namespace App\Http\Requests\SMS;

use Illuminate\Foundation\Http\FormRequest;

class SMS extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_code'   => 'required|regex:/^([0-9+]+)$/u|max:3',
            'phone_number' => 'required|regex:/^([0-9 ]+)$/u|max:25',
            'transactionId' => 'required|regex:/^([a-zA-Z0-9]+)$/u|min:10|max:250',
            'return_url'    => 'nullable|url|max:250',
            'cancel_url'    => 'nullable|url|max:250',
            'name'          => 'nullable|regex:/^([a-zA-Z ]+)$/u|max:25',
            'subcode'       => 'nullable',
        ];
    }
}
