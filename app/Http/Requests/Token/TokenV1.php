<?php

namespace App\Http\Requests\Token;

use Illuminate\Foundation\Http\FormRequest;

class TokenV1 extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token'         => 'required|regex:/^([a-zA-Z0-9-_]+)$/u|min:10|max:250',
            'return_url'    => 'required|url|min:10|max:250',
            'cancel_url'    => 'required|url|min:10|max:250',
            'mobile_number' => 'required|regex:/^([0-9 ]+)$/u|max:20',
            'subcode'       => 'nullable',
            'ruleid'        => 'nullable',
        ];
    }
}
