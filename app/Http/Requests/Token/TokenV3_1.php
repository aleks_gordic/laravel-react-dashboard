<?php

namespace App\Http\Requests\Token;

use Illuminate\Foundation\Http\FormRequest;

class TokenV3_1 extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transactionId' => 'required|regex:/^([a-zA-Z0-9]+)$/u|min:10|max:250',
            'return_url'    => 'nullable|url|max:250',
            'cancel_url'    => 'nullable|url|max:250',
            'name'          => 'nullable|regex:/^([a-zA-Z ]+)$/u|max:25',
            'subcode'       => 'nullable',
        ];
    }
}
