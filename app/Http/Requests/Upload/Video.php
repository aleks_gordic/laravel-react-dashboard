<?php

namespace App\Http\Requests\Upload;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Video extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'id'   => !$request->blob ? 'required|videoId|regex:/^([a-z0-9-.]+)/u|max:100' : 'string',
            'blob' => 'mimes:mp4,webm,mov,ogg,avi',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.video_id' => 'The video id not found.',
        ];
    }
}
