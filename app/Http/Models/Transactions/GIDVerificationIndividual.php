<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class GIDVerificationIndividual extends Model
{
    protected $table  = 'transaction_gid_verification_individual';
    protected $hidden = ['id', 'vId', 'created_at', 'updated_at'];
    protected $casts  = [
        'fieldResult' => 'array',
    ];
    protected $fillable = [
        'vId',
        'dateVerified',
        'method',
        'name',
        'state',
        'fieldResult',
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function gIDVerificationResult()
    {
        return $this->belongsTo('App\Http\Models\Transactions\GIDVerificationResult');
    }
}
