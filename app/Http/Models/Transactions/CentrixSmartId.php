<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class CentrixSmartId extends Model
{
    protected $table    = 'transaction_centrix_smart_id';
    protected $hidden   = ['id', 'tId', 'created_at', 'updated_at'];
    protected $fillable = [
        'IsVerified',
        'IsNameVerified',
        'IsDateOfBirthVerified',
        'IsAddressVerified'
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }

    /**
     * Transactions Centrix smart ID
     * @return Illuminate\Database\Eloquent\Model
     */
    public function data()
    {
        return $this->hasMany('App\Http\Models\Transactions\CentrixSmartIdData', 'sId');
    }

    public function deleteAll()
    {
        $this->data()->delete();
        $this->delete();
    }
}
