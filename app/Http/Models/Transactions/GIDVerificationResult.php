<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class GIDVerificationResult extends Model
{
    protected $table    = 'transaction_gid_verification_result';
    protected $hidden   = ['tId', 'created_at', 'updated_at'];
    protected $fillable = [
        'overallVerificationStatus',
        'ruleId',
        'verificationId',
    ];

    /**
     * @return Illuminate\Database\Eloquent\Model
     */
    public function individual()
    {
        return $this->hasMany('App\Http\Models\Transactions\GIDVerificationIndividual', 'vId');
    }

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
