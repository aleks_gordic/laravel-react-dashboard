<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class Seen extends Model
{
    protected $table    = 'transaction_seens';
    protected $fillable = ['tId', 'user_id'];
    protected $hidden   = ['created_at', 'updated_at'];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsToMany('App\Http\Models\Transactions\Transactions');
    }
}
