<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class GIDSourceList extends Model
{
    protected $table  = 'transaction_gid_source_list';
    protected $hidden = ['id', 'tId', 'created_at', 'updated_at'];
    protected $casts  = [
        'available'     => 'boolean',
        'notRequired'   => 'boolean',
        'oneSourceLeft' => 'boolean',
        'passed'        => 'boolean',
        'order'         => 'string',
        'version'       => 'string',
    ];
    protected $fillable = [
        'available',
        'name',
        'notRequired',
        'oneSourceLeft',
        'order',
        'passed',
        'state',
        'version',
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
