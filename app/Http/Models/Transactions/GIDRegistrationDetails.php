<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class GIDRegistrationDetails extends Model
{
    protected $table    = 'transaction_gid_registration_details';
    protected $hidden   = ['id', 'tId', 'created_at', 'updated_at'];
    protected $fillable = [
        'city',
        'country',
        'streetType',
        'suburb',
        'dateCreated',
        'givenName',
        'middleNames',
        'surname',
        'dob',
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
