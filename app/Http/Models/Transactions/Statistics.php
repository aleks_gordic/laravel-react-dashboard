<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    protected $table = 'transaction_statistics';
    protected $fillable = [
        'tId',
        'review_terms',
        'capture_id',
        'review_data',
        'liveness'
    ];

    public function Transactions()
    {
    	return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
