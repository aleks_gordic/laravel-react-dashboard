<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class CentrixResidential extends Model
{
    protected $table    = 'transaction_centrix_residential';
    protected $hidden   = ['id', 'tId', 'created_at', 'updated_at'];
    protected $fillable = [
        'IsSuccess',
        'Message',
        'ResidentialVerified',
        'ThirdPartyMatchType',
        'ThirdPartyMatchTypeDescription',
        'Surname',
        'FirstName',
        'MiddleName',
        'DateOfBirth',
        'ThirdPartyNameMatch',
        'ThirdPartyNameMatchDescription',
        'SurnameMatch',
        'FirstNameMatch',
        'FirstNameInitialMatch',
        'MiddleNameMatch',
        'MiddleNameInitialMatch',
        'DateOfBirthMatch',
        'ThirdPartyAddressMatch',
        'ThirdPartyAddressMatchDescription',
        'AddressLine1',
        'Suburb',
        'State',
        'Postcode',
        'DeliveryPointID',
        'UnitNoMatch',
        'StreetNoMatch',
        'StreetNameMatch',
        'StreetTypeMatch',
        'SuburbMatch',
        'StateMatch',
        'PostcodeMatch',
        'HomePhoneNo',
        'MobilePhoneNo',
        'PhoneNumberMatch',
        'MobileNumberMatch',
        'EmailAddress',
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
