<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class Extracted extends Model
{
    protected $table    = 'transaction_extracted';
    protected $fillable = [
        'cardType',
        'licenceType',
        'firstName',
        'lastName',
        'middleName',
        'dateOfBirth',
        'expiryDate',
        'countryOfIssue',
        'address',
        'street',
        'city',
        'state',
        'zipCode',
        'licenceNumber',
        'licenceVersion',
        'cardNumber',
        'imageQuality',
    ];
    protected $hidden = ['id', 'tId', 'created_at', 'updated_at'];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
