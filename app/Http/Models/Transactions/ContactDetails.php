<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class ContactDetails extends Model
{
    protected $table    = 'transaction_contact_details';
    protected $hidden   = ['id', 'tId', 'created_at', 'updated_at'];
    protected $fillable = [
        'name',
        'phone',
        'email',
        'residential_address',
        'previous_address',
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
