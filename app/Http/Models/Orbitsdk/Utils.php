<?php

namespace App\Http\Models\Orbitsdk;

use Illuminate\Support\Facades\Storage;
class Utils
{
    /**
     * Get URL from filename
     *
     * @param string $filename
     * @param string $type
     * @return boolean
     */

    protected static function existOnLocal($type, $filename){

        $path = config('orbitsdk.api.images_path');
        if ($type !== 'VID') {
            $path .= $type !== 'PASSPORT' ? 'ORBIT_ID_ANZ' : 'ORBIT_MRZ';
            $path .= '/' . $type . '/' . config('orbitsdk.api.token') . '/';
        } else {
            $path .= 'ORBIT_FR/FR/' . config('orbitsdk.api.token') . '/';
        }

        $filePath = $path.'/'.$filename;

        $exists = Storage::disk('tr')->exists($filePath);
        return $exists;
    }

    /**
     * Get URL from filename
     *
     * @param string $filename
     * @return string
     */
    public static function getFileUrl($type, $filename, $path = false)
    {
        return self::getBaseUrl($type, $path).$filename;
    }

    /**
     * Get file name from URL
     *
     * @param string $type
     * @param string $url
     * @return string
     */
    public static function getFileName($type, $url)
    {
        if (empty($url)) {
            return "";
        }

        return str_replace(self::getBaseUrl($type, true), '', $url);
    }

    /**
     * Get file base URL
     *
     * @param string $type
     * @param bool $path
     * @return string
     */
    public static function getBaseUrl($type, $path = false)
    {
        $host = config('orbitsdk.api.url');

        $url = $host . config('orbitsdk.api.images_path');
        if ($path) {
            $url = '../..' . config('orbitsdk.api.images_path');
        }

        if ($type !== 'VID') {

            if($type === 'CHN_IDCARD' || $type === 'HKG_IDCARD' || $type === 'MYS_DLCARD' || $type === 'SGP_IDCARD'){
                $sdktype = 'ORBIT_ID_ASIA';
            }elseif ($type !== 'PASSPORT'){
                $sdktype = 'ORBIT_ID_ANZ';
            }else{
                $sdktype = 'ORBIT_MRZ';
            }

            $url .= $sdktype. '/' . $type . '/' . config('orbitsdk.api.token') . '/';
        } else {
            return $url .= 'ORBIT_FR/FR/' . config('orbitsdk.api.token') . '/';
        }

        return $url;
    }

    /**
     * Get file name
     *
     * @param string $url
     * @return string
     */
    public static function getExtention($url)
    {
        $explode = explode('/', $url);
        if (!count($explode) || !preg_match('#.#i', $explode[count($explode) - 1])) {
            return "";
        }

        $extention = explode('.', $explode[count($explode) - 1]);

        return '.' . $extention[1];
    }

    /**
     * Get full file URL
     *
     * @param string $url
     * @return string
     */
    public static function getFullURL($token, $path, $filename)
    {
        return config('app.api_url') . '/files/' . $path . '/' . $token . strtolower(self::getExtention($filename));
    }
}
