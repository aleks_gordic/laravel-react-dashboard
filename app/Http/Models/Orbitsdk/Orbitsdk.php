<?php

namespace App\Http\Models\Orbitsdk;

use App\Http\Models\ApiSessions\ApiSessions;
use GuzzleHttp\Client;

class Orbitsdk
{
    /**
     * Create a http client
     * @var \GuzzleHttp\Client
     */
    protected $http;

    /**
     * OrbitSDK token
     * @var string
     */
    protected $token;

    public function __construct()
    {
        $this->token = config('orbitsdk.api.token');
        $this->http  = new Client([
            'http_errors' => false,
            'base_uri'    => config('orbitsdk.api.url') . '/' . config('orbitsdk.api.base_path') . '/',
        ]);
    }

    /**
     * Make a request to create new session
     *
     * @param \Illuminate\Http\Request $request
     * @param string $type
     * @return array
     */
    private function session($request, $type = 'image')
    {
        if($request->idType === 'ID_CARD'){
            $sdktype = 'ORBIT_MRZ';
            $subtype = 'PASSPORT';
        }else{

            if($request->idType === 'CHN_IDCARD' || $request->idType === 'HKG_IDCARD' || $request->idType === 'MYS_DLCARD' || $request->idType === 'SGP_IDCARD'){
                $sdktype = 'ORBIT_ID_ASIA';
            }elseif ($request->idType !== 'PASSPORT'){
                $sdktype = 'ORBIT_ID_ANZ';
            }else{
                $sdktype = 'ORBIT_MRZ';
            }

            $subtype = $request->idType;
        }

        if ($type == 'video') {
            $sdktype = 'ORBIT_FR';
            $subtype = 'VID';
        } elseif ($type == 'selfie') {
            $sdktype = 'ORBIT_FR';
            $subtype = 'IMG';
        }

        $session = $this->http->request('POST', 'request_new_session', [
            'form_params' => [
                'token'   => $this->token,
                'sdktype' => $sdktype,
                'subtype' => $subtype,
            ],
        ]);

        return json_decode($session->getBody(), true);
    }

    /**
     * Upload image file
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function uploadImageFile($request)
    {
        // Create a session
        $session = $this->session($request);

        // if request falied
        if ($session['status'] !== 'success') {
            return $session;
        }

        $file = $request->frontFile;

        if($request->idType === 'ID_CARD'){
            $sdktype = 'ORBIT_MRZ';
        }else{

            if($request->idType === 'CHN_IDCARD' || $request->idType === 'HKG_IDCARD' || $request->idType === 'MYS_DLCARD' || $request->idType === 'SGP_IDCARD'){
                $sdktype = 'ORBIT_ID_ASIA';
            }elseif ($request->idType !== 'PASSPORT'){
                $sdktype = 'ORBIT_ID_ANZ';
            }else{
                $sdktype = 'ORBIT_MRZ';
            }
            //$sdktype = $request->idType !== 'PASSPORT' ? 'ORBIT_ID_ANZ' : 'ORBIT_MRZ';
        }

        $params = [
            'query'     => [
                'token'     => $this->token,
                'sessionid' => $session['session'],
                'imageside' => 'F',
                'sdktype'   => $sdktype,
            ],
            'multipart' => [
                [
                    'name'     => 'imagefile',
                    'contents' => fopen($file->getPathname(), 'r'),
                    'filename' => $file->getClientOriginalName(),
                ],
            ],
        ];

        $uploads = [];

        // if its not Passport
        if (!empty($request->frontFile) && !empty($request->backFile)) {
            $files = [$request->frontFile, $request->backFile];
            foreach ($files as $key => $file) {
                $params['multipart'][0] = [
                    'name'     => 'imagefile',
                    'contents' => fopen($file->getPathname(), 'r'),
                    'filename' => $file->getClientOriginalName(),
                ];
                if ($key) {
                    $params['query']['imageside'] = 'B';
                }

                $response = json_decode($this->http->request('POST', 'uploadImageFile', $params)->getBody(), true);
                if ($response['status'] !== 'success') {
                    return $response;
                }

                $uploads[] = str_replace('../../','',$response['frontImg']);
            }
        } else {
            // if its driver licence
            $response = json_decode($this->http->request('POST', 'uploadImageFile', $params)->getBody(), true);

            if ($response['status'] !== 'success') {
                return $response;
            }

            $uploads[] = str_replace('../../','',$response['frontImg']);
        }

        $session['file'] = $uploads;

        return $session;
    }

    /**
     * Upload image file
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function uploadSelfieImageFile($request)
    {
        // Create a session
        $session = $this->session($request, 'selfie');

        // if request falied
        if ($session['status'] !== 'success') {
            return $session;
        }

        $params = [
            'query'     => [
                'token'     => $this->token,
                'sessionid' => $session['session'],
                'imageside' => 'B',
                'sdktype' => 'ORBIT_FR',
            ],
            'multipart' => [],
        ];

        $files = [$request->face, $request->turnHead];
        foreach ($files as $key => $file) {
            $params['multipart'][0] = [
                'name'     => 'imagefile',
                'contents' => fopen($file->getPathname(), 'r'),
                'filename' => $file->getClientOriginalName(),
            ];

            if ($key) {
                $params['query']['imageside'] = 'F';
            }

            $response = json_decode($this->http->request('POST', 'uploadImageFile', $params)->getBody(), true);
            if ($response['status'] !== 'success') {
                return $response;
            }

            $session['file'][] = $response['file'];
        }

        $tr = $request->transaction->first();
        $face_url =  Utils::getFileUrl($tr->identify_type, $tr->cards->face_photo);

        // call set session image
        $filepath = str_replace(config('orbitsdk.api.url'),'../..', $face_url);
        $this->setSessionSide($session['session'], $filepath,true);
        $data = $this->getFaceScanData($session['session']);

        //If FR Fails
        if(empty($data) || $data['status'] == 'error'){
            return [
                'status' => 'error',
                'msg' => 'FR failed.'
            ];
        }

        $response = Response::faceScan($data);
        // Store face scan data
        $fields = [
            'session'    => $session['session'],
            'confidence' => $response['confidence'],
            //'liveness'   => $response['liveness'],
            'features'   => json_encode($response['features']),
        ];
        if ($tr->faceScan) {
            $tr->faceScan()->update($fields);
        }else{
            $tr->faceScan()->create($fields);
        }

        return $session;
    }

    /**
     * Upload video file
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function uploadVideoFile($request)
    {
        // Create a session
        $session = $this->session($request, 'video');

        // if request falied
        if ($session['status'] !== 'success') {
            return $session;
        }

        $file = $request->blob;

        $response = json_decode($this->http->request('POST', 'uploadVideoFile', [
            'query'     => [
                'token'     => $this->token,
                'sessionid' => $session['session'],
                'videoside' => 'B',
            ],
            'multipart' => [
                [
                    'name'     => 'videofile',
                    'contents' => fopen($file->getPathname(), 'r'),
                    'filename' => $file->getClientOriginalName(),
                ],
            ],
        ])->getBody(), true);

        if ($response['status'] !== 'success') {
            return $response;
        }

        $session['file'] = $response['file'];

        return $session;
    }

    /**
     * Upload video file
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function uploadVideoFromID($request)
    {
        // Create a session
        $session = $this->session($request, 'video');

        // if request falied
        if ($session['status'] !== 'success') {
            return $session;
        }

        $form_params = [
            'token'       => $this->token,
            'sessionid'   => $session['session'],
            'side'        => 'B',
            'sdktype'     => 'ORBIT_FR',
            'id'          => trim($request->id),
            'auth_id'     => config('liveness.api.auth.client_id'),
            'auth_secret' => config('liveness.api.auth.client_secret'),
            'api'         => 'staging-idkit'
        ];

        $response = json_decode($this->http->request('POST', 'uploadVideoFromId', [
            'form_params' => $form_params
        ])->getBody(), true);

        if ($response['status'] !== 'success') {
            return $response;
        }

        $session['file'] = $response['file'];

        return $session;
    }

    /**
     * Get card information
     *
     * @param string $sessionid
     * @param boolean $both
     * @return array
     */
    public function getCardInformation($sessionid, $both = true)
    {
        $recSide = (!$both) ? 'F' : 'FB';
        return json_decode($this->http->request('POST', 'get_card_information', [
            'form_params' => [
                'token'     => $this->token,
                'sessionid' => $sessionid,
                'recSide'   => $recSide,
            ],
        ])->getBody(), true);
    }

    /**
     * Get face scan data
     *
     * @param string $sessionid
     * @return array
     */
    public function getFaceScanData($sessionid)
    {
        $response = $this->http->request('POST', 'identify_face', [
            'form_params' => [
                'token'     => $this->token,
                'sessionid' => $sessionid,
            ],
        ])->getBody()->getContents();

        return json_decode($response, true);
    }

    /**
     * Set session image side
     *
     * @param string $sessionid
     * @param string $filepath
     * @param boolean $selfie
     * @return array
     */
    public function setSessionSide($sessionid, $filepath, $selfie = false)
    {
        $form_params = [
            'token'     => $this->token,
            'sessionid' => $sessionid,
            'side'      => 'F',
            'filepath'  => $filepath,
        ];

        if($selfie){
            $form_params['sdktype'] = 'ORBIT_FR';
        }

        $response = $this->http->request('POST', 'set_Session_SideImage', [
            'form_params' => $form_params,
        ])->getBody();

        return json_decode($response, true);
    }

    /**
     * Set session image side
     *
     * @param string $sessionid
     * @param string $filepath
     * @return array
     */
    public function deleteSession($sessionid, $sdktype)
    {
        ApiSessions::where('session_id', $sessionid)->delete();

        return json_decode($this->http->request('POST', 'deleteSession', [
            'form_params' => [
                'token'     => $this->token,
                'sessionid' => $sessionid,
                'sdktype'   => $sdktype,
            ],
        ])->getBody(), true);
    }
}
