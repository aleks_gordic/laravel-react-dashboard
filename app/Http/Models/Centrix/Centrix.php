<?php

namespace App\Http\Models\Centrix;

use App\Http\Models\Transactions\CentrixSmartIdData;
use App\Http\Models\Transactions\Transactions;
use DB;
use SoapClient;
use \Illuminate\Http\Request;

class Centrix
{
    /**
     * Make a request to get centrix details
     *
     * @return array
     */
    public static function request($request)
    {
        $location = config('centrix.location');
        $action   = config('centrix.action');
        $wsdl     = config('centrix.WSDL');

        $client = new SoapClient($wsdl, config('centrix.auth'));

        $result = $client->__doRequest(self::generateXML($request), $location, $action, 1);
        $result = preg_replace([
            "#<s:#i",
            "#</s:#i",
            "#<([a-zA-Z0-9]+) i:nil=\"(.*?)\"\/>#i",
        ], [
            "<",
            "</",
            "<$1>$2</$1>",
        ], $result);

        $xml = simplexml_load_string($result);

        $json = json_decode(json_encode($xml), true);

        if (!$json) {
            return self::defaultCentrix();
        }

        if ($json && !array_key_exists('GetCreditReportProductsResponse', $json["Body"])) {
            return self::defaultCentrix();
        }

        $json = $json["Body"]["GetCreditReportProductsResponse"]["GetCreditReportProductsResult"];

        if ($json["ResponseDetails"]["IsSuccess"] == "true") {
            return ['success' => true, 'data' => $json];
        }

        return self::defaultCentrix();
    }

    /**
     * Generate an XML request
     *
     * @param array $data
     * @return string
     */
    protected static function generateXML($data)
    {
        $request = [
            'GetCreditReportProducts' => [
                'CreditReportProductsRequest' => [
                    'Credentials'     => config('centrix.credentials'),
                    'ServiceProducts' => [
                        'ServiceProduct' => [
                            'ProductCode' => 'SmartID',
                        ],
                    ],
                    'RequestDetails'  => [
                        'EnquiryReason'       => 'IDVF',
                        'SubscriberReference' => session('__token'),
                    ],
                    'DriverLicence'   => [],
                    'ConsumerData'    => [
                        'Personal'  => [
                            'Surname'     => ucfirst(strtolower($data->lastName)),
                            'FirstName'   => ucfirst(strtolower($data->firstName)),
                            'MiddleName'  => ucfirst(strtolower($data->middleName)),
                            'DateOfBirth' => date('Y-m-d', strtotime($data->dateOfBirth)),
                        ],
                        'Addresses' => [
                            'Address' => [
                                'AddressType'  => 'C',
                                'AddressLine1' => $data->address['addressLine1'],
                                'AddressLine2' => $data->address['addressLine2'],
                                'Suburb'       => $data->address['suburb'],
                                'City'         => $data->address['city'],
                                'Country'      => $data->countryCode == 'NZ' ? 'NZL' : 'AUS',
                                'Postcode'     => $data->address['postcode'],
                            ],
                        ],
                        'Passport'  => [],
                    ],
                    'Consents'        => [
                        'Consent' => [
                            'Name'         => 'DIAPassportVerification',
                            'ConsentGiven' => '1',
                        ],
                    ],
                ],
            ],
        ];

        $extraParametersSubcode = null;
        if ($data->transaction) {
            $tr = $data->transaction->first();
            if ($tr->subcode) {
                $extraParametersSubcode = [
                    'NameValuePair' => [
                        'Name'  => 'SubCode',
                        'Value' => $tr->subcode,
                    ],
                ];
            } else {
                $extraParametersSubcode = [
                    'NameValuePair' => [
                        'Name'  => 'SubCode',
                        'Value' => config('centrix.subCode'),
                    ],
                ];
            }
        }

        // Change Centrix to Australia
        if ($data->countryCode == 'AU') {

            unset($request['GetCreditReportProducts']['CreditReportProductsRequest']['DriverLicence']);
            unset($request['GetCreditReportProducts']['CreditReportProductsRequest']['ConsumerData']['Passport']);

            $request['GetCreditReportProducts']['CreditReportProductsRequest']['RequestDetails']['EnquiryReason'] = 'IDVI';
            $request['GetCreditReportProducts']['CreditReportProductsRequest']['ServiceProducts']                 = [
                [
                    'ServiceProduct' => [
                        'ProductCode' => 'IDDocumentAU',
                    ],
                ],
                [
                    'ServiceProduct' => [
                        'ProductCode' => 'PEPWatchlistAU',
                    ],
                ],
                [
                    'ServiceProduct' => [
                        'ProductCode' => 'IDResidentialAU',
                    ],
                ],
            ];

            $request['GetCreditReportProducts']['CreditReportProductsRequest']['Consents']['Consent'] = [
                'Name'         => 'IDAU',
                'ConsentGiven' => true,
            ];

            $extraParameters = [];
            $type            = 'ACT';

            if ($data->transaction && $data->transaction->first()->extracted) {
                $extract = $data->transaction->first()->extracted()->first();
                if ($extract && preg_match('#([A-Z]+)_([A-Z]+)_([A-Z]+)#i', $extract->cardType)) {
                    $type = preg_replace('#([A-Z]+)_([A-Z]+)_([A-Z]+)#i', '$2', $extract->cardType);
                }
            }

            if ($data->cardType !== 'PASSPORT') {
                $extraParameters = [
                    [
                        'NameValuePair' => [
                            'Name'  => 'DLVNAU',
                            'Value' => $data->licenceNumber,
                        ],
                    ],
                    [
                        'NameValuePair' => [
                            'Name'  => 'DLVSAU',
                            'Value' => $type,
                        ],
                    ],
                ];
            } else {
                $extraParameters = [
                    [
                        'NameValuePair' => [
                            'Name'  => 'PASSPORTNAU',
                            'Value' => $data->passportNumber,
                        ],
                    ],
                    [
                        'NameValuePair' => [
                            'Name'  => 'PASSPORTCAU',
                            'Value' => 'AUS',
                        ],
                    ],
                ];
            }

            if ($extraParametersSubcode) {
                $extraParameters[] = $extraParametersSubcode;
            }

            $request['GetCreditReportProducts']['CreditReportProductsRequest']['ExtraParameters'] = $extraParameters;

        } else {
            if ($data->cardType !== 'PASSPORT') {
                $request['GetCreditReportProducts']['CreditReportProductsRequest']['DriverLicence'] = [
                    'DriverLicenceNumber'  => $data->licenceNumber,
                    'DriverLicenceVersion' => $data->licenceVersion,
                ];
            } else {
                $request['GetCreditReportProducts']['CreditReportProductsRequest']['ConsumerData']['Passport'] = [
                    'PassportNumber' => $data->passportNumber,
                    'Expiry'         => date('Y-m-d', strtotime($data->expiryDate)),
                ];
            }

            if ($extraParametersSubcode) {
                $extraParameters   = [];
                $extraParameters[] = $extraParametersSubcode;

                $request['GetCreditReportProducts']['CreditReportProductsRequest']['ExtraParameters'] = $extraParameters;
            }
        }

        return self::toXML($request);
    }

    /**
     * Generate XML tag
     *
     * @param array|string
     * @return string
     */
    protected static function tag($data)
    {
        $tag = '';
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if ((is_array($value) && !count($value))) {
                    continue;
                }
                if (!is_int($key)) {
                    $tag .= '<cen:' . $key . '>';
                }

                if (is_array($value)) {
                    $tag .= self::tag($value);
                } else {
                    $tag .= $value;
                }
                if (!is_int($key)) {
                    $tag .= '</cen:' . $key . '>';
                }

            }
        } else {
            $tag .= $data;
        }

        return $tag;
    }

    /**
     * Array to XML
     *
     * @param array
     * @return string
     */
    protected static function toXML($request)
    {
        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cen="http://centrix.co.nz">
                    <soapenv:Header/>
                    <soapenv:Body>' . self::tag($request) . '</soapenv:Body>
                </soapenv:Envelope>';
    }

    /**
     * Default centrix result
     *
     * @return array
     */
    public static function defaultCentrix()
    {
        $json = '{
            "ResponseDetails": {
                "EnquiryNumber": "00000000000",
                "EnquiryDate": "2018-11-18T23:15:27.56113+13:00",
                "IsSuccess": "false",
                "ServiceVersion": "29.1.0.0",
                "WsdlVersion": "17.0.0.0",
                "ServiceEnvironment": "TEST"
            },
            "DataSets": {
                "DriverLicenceVerification": {
                    "IsDriverLicenceVerified": "false",
                    "IsDriverLicenceLastNameMatched": "false",
                    "IsDriverLicenceFirstNameMatched": "false",
                    "IsDriverLicenceMiddleNameMatched": "false",
                    "IsDriverLicenceDateOfBirthMatched": "false",
                    "IsSuccess": "false",
                    "IsDriverLicenceVerifiedAndMatched": "false"
                },
                "PEPWatchlistData": {
                    "IsSuccess": "false",
                    "InternationalWatchlistIsClear": "false"
                },
                "SmartID": {
                    "IsVerified": "false",
                    "IsNameVerified": "false",
                    "IsDateOfBirthVerified": "false",
                    "IsAddressVerified": "false",
                    "DataSourceMatchResults": {
                        "DataSourceMatchResult": [{
                            "DataSourceName": "ComprehensiveAccount",
                            "DataSourceDescription": "Comprehensive Account",
                            "NameMatchStatus": "NA",
                            "DateOfBirthMatchStatus": "NA",
                            "AddressMatchStatus": "NA"
                        }, {
                            "DataSourceName": "RetailEnergyAccount",
                            "DataSourceDescription": "Retail Energy Account",
                            "NameMatchStatus": "NA",
                            "DateOfBirthMatchStatus": "NA",
                            "AddressMatchStatus": "NA"
                        }, {
                            "DataSourceName": "NZPropertyOwner",
                            "DataSourceDescription": "NZ Property Owner",
                            "NameMatchStatus": "NA",
                            "DateOfBirthMatchStatus": "NA",
                            "AddressMatchStatus": "NA"
                        }, {
                            "DataSourceName": "NZTADriverLicence",
                            "DataSourceDescription": "NZTA Driver Licence",
                            "NameMatchStatus": "NA",
                            "DateOfBirthMatchStatus": "NA",
                            "AddressMatchStatus": "NA"
                        }]
                    }
                }
            }
        }';

        return ['success' => false, 'data' => json_decode($json, true)];
    }

    public static function boolean($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public static function stringify($value)
    {
        if (is_array($value) && !empty($value)) {
            return json_encode($value);
        } elseif (is_array($value) && empty($value)) {
            return null;
        } else {
            return $value;
        }
    }

    /**
     * Reset centrix database
     *
     * @param Transactions $model
     * @return void
     */
    protected static function reset($model)
    {
        $centrixSmartId = $model->centrixSmartId()->first();

        if ($centrixSmartId) {
            $centrixSmartId->data()->delete();
        }
        $model->centrixSmartId()->delete();
        $model->centrixLicence()->delete();
        $model->centrixPassport()->delete();
        $model->centrixPep()->delete();
    }

    public static function defaultAU($data)
    {
        if (!isset($data["DataSets"]["InternationalIDAU"])) {
            return [
                'DriverLicenceVerificationAU' => [
                    'DriverLicenceVerified' => false,
                    'IsSuccess'             => false,
                ],
                'PassportVerificationAU'      => [
                    'PassportVerified' => false,
                    'IsSuccess'        => false,
                ],
            ];
        }

        return $data["DataSets"]["InternationalIDAU"]['DocumentAUData'];
    }

    /**
     * Store centrix
     *
     * @return boolean
     */
    public static function store($data, $request)
    {
        $default        = self::defaultCentrix();
        $DocumentAUData = null;
        $Driverlicence  = null;
        if ($request->countryCode == 'AU') {
            $SmartID        = null;
            $DocumentAUData = self::defaultAU($data);
        } else {
            $SmartID       = $data["DataSets"]["SmartID"];
            $Driverlicence = $data["DataSets"]["DriverLicenceVerification"];
        }

        $DataSets        = $default['data']["DataSets"];
        $PepWatchlist    = $default['data']["DataSets"]["PEPWatchlistData"];
        $ResponseDetails = $data['ResponseDetails'];

        if ((is_array($Driverlicence) && !count($Driverlicence)) || !isset($Driverlicence['IsDriverLicenceVerified'])) {
            $Driverlicence = $default['data']["DataSets"]["DriverLicenceVerification"];
        }

        if (isset($data["DataSets"])) {
            $DataSets = $data["DataSets"];
        }

        if ($data && array_key_exists('PEPWatchlistData', $data["DataSets"])) {
            $PepWatchlist = $data["DataSets"]["PEPWatchlistData"];
        }

        $tr = $request->transaction->first();

        // Reset
        self::reset($tr);

        DB::transaction(function () use ($data, $request, $ResponseDetails, $DataSets, $tr, $SmartID, $Driverlicence, $PepWatchlist, $DocumentAUData) {

            // Store smart ID
            if ($SmartID) {
                $tr->centrixSmartId()->create([
                    'IsVerified'            => self::boolean($SmartID["IsVerified"]),
                    'IsNameVerified'        => self::boolean($SmartID["IsNameVerified"]),
                    'IsDateOfBirthVerified' => self::boolean($SmartID["IsDateOfBirthVerified"]),
                    'IsAddressVerified'     => self::boolean($SmartID["IsAddressVerified"]),
                ]);
            }

            // Store driver licence data
            if ($request->countryCode == 'AU') {
                $IDR = [];
                if (isset($DocumentAUData['IDResidentialAU'])) {
                    $IDR = $DocumentAUData['IDResidentialAU'];
                    $tr->centrixResidential()->create([
                        'IsSuccess'                         => self::boolean($IDR['IsSuccess']),
                        'Message'                           => $IDR['Message'],
                        'ResidentialVerified'               => self::boolean($IDR['ResidentialVerified']),
                        'ThirdPartyMatchType'               => $IDR['ThirdPartyMatchType'],
                        'ThirdPartyMatchTypeDescription'    => $IDR['ThirdPartyMatchTypeDescription'],
                        'Surname'                           => $IDR['Surname'],
                        'FirstName'                         => $IDR['FirstName'],
                        'MiddleName'                        => !is_array($IDR['MiddleName']) ? $IDR['MiddleName'] : null,
                        'DateOfBirth'                       => $IDR['DateOfBirth'],
                        'ThirdPartyNameMatch'               => $IDR['ThirdPartyNameMatch'],
                        'ThirdPartyNameMatchDescription'    => $IDR['ThirdPartyNameMatchDescription'],
                        'SurnameMatch'                      => self::boolean($IDR['SurnameMatch']),
                        'FirstNameMatch'                    => self::boolean($IDR['FirstNameMatch']),
                        'FirstNameInitialMatch'             => self::boolean($IDR['FirstNameInitialMatch']),
                        'MiddleNameMatch'                   => self::boolean($IDR['MiddleNameMatch']),
                        'MiddleNameInitialMatch'            => self::boolean($IDR['MiddleNameInitialMatch']),
                        'DateOfBirthMatch'                  => self::boolean($IDR['DateOfBirthMatch']),
                        'ThirdPartyAddressMatch'            => $IDR['ThirdPartyAddressMatch'],
                        'ThirdPartyAddressMatchDescription' => $IDR['ThirdPartyAddressMatchDescription'],
                        'AddressLine1'                      => self::stringify($IDR['AddressLine1']),
                        'Suburb'                            => self::stringify($IDR['Suburb']),
                        'State'                             => self::stringify($IDR['State']),
                        'Postcode'                          => self::stringify($IDR['Postcode']),
                        'DeliveryPointID'                   => self::stringify($IDR['DeliveryPointID']),
                        'UnitNoMatch'                       => self::boolean($IDR['UnitNoMatch']),
                        'StreetNoMatch'                     => self::boolean($IDR['StreetNoMatch']),
                        'StreetNameMatch'                   => self::boolean($IDR['StreetNameMatch']),
                        'StreetTypeMatch'                   => self::boolean($IDR['StreetTypeMatch']),
                        'SuburbMatch'                       => self::boolean($IDR['SuburbMatch']),
                        'StateMatch'                        => self::boolean($IDR['StateMatch']),
                        'PostcodeMatch'                     => self::boolean($IDR['PostcodeMatch']),
                        'HomePhoneNo'                       => self::stringify($IDR['HomePhoneNo']),
                        'MobilePhoneNo'                     => self::stringify($IDR['MobilePhoneNo']),
                        'PhoneNumberMatch'                  => self::boolean($IDR['PhoneNumberMatch']),
                        'MobileNumberMatch'                 => self::boolean($IDR['MobileNumberMatch']),
                        'EmailAddress'                      => self::stringify($IDR['EmailAddress']),
                    ]);
                }

                // Store DataSets
                $tr->centrixDataSets()->create([
                    'consumer_file'                            => self::boolean($DataSets['ConsumerFile']),
                    'driver_licence_verification'              => self::boolean($DataSets['DriverLicenceVerification']),
                    'known_names'                              => self::boolean($DataSets['KnownNames']),
                    'known_addresses'                          => self::boolean($DataSets['KnownAddresses']),
                    'previous_enquiries'                       => self::boolean($DataSets['PreviousEnquiries']),
                    'defaults'                                 => self::boolean($DataSets['Defaults']),
                    'judgments'                                => self::boolean($DataSets['Judgments']),
                    'insolvencies'                             => self::boolean($DataSets['Insolvencies']),
                    'company_affiliations'                     => self::boolean($DataSets['CompanyAffiliations']),
                    'file_notes'                               => self::boolean($DataSets['FileNotes']),
                    'fines_data_list'                          => self::boolean($DataSets['FinesDataList']),
                    'name_only_insolvencies'                   => self::boolean($DataSets['NameOnlyInsolvencies']),
                    'payment_history'                          => self::boolean($DataSets['PaymentHistory']),
                    'red_arrears_month_list'                   => self::boolean($DataSets['RedArrearsMonthList']),
                    'property_ownership'                       => self::boolean($DataSets['PropertyOwnership']),
                    'client_keys'                              => self::boolean($DataSets['ClientKeys']),
                    'identity_verification_data'               => self::boolean($DataSets['IdentityVerificationData']),
                    'bureau_header_data_identity_verification' => self::boolean($DataSets['BureauHeaderDataIdentityVerification']),
                    'summary_items'                            => self::boolean($DataSets['SummaryItems']),
                    'extra_data_items'                         => self::boolean($DataSets['ExtraDataItems']),
                    'application_decision'                     => self::boolean($DataSets['ApplicationDecision']),
                ]);

                if ($request->cardType !== 'PASSPORT') {
                    $licence    = $DocumentAUData['DriverLicenceVerificationAU'];
                    $isVerified = self::boolean($licence["DriverLicenceVerified"]);
                    $tr->centrixLicence()->create([
                        'NumberAndVersion'                  => false,
                        'IsSuccess'                         => self::boolean($licence["IsSuccess"]),
                        'IsDriverLicenceVerified'           => $isVerified,
                        'IsDriverLicenceLastNameMatched'    => $isVerified,
                        'IsDriverLicenceFirstNameMatched'   => $isVerified,
                        'IsDriverLicenceMiddleNameMatched'  => $isVerified,
                        'IsDriverLicenceDateOfBirthMatched' => $isVerified,
                        'IsDriverLicenceVerifiedAndMatched' => $isVerified,
                    ]);
                } else {
                    $passport   = $DocumentAUData["PassportVerificationAU"];
                    $isVerified = self::boolean($passport["PassportVerified"]);
                    $tr->centrixPassport()->create([
                        'IsSuccess'          => self::boolean($passport["IsSuccess"]),
                        'IsPassportVerified' => $isVerified,
                    ]);
                }
            } else {

                if ($request->cardType !== 'PASSPORT') {

                    $tr->centrixLicence()->create([
                        'NumberAndVersion'                  => !empty($Driverlicence["DriverLicenceNumber"]) && !empty($Driverlicence["DriverLicenceVersion"]),
                        'IsDriverLicenceVerified'           => self::boolean($Driverlicence["IsDriverLicenceVerified"]),
                        'IsDriverLicenceLastNameMatched'    => self::boolean($Driverlicence["IsDriverLicenceLastNameMatched"]),
                        'IsDriverLicenceFirstNameMatched'   => self::boolean($Driverlicence["IsDriverLicenceFirstNameMatched"]),
                        'IsDriverLicenceMiddleNameMatched'  => self::boolean($Driverlicence["IsDriverLicenceMiddleNameMatched"]),
                        'IsDriverLicenceDateOfBirthMatched' => self::boolean($Driverlicence["IsDriverLicenceDateOfBirthMatched"]),
                        'IsSuccess'                         => self::boolean($Driverlicence["IsSuccess"]),
                        'IsDriverLicenceVerifiedAndMatched' => self::boolean($Driverlicence["IsDriverLicenceVerifiedAndMatched"]),
                    ]);

                } else {
                    $PassportData = [
                        'IsSuccess'          => false,
                        'IsPassportVerified' => false,
                    ];
                    if ($data && array_key_exists('PEPWatchlistData', $data["DataSets"]["DIAPassport"])) {
                        $DIAPassport                        = $data["DataSets"]["DIAPassport"];
                        $PassportData['IsSuccess']          = self::boolean($DIAPassport["IsSuccess"]);
                        $PassportData['IsPassportVerified'] = self::boolean($DIAPassport["IsPassportVerified"]);
                    }
                    $tr->centrixPassport()->create($PassportData);
                }

            }

            // Store PEP watchlist
            $tr->centrixPep()->create([
                'IsSuccess'                     => self::boolean($PepWatchlist["IsSuccess"]),
                'InternationalWatchlistIsClear' => self::boolean($PepWatchlist["InternationalWatchlistIsClear"]),
                'ResponseDetails'               => json_encode($ResponseDetails),
            ]);

            if ($SmartID) {
                $centrixSmartId = $tr->centrixSmartId()->first();
                $smDataExists   = CentrixSmartIdData::where('sId', $centrixSmartId->id)->first();

                // Store smart ID data
                if ($SmartID["DataSourceMatchResults"] && !$smDataExists) {
                    foreach ($SmartID["DataSourceMatchResults"]["DataSourceMatchResult"] as $r) {
                        $centrixSmartId->data()->create([
                            'DataSourceName'         => $r["DataSourceName"],
                            'DataSourceDescription'  => $r["DataSourceDescription"],
                            'NameMatchStatus'        => $r["NameMatchStatus"],
                            'DateOfBirthMatchStatus' => $r["DateOfBirthMatchStatus"],
                            'AddressMatchStatus'     => $r["AddressMatchStatus"],
                        ]);
                    }
                }
            }
        });

        return true;
    }
}
