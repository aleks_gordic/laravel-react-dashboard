<?php

namespace App\Http\Models\BatchCheck;

use Illuminate\Database\Eloquent\Model;

class BatchEmail extends Model
{
    protected $table    = 'batch_email';
    protected $fillable = [
        'id',
        'bid',
        'tid',
        'contact',
        'reference'
    ];
}