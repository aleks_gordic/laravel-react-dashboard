<?php

namespace App\Http\Models\BatchCheck;

use Illuminate\Database\Eloquent\Model;

class BatchCheck extends Model
{
    protected $table    = 'batch_check';
    protected $fillable = [
        'id',
        'name',
        'method',
        'records',
        'completion'
    ];
}
