<?php

namespace App\Http\Models\GreenID;

use App\Http\Models\GreenID\Save;
use App\Http\Models\Transactions\Transactions;
use GuzzleHttp\Client;
use SimpleXMLElement;
use \Illuminate\Http\Request;

class GreenID
{
    /**
     * Make a request to get GreenID results.
     */
    public static function get($data = [])
    {
        try {
            $http = new Client([
                'http_errors' => false,
            ]);
            $request = $http->request('POST', config('greenid.endpoint'), [
                'headers' => [
                    'Content-Type' => 'text/xml',
                ],
                'body'    => self::request($data),
            ])->getBody();

            $json = self::xml2Json($request);

            if (isset($json['faultstring'])) {
                return [
                    'success' => false,
                    'message' => $json['faultstring'],
                ];
            }

            return ['success' => true, 'data' => $json];
        } catch (HttpException $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }
    }

    private static function request($data)
    {
        $type   = 'ACT';
        $ruleId = 'default';
        // Generate Drivers Licence State
        if ($data->transaction) {
            $tr = $data->transaction->first();
            if ($tr->extracted) {
                $extract = $tr->extracted;
                if ($extract && preg_match('#([A-Z]+)_([A-Z]+)_([A-Z]+)#i', $extract->cardType)) {
                    $type = preg_replace('#([A-Z]+)_([A-Z]+)_([A-Z]+)#i', '$2', $extract->cardType);
                }
            }

            if ($tr->ruleId) {
                $ruleId = $tr->ruleId;
            }
        }

        $request = [
            'accountId'                 => config('greenid.login.accountId'),
            'password'                  => config('greenid.login.password'),
            'ruleId'                    => $ruleId,
            'name'                      => [
                'givenName'   => ucfirst(strtolower($data->firstName)),
                'middleNames' => ucfirst(strtolower($data->middleName)),
                'surname'     => ucfirst(strtolower($data->lastName)),
            ],
            'currentResidentialAddress' => [
                'country'  => $data->countryCode == 'NZ' ? 'NZL' : 'AUS',
                'city'     => $data->address['city'],
                'postcode' => $data->address['postcode'],
                'suburb'   => $data->address['suburb'],
            ],
            'dob'                       => [],
            'generateVerificationToken' => 'false',
        ];

        // Set date of birth
        $dob = date('Y-m-d', strtotime($data->dateOfBirth));
        $dob = explode('-', $dob);

        $request['dob'] = [
            'year'  => $dob[0],
            'month' => $dob[1],
            'day'   => $dob[2],
        ];

        // Set drivers Licence Number or passport number
        if ($data->cardType !== 'PASSPORT') {
            $request[] = [
                [
                    'extraData' => [
                        'name'  => 'driversLicenceState',
                        'value' => $type,
                    ],
                ],
                [
                    'extraData' => [
                        'name'  => 'driversLicenceNumber',
                        'value' => $data->licenceNumber,
                    ],
                ],
            ];
        } else {
            $request[] = [
                [
                    'extraData' => [
                        'name'  => 'passportNumber',
                        'value' => $data->passportNumber,
                    ],
                ],
            ];
        }

        return self::toXML($request);
    }

    protected static function toXML($request)
    {
        $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dyn="http://dynamicform.services.registrations.edentiti.com/"><soapenv:Header/><soapenv:Body><dyn:registerVerification>';
        $xml .= self::tag($request);
        $xml .= '</dyn:registerVerification></soapenv:Body></soapenv:Envelope>';
        return $xml;
    }

    /**
     * Generate XML tag
     *
     * @param array|string
     * @return string
     */
    protected static function tag($data)
    {
        $tag = '';
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if ((is_array($value) && !count($value))) {
                    continue;
                }
                if (!is_int($key)) {
                    $tag .= '<' . $key . '>';
                }

                if (is_array($value)) {
                    $tag .= self::tag($value);
                } else {
                    $tag .= $value;
                }
                if (!is_int($key)) {
                    $tag .= '</' . $key . '>';
                }

            }
        } else {
            $tag .= $data;
        }

        return $tag;
    }

    private static function xml2Json($xml)
    {
        $xml = new SimpleXMLElement($xml);

        $body = $xml->xpath('//faultstring');
        if (isset($body[0])) {
            $body = (array) $body[0];
            return [
                'faultstring' => $body[0],
            ];
        }

        $body = $xml->xpath('//return');
        $json = (array) $body;

        return $json[0];
    }

    public static function save($data, $tr)
    {
        Save::registrationDetail($data, $tr);
        Save::sourceList($data, $tr);
        Save::verificationResult($data, $tr);
    }
}
