<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CentrixTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_centrix_smart_id_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sId')->unsigned();
            $table->foreign('sId')->references('id')->on('transaction_centrix_smart_id');
            $table->string('DataSourceName', 32);
            $table->text('DataSourceDescription');
            $table->string('NameMatchStatus', 32);
            $table->string('DateOfBirthMatchStatus', 32);
            $table->string('AddressMatchStatus', 32);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_centrix_smart_id_data');
    }
}
