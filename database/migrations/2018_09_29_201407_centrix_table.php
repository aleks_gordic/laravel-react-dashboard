<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CentrixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_centrix_smart_id', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->boolean('IsVerified');
            $table->boolean('IsNameVerified');
            $table->boolean('IsDateOfBirthVerified');
            $table->boolean('IsAddressVerified');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_centrix_smart_id');
    }
}
