<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64)->nullable();
            $table->string('slug', 64)->nullable();
            $table->timestamps();
        });

        DB::statement("DROP TABLE `rates`, `transaction_investment`;");
        DB::statement("ALTER TABLE `transactions` DROP `seen_at`;");
        DB::statement("ALTER TABLE `users` DROP `role`;");
        DB::statement("ALTER TABLE `permissions` DROP `user_id`;");

        Schema::table('users', function (Blueprint $table) {
            $table->string('role')->references('slug')->on('roles')->nullable();
            $table->string('created_by')->nullable();
        });

        $fields = config('database.user_meta_fields');
        if(count($fields) > 0){
            foreach ($fields as $field) {
                Schema::table('users', function (Blueprint $table) use ($field) {
                    $table->string(str_slug($field['name']))->nullable();
                });
            }
        }

        Schema::table('modules', function (Blueprint $table) {
            $table->string('parent_id')->nullable()->after('id');
        });

        Schema::table('permissions', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
