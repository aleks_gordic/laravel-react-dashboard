<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->string('firstName', 64);
            $table->string('lastName', 64);
            $table->string('middleName', 64)->nullable();
            $table->date('dateOfBirth')->nullable();
            $table->date('dateOfExpiry')->nullable();
            $table->string('licenceNumber', 13)->nullable();
            $table->string('versionNumber', 8)->nullable();
            $table->string('passportNumber', 13)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_user_details');
    }
}
