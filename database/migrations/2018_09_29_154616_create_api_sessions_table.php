<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned()->nullable();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->enum('type', ['NZL_DRIVERLICENCE', 'AUS_AUTO_DRIVERLICENCE', 'PASSPORT', 'VID', 'SELFIE']);
            $table->char('token', 32);
            $table->string('session_id', 20);
            $table->string('host', 50)->nullable();
            $table->text('files')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_sessions');
    }
}
