<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionGidRegistrationDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_gid_registration_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->string('city', 64)->nullable();
            $table->string('country', 3)->nullable();
            $table->string('streetType', 64)->nullable();
            $table->string('suburb', 64)->nullable();
            $table->string('dateCreated', 30)->nullable();
            $table->string('givenName', 64)->nullable();
            $table->string('middleNames', 64)->nullable();
            $table->string('surname', 64)->nullable();
            $table->string('dob', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_gid_registration_details');
    }
}
