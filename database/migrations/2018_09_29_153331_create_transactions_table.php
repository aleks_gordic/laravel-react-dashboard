<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('category')->references('slug')->on('categories')->nullable();
            $table->string('module')->references('slug')->on('modules')->nullable();

            $table->string('token', 250);
            $table->string('transactionId', 250);
            $table->string('original_token', 250);
            $table->string('reference', 64)->nullable();
            $table->string('username', 20)->nullable();
            $table->enum('identify_type', ['NZL_DRIVERLICENCE', 'AUS_ASW_DRIVERLICENCE', 'AUS_AUTO_DRIVERLICENCE', 'PASSPORT'])->default('PASSPORT');
            $table->enum('flow_type', ['MAIN', 'ALTERNATE'])->default('MAIN');
            $table->enum('instruction_type', ['GUIDED', 'SELF_GUIDED', 'BATCH_CHECK'])->default('SELF_GUIDED');
            $table->enum('country', ['NZ', 'AU', 'OTHER'])->default('AU');
            $table->boolean('confidence')->default(0);
            $table->boolean('centrix')->default(0);
            $table->string('joint_session')->nullable();
            $table->enum('message_template', ['RETAIL', 'COMMERCIAL', 'BROKER'])->default('RETAIL');
            $table->string('user_status', 20)->nullable();
            $table->enum('status', ['PENDING', 'INPROGRESS', 'FAILED', 'EXPIRED', 'COMPLETED','CANCELLED' , 'REDO'])->default('PENDING');
            $table->dateTime('activated_at')->nullable();
            $table->dateTime('seen_at')->nullable();
            $table->dateTime('completed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
