<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionCentrixDataSets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_centrix_data_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->boolean('consumer_file')->nullable();
            $table->boolean('driver_licence_verification')->nullable();
            $table->boolean('known_names')->nullable();
            $table->boolean('known_addresses')->nullable();
            $table->boolean('previous_enquiries')->nullable();
            $table->boolean('defaults')->nullable();
            $table->boolean('judgments')->nullable();
            $table->boolean('insolvencies')->nullable();
            $table->boolean('company_affiliations')->nullable();
            $table->boolean('file_notes')->nullable();
            $table->boolean('fines_data_list')->nullable();
            $table->boolean('name_only_insolvencies')->nullable();
            $table->boolean('payment_history')->nullable();
            $table->boolean('red_arrears_month_list')->nullable();
            $table->boolean('property_ownership')->nullable();
            $table->boolean('client_keys')->nullable();
            $table->boolean('identity_verification_data')->nullable();
            $table->boolean('bureau_header_data_identity_verification')->nullable();
            $table->boolean('summary_items')->nullable();
            $table->boolean('extra_data_items')->nullable();
            $table->boolean('application_decision')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_centrix_data_sets');
    }
}
