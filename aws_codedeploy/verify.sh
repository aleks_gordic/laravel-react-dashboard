#!/bin/bash
echo "Verifying Application"

set -e
for counter in {1..4}
do
    if [ $(curl -sL -w "%{http_code}\\n" "http://127.0.0.1:80/health-check" -o /dev/null --connect-timeout 3 --max-time 5) == "200" ] || [ $(curl -sL -w "%{http_code}\\n" "http://127.0.0.1:80" -o /dev/null --connect-timeout 3 --max-time 5) == "302" ];
    then 
    	echo "OK" ; 
    	exit 0
    else 
        echo "Sleeping for 30 seconds" ; 
    	sleep 30s
    fi
done

echo "Application not running . . ."
exit 1
