#!/bin/bash
echo "Installing Application"

set -e

cd /var/www/html
composer install --no-dev

#npm install
#php artisan key:generate

#Update Apache Configuration
find /var/www/html -user root -exec chown apache:apache {} \;

# Configure cron job
echo "* * * * * php /var/www/html/artisan schedule:run >> /dev/null 2>&1" > /var/spool/cron/apache

echo "INSTALL IS FINISHED SUCCESSFULLY"

