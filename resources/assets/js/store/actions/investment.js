export default class Document {

	/**
	 * Return an action to update the document info
	 * @param {Object} data The document data extracted
	 */
	static setAmount(data) {
		return {
			type: 'SET_AMOUNT',
			data,
		};
	}

    static setApplicants(data) {
        return {
            type: 'SET_APPLICANTS',
            data,
        };
    }

    static setRate(data) {
        return {
            type: 'SET_RATE',
            data,
        };
    }

    static setBankDetails(data) {
        return {
            type: 'SET_BANK_DETAILS',
            data,
        };
    }

}