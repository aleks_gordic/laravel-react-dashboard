const initialState = {
    amount: 0.00,
    applicants:{},
    rate:{},
    bankDetails:{},
};

export default function currentUser(state = initialState, action) {
    switch(action.type) {

        case 'SET_AMOUNT':
            return {
                ...state,
                amount: action.data
            }

        case 'SET_APPLICANTS':
            return {
                ...state,
                applicants: action.data
            }

        case 'SET_RATE':
            return {
                ...state,
                rate: action.data
            }

        case 'SET_BANK_DETAILS':
            return {
                ...state,
                bankDetails: action.data
            }


        default:
            return state;
    }
}

