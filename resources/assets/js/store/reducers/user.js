import {
  SET_CURRENT_USER,
  SET_CURRENT_SESSION,
  SET_TWO_STEP_STATUS,
  SET_SUB_TITLE_HEADER
} from "@js/store/actions/user";

const initialState = {
  info: null,
  session: "",
  twoStepVerified: false,
  subTitleHeader: false
};

export default function currentUser(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        info: action.data
      };
    case SET_CURRENT_SESSION:
      return {
        ...state,
        session: action.data
      };
    case SET_TWO_STEP_STATUS:
      return {
        ...state,
        twoStepVerified: action.data
      };
    case SET_SUB_TITLE_HEADER:
      return {
        ...state,
        subTitleHeader: action.data
      };
    default:
      return state;
  }
}
