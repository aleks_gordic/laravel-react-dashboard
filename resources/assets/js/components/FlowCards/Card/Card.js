import React, { Component } from 'react';
import {ASSETS_URL} from '@js/config';
import { Link } from "react-router-dom";

export default class Card extends Component {

	/** The component's constructor */
	constructor(props) {
		super(props);
		this.state = {
		};
	}


	/**
	 * Render the component's markup
	 * @return {ReactElement}
	 */
	render() {

		const { module, workflow  } = this.props

		return (
			<div className="retail-item bg-white mr-3">
				<Link to={`/portal/${module}/${workflow.slug}`}>
					<h6 className="item-title">{workflow.title.toLowerCase().replace(/^\w/, c => c.toUpperCase())}</h6>
				</Link>
			</div>
			);
	}
}
