import React, { Component } from 'react';
import {FormGroup,Label} from 'reactstrap'
import {ASSETS_URL} from '@js/config';
import Card from './Card';

class FlowCards extends Component {

	/** The component's constructor */
	constructor(props) {
		super(props);
		this.state = {
		};
	}


	/**
	 * Render the component's markup
	 * @return {ReactElement}
	 */
	render() {

		let { workFlows = [], module = '', title } = this.props
		let overall = false
		workFlows.forEach( wf => {
			overall = overall || wf.access
		})

		workFlows = workFlows.map((workflow)=>{

			if(!workflow.access || workflow.slug === 'role' || workflow.slug === 'email-alerts') return ''
			return (
				<Card key={workflow.id} module={module} workflow={workflow} />
			)
		})


		return (
				<FormGroup>
					{overall && <Label for="retail">{ title }</Label>}
					<div className="retail-container d-md-flex align-items-center">
						{workFlows}
					</div>
				</FormGroup>
			);
	}
}


export default FlowCards;

