import React, {Component} from 'react';
import { Button } from 'reactstrap';
import {ASSETS_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'
import  './SmsActivated.style.scss';
import { withRouter } from 'react-router-dom'
import {connect} from "react-redux";

import Transaction from '@services/Transaction'

class SmsActivated extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
        };
    }

    componentWillMount(){
        this.setState({
            id: this.props.location.hash.replace('#',''),
        })
    }

    handleClick = () => {

        let params = {
            id: this.state.id,
            action:"cancel"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.history.push(`/portal`);
                    success('Transaction cancelled successfully');
                }else{
                    error(data.message);
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

render() {

    return (
        <div className="custom-modal" >
            <div className="left-side" >
                <div className="outer-box">
                    <div className="inner-box">
                        <img className="image-wating" src={`${ASSETS_URL}/steps/waiting.png`} />
                    </div>
                </div>
            </div>

            <div className="right-side">
                <div className="right-margin">
                    <h4 className="dialog-title">SMS activated - review investment</h4>
                    <div className="under-line"></div>
                    <h5 className="dialog-subtitle">Introductions to the user:</h5>
                    <h5 className="dialog-description">Your investment details we discussed are shown on the screen.If this is correct,just tap "Accept".</h5>

                    <div className="cancel-button">
                        <Button onClick={this.handleClick} label='Cancel'  className="bottom-cacel">Cancel</Button>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}


export default withRouter(connect(mapStateToProps, null)(SmsActivated));
function mapStateToProps({user}) {
    return {session: user.session};
}