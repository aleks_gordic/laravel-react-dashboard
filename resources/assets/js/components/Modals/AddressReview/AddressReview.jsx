import React, {Component} from 'react';
import { Button } from 'reactstrap';

import {ASSETS_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'

import { withRouter } from 'react-router-dom'
import  './AddressReview.style.scss';
import {connect} from "react-redux";
import Transaction from '@services/Transaction'

class AddressReview extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
        };
    }

    componentWillMount(){
        this.setState({
            id: this.props.location.hash.replace('#',''),
        })
    }

    handleClick = () => {

        let params = {
            id: this.state.id,
            action:"cancel"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.history.push(`/portal`);
                    success('Transaction cancelled successfully');
                }else{
                    error(data.message);
                }
            })
            .catch(err => {
                console.warn(err);
            });

    }

    render() {
        return (
            <div className="custom-modal" >
                <div className="left-side" >

                    <div className="outer-box">
                        <div className="inner-box">
                            <img className="image-wating" src={`${ASSETS_URL}/steps/verifyAddress.png`} />
                        </div>
                    </div>
                </div>

                <div className="right-side">
                    <div className="right-margin">
                        <h4 className="dialog-title">Contact details review</h4>
                        <div className="under-line"></div>
                        <h5 className="dialog-subtitle">Instructions to the user:</h5>
                        <h5 className="dialog-description">Please either confirm the address extracted from your ID document or enter in your residential address. Please also enter or confirm your mobile number and email address</h5>


                        <div className="cancel-button">
                            <Button onClick={this.handleClick} label='Cancel'  className="bottom-cacel">Cancel</Button>
                        </div>
                    </div>
                </div>
            </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, null)(AddressReview));
function mapStateToProps({user}) {
    return {session: user.session};
}