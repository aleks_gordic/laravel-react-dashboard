import React, {Component} from 'react';
import { Button } from 'reactstrap';
import {ASSETS_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'
import { withRouter } from 'react-router-dom'
import  './CaptureInfoScreen.style.scss';
import {connect} from "react-redux";
import Transaction from '@services/Transaction'

class CaptureInfoScreen extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
        };
    }

    componentWillMount(){
        this.setState({
            id: this.props.location.hash.replace('#',''),
        })
    }

    handleClick = () => {

        let params = {
            id: this.state.id,
            action:"cancel"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.history.push(`/portal`);
                    success('Transaction cancelled successfully');
                }else{
                    error(data.message);
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    render() {
        return (
            <div className="custom-modal" >
                <div className="left-side" >

                    <div className="outer-box">
                        <div className="inner-box">
                            <img className="image-wating" src={`${ASSETS_URL}/steps/captureFace.png`} />
                        </div>
                    </div>
                </div>

                <div className="right-side">
                    <div className="right-margin">
                        <h4 className="dialog-title">Facial recognition - information screens</h4>
                        <div className="under-line"></div>
                        <h5 className="dialog-subtitle">Instructions to the user:</h5>
                        <h5 className="dialog-description">We will now need to do a face verification. Please make sure you follow the instructions. It will ask you to perform some actions such as smiling or turning your head a certain direction, be sure to follow when asked.</h5>

                        <h5 className="dialog-descriptiontitle">Notes</h5>
                        <h5 className="dialog-description">- User can wear reading/transparent glasses.</h5>
                        <h5 className="dialog-description">- User can wear a head dress, scarf or hat if needed as long as the face can be clearly seen.</h5>
                        <div className="cancel-button">
                            <Button label='Cancel' onClick={this.handleClick} className="bottom-cacel">Cancel</Button>
                        </div>
                    </div>
                </div>
            </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, null)(CaptureInfoScreen));
function mapStateToProps({user}) {
    return {session: user.session};
}