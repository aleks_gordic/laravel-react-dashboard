import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import { Button, Modal } from 'reactstrap';
import { ASSETS_URL } from '@js/config';
import  './BatchUserPrompt.style.scss';

class BatchUserPrompt extends Component  {

    constructor(props) {
        super(props);
        this.state = {};
    }

    toggle = (e) => {
        e.preventDefault()
        this.props.onClose();
    }

    render() {

        const { onSubmit, show, userCount, processing} = this.props

        return (
          <div>
            <Modal size="md" isOpen={show||false} className="batch-user-prompt modal-class">
                <div className="inner-record text-center">
                    <h2>{userCount} Users have been found in the batch file.</h2>
                    <h4>Are you sure you would like to create users?</h4>
                    <div className="mt-4">
                        <Button disabled={processing} onClick={this.toggle} outline={true} color="primary">Cancel</Button>
                        <Button disabled={processing} onClick={() => onSubmit()} color="primary">{(processing) ? 'Please wait...': 'Confirm'}</Button>
                    </div>
                </div>

            </Modal>
        </div>
    );
  }
}

export default  withRouter(BatchUserPrompt)