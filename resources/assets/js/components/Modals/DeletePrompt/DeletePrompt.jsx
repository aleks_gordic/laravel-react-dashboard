import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import { Button, Modal } from 'reactstrap';
import { ASSETS_URL } from '@js/config';
import  './DeletePrompt.style.scss';

class DeletePrompt extends Component  {

    constructor(props) {
        super(props);
        this.state = {};
    }

    toggle = (e) => {
        e.preventDefault()
        this.props.onClose();
    }

    render() {

        const { onSubmit, show} = this.props

        return (
          <div>
            <Modal toggle={this.toggle} size="md" isOpen={show||false} className="modal-class">
                <div className="inner-record text-center">
                    <h5>Are you sure you wish to <br/> delete record?</h5>
                    <div className="mt-4">
                        <Button onClick={this.toggle} outline={true} color="primary">Cancel</Button>
                        <Button onClick={() => onSubmit()} color="primary">Delete</Button>
                    </div>
                </div>

            </Modal>
        </div>
    );
  }
}

export default  withRouter(DeletePrompt)