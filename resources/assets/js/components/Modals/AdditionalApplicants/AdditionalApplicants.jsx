import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import  './AdditionalApplicants.style.scss';

class AdditionalApplicants extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            reference:'NA',
            applicants:[

            ]
        };
    }


    handleClick = () => {
        this.props.history.push(`/portal`);
    }


    render() {

        let {additional} = this.props;

        let reference = additional.reference || '';
        let applicants = additional.applicants || [];

        let applicantsRecords = applicants.map((app,i)=>{
            let contact;
            if(app.medium === 'sms'){
                contact = app.mobile;
            }else if(app.medium === 'email'){
                contact = app.email;
            }else{
                contact = 'NA'
            }

            return (
                <div className="applicant-props" key={i}>
                    <h5 className="applicant-name">{i+2}. {app.full_name || 'Applicant '+(i+2)}</h5>
                    <h5 className="applicant-phonenumber"> -   {contact}</h5>
                </div>
            );
        })



        return (
          <div>
            <Modal size="lg" isOpen={this.props.show||false} className="modal-class">
                <ModalBody className="single-Modal">
              
                    <div className="applicants-modal">       
                        <h4 className="dialog-title">Identification request sent</h4>
                        <div className="under-line"></div>
                        <div className="subtitle-group">
                            <h5 className="subtitle-key  dialog-marginRight">Reference:   </h5>
                            <h5 className="dialog-subtitle subtitle-value">{reference}  </h5>
                        </div>
                        <h5 className="dialog-description">Identification requests have been sent to the other applicants at:</h5>
                        <div className="applicant-info">
                            {applicantsRecords}
                        </div>
                        <Button onClick={this.handleClick} label='Close'  className="dialog-close">Close</Button>
                    </div>                     
               
                </ModalBody>
            </Modal>
        </div>
    );
  }
}

export default  withRouter(AdditionalApplicants)