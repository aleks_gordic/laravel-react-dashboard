import React, {Component} from 'react';
import { Button } from 'reactstrap';
import {ASSETS_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'
import  './AwaitingSMS.style.scss';

import { withRouter } from 'react-router-dom'
import {connect} from "react-redux";

import Transaction from '@services/Transaction'

class AwaitingSMS extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            id:'',
        };
    }

    componentWillMount(){
        this.setState({
            id: this.props.location.hash.replace('#',''),
        })
    }

    handleClick = () => {
        let params = {
            id: this.state.id,
            action:"cancel"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.history.push(`/portal`);
                    success('Transaction cancelled successfully');
                }else{
                    error(data.message);
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    render() {
        return (
            <div className="custom-modal" >
                <div className="left-side" >

                    <div className="outer-box">
                        <div className="inner-box">
                            <img className="image-wating" src={`${ASSETS_URL}/steps/waiting.png`} />
                        </div>
                    </div>
                </div>

                <div className="right-side">
                    <div className="right-margin">
                        <h4 className="dialog-title">Awaiting SMS Activation</h4>
                        <div className="under-line"></div>
                        <h5 className="dialog-subtitle">Instructions to the user:</h5>
                        <h5 className="dialog-description">Please open the SMS message I have just sent you and tap on the link to start the process.</h5>

                        <h5 className="dialog-descriptiontitle">Notes</h5>
                        <h5 className="dialog-description">For iOS they need to press the darker grey area on the message if they are having trouble.</h5>
                        <div className="cancel-button">
                            <Button onClick={this.handleClick} label='Cancel'  className="bottom-cacel">Cancel</Button>
                        </div>
                    </div>
                </div>
            </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, null)(AwaitingSMS));
function mapStateToProps({user}) {
    return {session: user.session};
}