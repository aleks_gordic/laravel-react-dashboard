import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import * as XLSX from 'xlsx';
import { Button, Modal } from 'reactstrap';
import Dropzone from 'react-dropzone'
import { success, error } from "@js/lib/Tostify";
import { ASSETS_URL } from '@js/config';
import BatchUserPrompt from '@components/Modals/BatchUserPrompt'
import User from '@services/User'
import  './BatchUser.style.scss';

class BatchUser extends Component  {

    constructor(props) {
        super(props);
        this.state = {
            openProcess: false,
            isFileUploaded: false,
            processedUsers: [],
            processing: false,
        };
    }

    toggle = (e) => {
        e.preventDefault()
        this.props.onClose();
    }

    processUser = () => {
        const { processedUsers } = this.state
        const { fields = [] } = this.props

        const mappings = {
            email: 'Email',
            first_name: 'First Name',
            last_name: 'Last Name',
            role: 'Role',
            mobile: 'Mobile (optional)',
        }

        fields.forEach( field => {
            mappings[field.key] = field.label + ' (optional)'
        })

        const inputs = processedUsers.map( user => {
            let temp = {};
            Object.keys(mappings).forEach(key => {
                temp[key] = user[mappings[key]];
            })

            return temp;
        })

        this.setState({
            processing: true,
        })
        User.addBatchUsers(inputs).then(({status, message}) => {
            if(status === 'success'){
                success('Users processed successfully.');
                this.setState({
                    openProcess: false,
                    processedUsers:[],
                    processing: false,
                })
                this.props.onSubmit();
            }else{
                error(message);
            }
        }).catch(e => {
            error(e.message);
        })
    }

    onDrop = (accepted, rejected) => {
        if (Object.keys(rejected).length !== 0) {
            error("Please submit valid file type");
        } else {

            let f = accepted[0];
            const reader = new FileReader();
            reader.onload = (evt) => {
                const bstr = evt.target.result;
                const wb = XLSX.read(bstr, {type:'binary'});
                wb.SheetNames.forEach((sheetName) => {
                    let XL_row_object = XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);
                    this.setState({
                        processedUsers: XL_row_object,
                        openProcess: true,
                    })
                    this.props.onClose();
                })
            };
            reader.readAsBinaryString(f);
        }
    }

    download() {

        User.download()
            .then((data) => {
                const downloadUrl = window.URL.createObjectURL(new Blob([data]));
                const link = document.createElement('a');
                link.href = downloadUrl;

                link.setAttribute('download', 'BatchUser.xlsx'); //any other extension

                document.body.appendChild(link);
                link.click();
                link.remove();
            })
            .catch(err => {
                console.warn(err);
            });
    }

    renderBatchUserProcessModal= () => {
        return (
            <BatchUserPrompt
                processing={this.state.processing}
                userCount={this.state.processedUsers.length || 0}
                show={this.state.openProcess}
                onClose={() => this.setState({openProcess: false})}
                onSubmit={this.processUser}
            />
        )
    }

    render() {

        const { onSubmit, show} = this.props
        return (
          <div>
            <Modal toggle={this.toggle} isOpen={show||false} className="modal-class">
                <div className="batch-user">
                    <h3 className="heading mb-3">Invite User - Batch Upload</h3>
                    <ol>
                        <li>
                            Download template and fill the details.
                            <div className="mt-2">
                                <Button onClick={this.download}>Download  <img src={`${ASSETS_URL}/icons/download.png`} /></Button>
                            </div>
                        </li>
                        <li>
                            After filling the template with the details, drag the file on to the box below to click upload.
                        </li>
                    </ol>
                    <Dropzone
                        onDrop={(accepted, rejected) => this.onDrop(accepted, rejected)}
                        multiple={false}
                        accept="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">


                        {({getRootProps, getInputProps, acceptedFiles}) => (
                            <div className="dotted-border margin-bottom margin-left" {...getRootProps()}>
                                <input {...getInputProps()} />
                                <div className="center">
                                    {this.state.isFileUploaded ?
                                        <div className="text-center">
                                            <h4 className="file-fontsize" hide={(!this.state.isFileUploaded).toString()}>
                                                {acceptedFiles.length > 0 && acceptedFiles[0].name}
                                            </h4>
                                        </div> :
                                        <div hide={(this.state.isFileUploaded).toString()}>
                                            <div className="mt-5 text-center">
                                                <img src={`${ASSETS_URL}/icons/upload.png`} width="30"/>
                                            </div>
                                            <div className="mt-2 text-center">
                                                <h4 className="description"><span>Choose a file</span> or drag it here </h4>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                        )}
                    </Dropzone>
                    <div className="batch-user-btns d-flex mt-4">
                        <Button
                            outline={true}
                            onClick={this.toggle}>
                            Close
                        </Button>
                        <Button className="ml-auto" onClick={() => onSubmit()}>
                            Process
                        </Button>
                    </div>
                </div>
            </Modal>
              {this.renderBatchUserProcessModal()}
        </div>
    );
  }
}

export default  withRouter(BatchUser)