import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import classnames from 'classnames';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ResetPasswordEmail from '@components/ResetPassword/ResetPasswordEmail'
import { ASSETS_URL } from '@js/config';
import  './ResetPassword.style.scss';

class ResetPassword extends Component  {

    constructor(props) {
        super(props);
        this.state = {};
    }

    toggle = (e) => {
        e.preventDefault()
        this.props.onClose();
    }

    render() {

        const { show } = this.props

        return (
          <div>
            <Modal size="md" isOpen={show||false} toggle={this.toggle} className="modal-class">
                {/*<ModalHeader toggle={this.toggle}>Reset Password</ModalHeader>*/}
                <ResetPasswordEmail />
            </Modal>
        </div>
    );
  }
}

export default  withRouter(ResetPassword)