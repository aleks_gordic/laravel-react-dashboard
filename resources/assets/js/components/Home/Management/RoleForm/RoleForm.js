import React, { Component } from "react";
import { Form, FormGroup, Label, Col, Input, Button } from "reactstrap";
import PropTypes from "prop-types";
import "./RoleForm.style.scss";
import Role from "@services/Role";
import { success, error } from "@js/lib/Tostify";
import LoadingScreen from "@components/Loading";

class RoleForm extends Component {
  /** The component's constructor */
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  handleChange = e => {
    let field = $(e.target).attr("name");
    this.props.onFieldChange(field, e.target.value);
  };

  createNewRole = e => {
    this.setState({ loading: true });
    e.preventDefault();

    let { role } = this.props;
    Role.create(
      { name: role.name }
    ).then(res => {
      if (res.status === "success") {
        //success
        success("Role created !");
        this.props.onSubmit();
      } else {
        //error
        error("Error occured while creating a new role!");
      }
      this.setState({ loading: false });
    });
  };

  render() {
    const { prefix, role } = this.props;
    const { loading } = this.state;
    return (
      <div className="form-fields">
        {loading && <LoadingScreen />}
        <Form>
          <FormGroup row>
            <Col sm="12">
              <Label for={`${prefix}-name`}>Name</Label>
              <Input
                type="text"
                name="name"
                id={`${prefix}-name`}
                value={role.name || ""}
                onChange={this.handleChange}
              />
            </Col>
          </FormGroup>
          {this.props.cta && (
            <Col className=" cta-container d-flex justify-content-between">
              <Button color="secondary" outline className="cancel">
                {" "}
                Close
              </Button>

              <Button color="secondary" onClick={this.createNewRole}>
                {" "}
                Create
              </Button>
            </Col>
          )}
        </Form>
      </div>
    );
  }
}

RoleForm.propTypes = {
  role: PropTypes.object,
  prefix: PropTypes.string,
  cta: PropTypes.bool,
  onFieldChange: PropTypes.func,
  onSubmit: PropTypes.func
};

RoleForm.defaultProps = {
  user: {
    id: "",
    name: "",
  },
  prefix: "edit",
  cta: false,
  onFieldChange: () => {},
  onSubmit: () => {}
};

export default RoleForm
