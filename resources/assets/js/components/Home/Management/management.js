import React, { Component } from 'react';
import {FormGroup,Label} from 'reactstrap'
import { withRouter } from 'react-router';
import {ASSETS_URL} from '@js/config';
import { Link } from "react-router-dom";

import { connect } from 'react-redux';

class Management extends Component {

	/** The component's constructor */
	constructor(props) {
		super(props);
        this.state = {
            modules:[],
            menu:''
        };
	}

    formatPermission = () =>{
        let permissions = this.props.user.info.permissions || [];
        let categories = [];

        permissions.forEach(perm => {
            let catExist = categories.findIndex(cat => {
                return cat.id === perm.category.id
            })

            if(catExist === -1){
                categories.push(perm.category)
            }

        });

        permissions.forEach(perm => {
            let catExist = categories.findIndex(cat => {
                return cat.id === perm.category.id
            })

            if(catExist > -1){
                if(!categories[catExist]['modules']){
                    categories[catExist]['modules'] = [];
                }

                let modExists = categories[catExist]['modules'].findIndex(mod => {
                    return mod.id === perm.id
                });

                if(modExists === -1){
                    categories[catExist]['modules'].push({
                        id: perm.id,
                        title: perm.title,
                        slug: perm.slug,
                        access: perm.access,
                    })
                }

            }

        });

        return categories;
    }

    componentDidMount() {
        const {match} = this.props;
        let selected = ""
        let permissions = this.props.user.info.permissions || [];

        if(match && match.params && match.params.module){
            selected = match.params.module;

            let category = permissions.filter((perm)=>{
                return perm.slug === selected
            })

            category = category[0]
            this.setState({
                modules:category.modules,
                menu:selected
            })

        }

    }

	/**
	 * Render the component's markup
	 * @return {ReactElement}
	 */
	render() {

        let isLoggedIn = !(_.isEmpty(this.props.user.info))

        let {modules,menu} = this.state
        modules = this.state.modules || []
        modules = modules.map((module)=>{

            if(!module.access) return ''
            return (
                <div key={module.id} className="retail-item bg-white mr-3">
                    <Link to={`/portal/${menu}/${module.slug}`}>
                        <h6 className="item-title">{module.title}</h6>
                    </Link>
                </div>
            )
        })

		return (
				<FormGroup>
					<Label for="retail">Workflow</Label>
					<div className="retail-container d-md-flex align-items-center">
						{modules}
					</div>
				</FormGroup>
			);
	}
}

export default withRouter(connect(mapStateToProps, null)(Management));


function mapStateToProps({user}) {
    return {user};
}
