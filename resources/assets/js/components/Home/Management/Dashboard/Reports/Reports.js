import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
} from "reactstrap";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import Loading from "@components/PageLoader";

import Dashboard from "@services/Dashboard";
import "./Reports.style.scss";

library.add(faCaretDown);
library.add(faCaretUp);


function Reports({ session }) {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState(sessionReportSampleData)

  useEffect(() => {
    setLoading(true)

    let headers = {
      Authorization: "Bearer " + session
    };

    Dashboard.getSessionReport(headers).then((reportData) => {
      if (typeof(reportData.sessionsToday) !== 'undefined') {
        setData(reportData)
      } else {
        setData(sessionReportSampleData)
      }
      setLoading(false)
    }).catch((e) => {
      setLoading(false)
    })
  }, [])

  const todayUp = data.sessionsToday > data.sessionsYesterDaySameTime
  const thisWeekUp = data.sessionsThisWeek > data.sessionsLastWeekSameDay
  const thisMonthUp = data.sessionsThisMonth > data.sessionsLastMonthSameDay

  return (
    <div className="report-header-container mb-3">
      <Row className="m-0">
        <Col className="header-item">
          <div className="title">Sessions today</div>
          <div className="big-number-container">
            <FontAwesomeIcon color={todayUp ? "green" : "red"} icon={todayUp ? "caret-up" : "caret-down"} size="2x"/>
            <div className="big-number">{data.sessionsToday}</div>
          </div>
        </Col>
        <Col className="header-item">
          <div className="title">Sessions this week</div>
          <div className="big-number-container">
            <FontAwesomeIcon color={thisWeekUp ? "green" : "red"} icon={thisWeekUp ? "caret-up" : "caret-down"} size="2x"/>
            <div className="big-number">{data.sessionsThisWeek}</div>
          </div>
        </Col>
        <Col className="header-item">
          <div className="title">Sessions this month</div>
          <div className="big-number-container">
            <FontAwesomeIcon color={thisMonthUp ? "green" : "red"} icon={thisMonthUp ? "caret-up" : "caret-down"} size="2x"/>
            <div className="big-number">{data.sessionsThisMonth}</div>
          </div>
        </Col>
        <Col className="header-item">
          <div className="title">Sessions this quarter</div>
          <div className="big-number-container">
            <div className="big-number">{data.sessionsThisQuarter}</div>
          </div>
        </Col>
      </Row>
      <Row className="m-0">
        <Col className="header-item">
          <div className="gray-title">Sessions Yesterday</div>
          <div className="sub-container">
            <div className="sub-label">Same time</div>
            <div className="sub-number">
              {data.sessionsYesterDaySameTime}
            </div>
          </div>
          <div className="sub-container">
            <div className="sub-label">Total</div>
            <div className="sub-number">
              {data.sessionsYesterDayTotal}
            </div>
          </div>
        </Col>
        <Col className="header-item">
          <div className="gray-title">Sessions Last Week</div>
          <div className="sub-container">
            <div className="sub-label">Same day</div>
            <div className="sub-number">
              {data.sessionsLastWeekSameDay}
            </div>
          </div>
          <div className="sub-container">
            <div className="sub-label">Total</div>
            <div className="sub-number">
              {data.sessionsLastWeekTotal}
            </div>
          </div>
        </Col>
        <Col className="header-item">
          <div className="gray-title">Sessions Last Month</div>
          <div className="sub-container">
            <div className="sub-label">Same day</div>
            <div className="sub-number">
              {data.sessionsLastMonthSameDay}
            </div>
          </div>
          <div className="sub-container">
            <div className="sub-label">Total</div>
            <div className="sub-number">
              {data.sessionsLastMonthTotal}
            </div>
          </div>
        </Col>
        <Col className="header-item">
            <div className="gray-title">Sessions all time</div>
            <div className="big-number-container">
              <div className="big-number">{data.sessionsAllTime}</div>
            </div>
        </Col>
      </Row>
      {loading && <Loading />}
    </div>
  );
}

function mapStateToProps({ user }) {
  return { session: user.session };
}

export default connect(mapStateToProps, null)(Reports)

const sessionReportSampleData = {
  sessionsToday: 0,
  sessionsThisWeek: 0,
  sessionsThisMonth: 0,
  sessionsThisQuarter: 0,
  sessionsYesterDaySameTime: 0,
  sessionsYesterDayTotal: 0,
  sessionsLastWeekSameDay: 0,
  sessionsLastWeekTotal: 0,
  sessionsLastMonthSameDay: 0,
  sessionsLastMonthTotal: 0,
  sessionsAllTime: 0,
}