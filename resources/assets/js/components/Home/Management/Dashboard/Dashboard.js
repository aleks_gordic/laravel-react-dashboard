import React, { Component } from "react";
import {
    Container,
    TabContent, 
    TabPane,
    Nav, 
    NavItem, 
    NavLink,
} from "reactstrap";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import "./Dashboard.style.scss";
import Reports from "./Reports";
import Analytics from "./Analytics";
import Incomplete from "./Incomplete";
import Usage from "./Usage";
import classnames from 'classnames';


class Dashboard extends Component {
    /** The component's constructor */
  constructor(props) {
    super(props);

    this.state = {
      activeTab: 'Usage'
    };
  }

  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    return (
      <section className="management-dashboard mt-2 mb-5">
        <Container>
          <Reports/>
          <Nav tabs>
            <NavItem className="mangement-tab">
              <NavLink
                className={classnames({ activeTab: this.state.activeTab === 'Usage', inactiveTab: this.state.activeTab !== 'Usage' })}
                onClick={() => { this.toggle('Usage'); }}
              >
                Usage
              </NavLink>
            </NavItem>
            <NavItem className="mangement-tab">
              <NavLink
                className={classnames({ activeTab: this.state.activeTab === 'Analytics', inactiveTab: this.state.activeTab !== 'Analytics' })}
                onClick={() => { this.toggle('Analytics'); }}
              >
                Analytics
              </NavLink>
            </NavItem>
            <NavItem className="mangement-tab">
              <NavLink
                className={classnames({ activeTab: this.state.activeTab === 'Incomplete', inactiveTab: this.state.activeTab !== 'Incomplete' })}
                onClick={() => { this.toggle('Incomplete'); }}
              >
                Incomplete
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="Usage">
              <Usage/>
            </TabPane>
            <TabPane tabId="Analytics">
              <Analytics/>
            </TabPane>
            <TabPane tabId="Incomplete">
              <Incomplete/>
            </TabPane>
          </TabContent>
        </Container>
      </section>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(Dashboard)
);
function mapStateToProps({ user }) {
  return { session: user.session, user: user.info };
}
