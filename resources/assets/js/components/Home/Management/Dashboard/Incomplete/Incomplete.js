import React, { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import { connect } from "react-redux"; 
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import Dashboard from "@services/Dashboard";
import DatePicker from "react-datepicker";
import Loading from "@components/PageLoader";
import "./Incomplete.style.scss";
import "react-datepicker/dist/react-datepicker.css";

library.add(faCalendarAlt);

function Incomplete({ session }) {
	const [loading, setLoading] = useState(true)
	const [data, setData] = useState(inCompleteReportSample)
	const [startDate, setStartDate] = useState(new Date(Date.now() - 864e5))
	const [endDate, setEndDate] = useState(new Date())

	useEffect(() => {
		setLoading(true)
	
		let headers = {
		  Authorization: "Bearer " + session
		};
		let params = {
		  startOfDate: Math.round(startDate.getTime()/1000),
		  endOfDate: Math.round(endDate.getTime()/1000)
		};
	
		Dashboard.getIncompleteReport(headers, params).then((incompleteData) => {
			if (typeof(incompleteData.totalSessionsIncomplete) !== 'undefined') {
				setData(incompleteData)
			} else {
				setData(inCompleteReportSample)
			}
			setLoading(false)
		}).catch((e) => {
		  	setLoading(false)
		})
	}, [startDate, endDate])


	return (
		<div className="incomplete-container mt-3">
			<Row className="m-0 mb-3">
				<div className="date-picker mr-3">
					<FontAwesomeIcon icon="calendar-alt" size="lg"/>
					<DatePicker
						selected={startDate}
						onChange={date => {setStartDate(date)}}
						maxDate={endDate}
						dateFormat="MMMM d, yyyy"
						placeholderText="From"
						ClassName="rasta-stripes"
					/>
				</div>
				<div className="date-picker">
					<FontAwesomeIcon icon="calendar-alt" size="lg"/>
					<DatePicker
						selected={endDate}
						onChange={date => {setEndDate(date)}}
						minDate={startDate}
						dateFormat="MMMM d, yyyy"
						placeholderText="To"
						ClassName="rasta-stripes"
					/>
				</div>
			</Row>
			<div className="anlaytics">
				<Row className="figures m-0">
					<Col className="header-item">
						<div className="gray-title">Total sessions incomplete</div>
						<div className="big-number-container">
							<div className="big-number">{data.totalSessionsIncomplete}</div>
						</div>
					</Col>
					<Col className="header-item">
						<div className="gray-title">Total sessions ignored</div>
						<div className="big-number-container">
							<div className="big-number">{data.totalSessionsIgnored}</div>
						</div>
					</Col>
					<Col className="header-item">
						<div className="gray-title">Total sessions attempted not completed</div>
						<div className="big-number-container">
							<div className="big-number">{data.totalSessionAttemptedNotCompleted}</div>
						</div>
					</Col>
				</Row>
				<Row className="status-details p-5 m-0">
					<Row className="status-group mb-0">
						<Col className="status-figures">
							<div className="group">
								<label>Incomplete Type Breakdown</label>
								<Row className="item m-0">
									<Col>Ignored/Expired</Col>
									<Col>{data.incompleteTypeBreakdown.ignoredExpired.times} times</Col>
									<Col>{data.incompleteTypeBreakdown.ignoredExpired.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Time out during flow</Col>
									<Col>{data.incompleteTypeBreakdown.timeoutDuringFlow.times} times</Col>
									<Col>{data.incompleteTypeBreakdown.timeoutDuringFlow.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Exited during flow</Col>
									<Col>{data.incompleteTypeBreakdown.exitedDuringFlow.times} times</Col>
									<Col>{data.incompleteTypeBreakdown.exitedDuringFlow.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Device not compatible</Col>
									<Col>{data.incompleteTypeBreakdown.deviceNotCompatible.times} times</Col>
									<Col>{data.incompleteTypeBreakdown.deviceNotCompatible.percentage}%</Col>
								</Row>
								<Row className="item total m-0">
									<Col>TOTAL</Col>
									<Col>{data.incompleteTypeBreakdown.total.times} times</Col>
									<Col>{data.incompleteTypeBreakdown.total.percentage}%</Col>
								</Row>
							</div>
							<div className="group">
								<label>Timed Out Breakdown</label>
								<Row className="item m-0 mt-3">
									<Col>Timed out on Terms Screen</Col>
									<Col>{data.timedOutBreakdown.termsScreen.times} times</Col>
									<Col>{data.timedOutBreakdown.termsScreen.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Timed out on Overview Screen</Col>
									<Col>{data.timedOutBreakdown.overviewScreen.times} times</Col>
									<Col>{data.timedOutBreakdown.overviewScreen.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Timed out on ID Section</Col>
									<Col>{data.timedOutBreakdown.idSelection.times} times</Col>
									<Col>{data.timedOutBreakdown.idSelection.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Timed out on Information Review</Col>
									<Col>{data.timedOutBreakdown.informationReview.times} times</Col>
									<Col>{data.timedOutBreakdown.informationReview.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Timed out on Address Screen</Col>
									<Col>{data.timedOutBreakdown.addressScreen.times} times</Col>
									<Col>{data.timedOutBreakdown.addressScreen.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Timed out on Face Capture</Col>
									<Col>{data.timedOutBreakdown.faceCapture.times} times</Col>
									<Col>{data.timedOutBreakdown.faceCapture.percentage}%</Col>
								</Row>
								<Row className="item total m-0">
									<Col>TOTAL</Col>
									<Col>{data.timedOutBreakdown.total.times} times</Col>
									<Col>{data.timedOutBreakdown.total.percentage}%</Col>
								</Row>
							</div>
							<div className="group">
								<label>Exited Breakdown</label>
								<Row className="item m-0 mt-3">
									<Col>Exited Terms Screen</Col>
									<Col>{data.exitedBreakdown.termsScreen.times} times</Col>
									<Col>{data.exitedBreakdown.termsScreen.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Exited on Overview Screen</Col>
									<Col>{data.exitedBreakdown.overviewScreen.times} times</Col>
									<Col>{data.exitedBreakdown.overviewScreen.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Exited on Information Review</Col>
									<Col>{data.exitedBreakdown.informationReview.times} times</Col>
									<Col>{data.exitedBreakdown.informationReview.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Exited on Address Screen</Col>
									<Col>{data.exitedBreakdown.addressScreen.times} times</Col>
									<Col>{data.exitedBreakdown.addressScreen.percentage}%</Col>
								</Row>
								<Row className="item total m-0">
									<Col>TOTAL</Col>
									<Col>{data.exitedBreakdown.total.times} times</Col>
									<Col>{data.exitedBreakdown.total.percentage}%</Col>
								</Row>
							</div>
							<div className="group">
								<label>Never Initiated Breakdown</label>
								<Row className="item m-0 mt-3">
									<Col>SMS session</Col>
									<Col>{data.neverInitiatedBreakdown.smsSession.times} times</Col>
									<Col>{data.neverInitiatedBreakdown.smsSession.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Email Session</Col>
									<Col>{data.neverInitiatedBreakdown.emailSession.times} times</Col>
									<Col>{data.neverInitiatedBreakdown.emailSession.percentage}%</Col>
								</Row>
								<Row className="item total m-0">
									<Col>TOTAL</Col>
									<Col>{data.neverInitiatedBreakdown.total.times} times</Col>
									<Col>{data.neverInitiatedBreakdown.total.percentage}%</Col>
								</Row>
							</div>
						</Col>
					</Row>
				</Row>
			</div>
			{loading && <Loading />}
		</div>
	)
}
export default
  connect(
    mapStateToProps,
    null
  )(Incomplete);

function mapStateToProps({ user }) {
  return { session: user.session };
}


const inCompleteReportSample={
  totalSessionsIncomplete: 0,
  totalSessionsIgnored: 0,
  totalSessionAttemptedNotCompleted: 0,
  incompleteTypeBreakdown: {
    ignoredExpired: {
      times: 0,
      percentage: 0
    },
    timeoutDuringFlow: {
      times: 0,
      percentage: 0
    },
    exitedDuringFlow: {
      times: 0,
      percentage: 0
    },
    deviceNotCompatible: {
      times: 0,
      percentage: 0
    },
    total: {
      times: 0,
      percentage: 0
    },
  },
  timedOutBreakdown: {
    termsScreen: {
      times: 0,
      percentage: 0,
    },
    overviewScreen: {
      times: 0,
      percentage: 0,
    },
    idSelection: {
      times: 0,
      percentage: 0,
    },
    informationReview: {
      times: 0,
      percentage: 0,
    },
    addressScreen: {
      times: 0,
      percentage: 0,
    },
    faceCapture: {
      times: 0,
      percentage: 0,
    },
    total: {
      times: 0,
      percentage: 0,
    },
  },
  exitedBreakdown: {
    termsScreen: {
      times: 0,
      percentage: 0,
    },
    overviewScreen: {
      times: 0,
      percentage: 0,
    },
    informationReview: {
      times: 0,
      percentage: 0,
    },
    addressScreen: {
      times: 0,
      percentage: 0,
    },
    total: {
      times: 0,
      percentage: 0,
    },
  },
  neverInitiatedBreakdown: {
    smsSession: {
      times: 0,
      percentage: 0,
    },
    emailSession: {
      times: 0,
      percentage: 0,
    },
    total: {
      times: 0,
      percentage: 0,
    },
  }
}
