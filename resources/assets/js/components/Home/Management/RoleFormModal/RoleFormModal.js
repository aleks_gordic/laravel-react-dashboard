import React from "react";
import { Modal } from "reactstrap";
import RoleForm from "@components/Home/Management/RoleForm";
import { ASSETS_URL } from "@js/config";

import "./RoleFormModal.style.scss";

class RoleFormModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      role: {
        name: "",
      }
    };
  }

  toggle = e => {
    e.preventDefault();
    this.props.onToggleUserFormModal();
  };

  handleChange = (field, value) => {
    let role = { ...this.state.role };
    role[field] = value;
    this.setState({
      role
    });
  };

  handleSubmit = () => {
    this.props.onSubmit();
  };

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.modalOpen}
          fade={false}
          toggle={this.toggle}
          className="role-form-modal"
          centered
        >
          <RoleForm
            role={this.state.role}
            disableUsername={false}
            onFieldChange={this.handleChange}
            onSubmit={this.handleSubmit}
            cta={true}
            prefix="create"
          />
        </Modal>
      </div>
    );
  }
}

export default RoleFormModal;
