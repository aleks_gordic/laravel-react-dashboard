import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import UserFormModal from "@components/Home/Management/UserFormModal";
import API from "@services/APIs";
import UserAPI from "@services/User";
import RoleAPI from "@services/Role";
import { success, error } from "@js/lib/Tostify";
import { ASSETS_URL, API_URL } from "@js/config";
import NoRecordFoundGeneric from "@components/NoRecordFoundGeneric";
import { withRouter } from "react-router-dom";
import Pagination from "@components/Pagination";
import UserProfile from "./UserProfile";
import DeletePrompt from '@components/Modals/DeletePrompt'
import BatchUser from '@components/Modals/BatchUser'
import "./User.style.scss";

library.add(faPlus);

class User extends Component {
  /** The component's constructor */
  constructor(props) {
    super(props);

    this.state = {
      users: {
        data: []
      },
      selected: {},
      permissions: {},
      modalOpen: false,
      query: "",
      page: 1,
      userPermission: [],
      fields: [],
      roles:[],
      showBatchUser: false,
    };
  }

  componentWillMount() {
    this.fetchUsers();
  }

  componentDidMount() {
    const { user } = this.props
    this.fetchFields();
    this.fetchRoles();
  }

  changeCurrentUser = (e, user) => {
    e.preventDefault();
    this.setState({
      selected: user
    });

  };

  handleEditUserChange = (field, value) => {
    let selected = { ...this.state.selected };
    selected[field] = value;
    this.setState({
      selected
    });
  };

  handleSubmitUserChange = () => {
    let { selected, fields = []} = this.state
    selected = { ...this.state.selected };
    let params = {
      id: selected.id,
      username: selected.username,
      first_name: selected.first_name,
      last_name: selected.last_name,
      mobile: selected.mobile,
      email: selected.email,
      role: selected.role
    };

    fields.forEach( field => {
      params[field.key] = selected[field.key]
    })


    if (selected.password) {
      params["password"] = selected.password;
    }

    let headers = {
      Authorization: "Bearer " + this.props.session
    };
    API.updateUser(params, headers).then(res => {
      if (res.status === "success") {
        //success
        this.fetchUsers(params.id);
        success("User updated successfully!");
      } else {
        //error
        error("Error in creating user !");
      }
    });
    //console.log('submit', params)
  }

  resetTwoStep = e => {
    let selected = { ...this.state.selected };

    let params = {
      id: selected.id
    };
    let headers = {
      Authorization: "Bearer " + this.props.session
    };

    API.resetTwoStep(params, headers).then(res => {
      if (res.status === "success") {
        //success
        this.fetchUsers(params.id);
        success("User updated successfully!");
      } else {
        //error
        error("Error in creating user !");
      }
    });
  }

  resetPassword = e => {
    let selected = { ...this.state.selected };

    let params = {
      email: selected.email
    };
    let headers = {
      Authorization: "Bearer " + this.props.session
    };

    API.resetPassword(params, headers).then(res => {
      if (res.status === "success") {
        //success
        this.fetchUsers(params.id);
        success("User updated successfully!");
      } else {
        //error
        error("Error in creating user !");
      }
    });
  }
  //-----------------------
  toggleStatus = () => {
    const { selected } = this.state;
    let params = {
      id: selected.id,
      active: !selected.active
    };
    let headers = {
      Authorization: "Bearer " + this.props.session
    };
    API.updateUser(params, headers).then(res => {
      //console.log('res',res)
      if (res.status === "success") {
        //success
        this.fetchUsers(params.id);
        success("User updated successfully!");
      } else {
        //error
        error("Error in creating user !");
      }
    });
  };

  deleteUser = () => {
    let { selected } = this.state;
    let headers = {
      Authorization: "Bearer " + this.props.session
    };
    API.deleteUser(selected.id, headers).then(res => {
      //console.log('res',res)
      if (res.status === "success") {
        //success
        this.setState({showDeletePrompt: false, selected: {}});
        this.fetchUsers();
        success("User deleted successfully!");
      } else {
        //error
        error("Error in deleting user !");
      }
    });
  };

  fetchUsers = (setSelected = false) => {
    let headers = {
      Authorization: "Bearer " + this.props.session
    };

    let params = {
      page: this.state.page || 1
    };

    API.fetchUser(null, headers, params).then(res => {
      if (res.status === "success") {
        //success

        let data = {
          users: res.data
        };

        if (setSelected) {
          let i;
          //console.log(data.users)
          i = data.users.data.findIndex(user => {
            return setSelected === user.id;
          });

          if (i > -1) {
            data["selected"] = data.users.data[i];
          } else {
            data["selected"] = data.users.data[0];
          }
        }

        this.setState({ ...data });
      } else {
        //error
        error("Error in fetching users!");
      }
    });
  };

  fetchFields = () => {
    let headers = {
      Authorization: "Bearer " + this.props.session
    };

    UserAPI.getFields({}, headers).then((fields) => {
      this.setState({
        fields
      })
    })
  }

  fetchRoles = () =>{
    let headers = {
      Authorization: "Bearer " + this.props.session
    };

    let params = {
      page: this.state.page || 1,
      option: 'all'
    };

    RoleAPI.read( params, headers).then(res => {
      if (res.status === "success") {
        //success
        this.setState({
          roles: res.data
        })

      }
    });
  }

  handleChange = e => {
    let field = $(e.target).attr("name");

    this.setState({
      [field]: e.target.value
    });

    this.debounceHandleUpdate(e.target.value);
  };

  debounceHandleUpdate = _.debounce(input => this.searchUser(input), 500, {
    maxWait: 500
  });

  searchUser = q => {
    let headers = {
      Authorization: "Bearer " + this.props.session
    };
    API.searchUser(q, headers).then(res => {
      //console.log('res',res)
      this.setState({
        users: res,
        //selected: res.data.length > 0 ? res.data[0] : {}
      });
    });
  };

  clearSearch = () => {
    this.setState({
      query: ""
    });
    this.fetchUsers();
  };
  //--------------
  userCreated = () => {
    this.setState({
      modalOpen: !this.state.modalOpen
    });
    this.fetchUsers();
  };

  renderNewUserModal = () => {
    return (
      <UserFormModal
        onClickBatchUser={()=> this.setState({showBatchUser: true, modalOpen: false})}
        modalOpen={this.state.modalOpen}
        onSubmit={this.userCreated}
        roles={this.state.roles}
        fields={this.state.fields}
        onToggleUserFormModal={this.toggleModal}
      />
    );
  };

  toggleModal = () => {
    this.setState({
      modalOpen: !this.state.modalOpen
    });
  };
  //----------------------------
  handlePageChange = async currentPage => {
    await this.setState({
      page: currentPage || 1
    });

    this.fetchUsers();
  };
  //----------------------------
  handleDelete = (e) => {
    e.preventDefault()
    this.setState({
      showDeletePrompt: true,
    })
  }

  renderDeletePrompt = () =>{
    return (
        <DeletePrompt
            show={this.state.showDeletePrompt}
            onClose={() => this.setState({showDeletePrompt: false})}
            onSubmit={this.deleteUser}
        />
    )
  }

  renderBatchUser = () => {
    return (
        <BatchUser
            show={this.state.showBatchUser}
            onClose={() => this.setState({showBatchUser: false})}
            fields={this.state.fields}
            onSubmit={(e)=>{
              this.fetchUsers()
            }}
        />
    )
  }

  render() {

    const { user } = this.props
    const { selected = {}, userPermission = [] } = this.state;

    let statusText = selected.active ? "Disable" : "Enable";

    let hasUsers = Object.keys(selected).length > 0;

    let userList = this.state.users.data.map(user => {
      let classname = user.id === this.state.selected.id ? "active" : "";
      return (
        <li key={user.id}>
          <a
            href="#"
            className={classname}
            onClick={e => this.changeCurrentUser(e, user)}
          >
            {user.first_name + ' '+ user.last_name}
          </a>
        </li>
      );
    });

    let Norecords;
    if (this.state.query) {
      let cta = {
        show: true,
        text: "Reset Search"
      };
      Norecords = (
        <NoRecordFoundGeneric
          message="No records found"
          onClickCta={this.clearSearch}
          cta={cta}
        />
      );
    } else {
      let cta = {
        show: false
      };
      Norecords = (
        <NoRecordFoundGeneric message="Please select a record" cta={cta} />
      );
    }

    //pagination
    let paginate = this.state.users || {};
    paginate = {
      current_page: paginate.current_page,
      last_page: paginate.last_page
    };

    const isCurrentUser =  selected.id === user.id

    return (
      <section className="management-user mt-2 mb-5">
        <Container>
          <Row className="content-area m-0">
            <Col sm="3" className="p-0 bg-white">
              <h4 className="m-0 col-heading d-flex">Users
                <span onClick={() => this.props.history.push("/portal/management/role")} className="ml-auto mr-2"><img src={`${ASSETS_URL}/icons/settings.png`} width="19" alt="" /></span>
                <span onClick={this.toggleModal}><FontAwesomeIcon
                  icon="plus"
                  size="lg"
                  className=""/>
                </span>
                </h4>
              <ul className="user-list primary-actions">
                <li>
                  <Form>
                    <FormGroup className="p-0">
                      <img src={`${ASSETS_URL}/icons/search.png`} alt="" />
                      <Input
                          type="text"
                          value={this.state.query}
                          name="query"
                          onChange={this.handleChange}
                          placeholder="Search"
                          className="search-field"
                      />
                    </FormGroup>
                  </Form>
                </li>
                {userList}
              </ul>
            </Col>
            <Col sm="9" className="pl-3 pr-0">
              <div className="bg-white h-100">
                <h4 className="m-0 col-heading">Profile</h4>

                {hasUsers && (
                    <div>
                      {!isCurrentUser && <div className="secondary-actions mt-2 d-flex">
                        <Button
                            className="btn-management ml-auto"
                            outline
                            onClick={this.toggleStatus}
                            color="gray"
                        >
                          {statusText} Access
                        </Button>
                        <Button
                            className="btn-management ml-1"
                            outline
                            color="gray"
                            onClick={this.handleDelete}
                        >
                          Delete
                        </Button>
                      </div>}
                      <UserProfile
                          onFieldChange={values => this.setState({selected: {...this.state.selected,...values}})}
                          resetPassword={this.resetPassword}
                          resetTwoStep={this.resetTwoStep}
                          data={selected}
                          loggedInUser={user}
                          fields={this.state.fields}
                          roles={this.state.roles}
                      />

                      <div className="secondary-actions d-flex mb-3">
                        <Button
                            className="ml-auto btn-management"
                            outline
                            onClick={e =>  this.setState({selected:{}})}
                            color="primary"
                        >
                          Cancel
                        </Button>
                        <Button
                            className="btn-management ml-2 mr-2"
                            onClick={e => this.handleSubmitUserChange(e)}
                            color="primary"
                        >
                          Save
                        </Button>
                      </div>
                    </div>
                )}

                {!hasUsers && Norecords}
              </div>
            </Col>
          </Row>
          <div className="float-right mt-3">
            <Pagination onPageChange={this.handlePageChange} data={paginate} />
          </div>
          {this.renderNewUserModal()}
          {this.renderDeletePrompt()}
          {this.renderBatchUser()}
        </Container>
      </section>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(User)
);
function mapStateToProps({ user }) {
  return { session: user.session, user: user.info };
}
