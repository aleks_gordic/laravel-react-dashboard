import React, { Component } from "react";
import { Form, FormGroup, Label, Col, Input, Button } from "reactstrap";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./UserForm.style.scss";
import API from "@services/APIs";
import { success, error } from "@js/lib/Tostify";
import LoadingScreen from "@components/Loading";

class UserForm extends Component {
  /** The component's constructor */
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  handleChange = e => {
    let field = $(e.target).attr("name");
    this.props.onFieldChange(field, e.target.value);
  };

  createNewUser = e => {
    this.setState({ loading: true });
    e.preventDefault();

    let { user } = this.props;

    let headers = {
      Authorization: "Bearer " + this.props.session
    };

    API.createUser(
      { ...user, fullName: `${this.props.firstName} ${this.props.lastName}` },
      headers
    ).then(res => {
      if (res.status === "success") {
        //success
        success("User created !");
        this.props.onSubmit();
      } else {
        //error
        if (Object.keys(res.messages)[0] == "email")
          error(res.messages.email[0]);
        else error("Error occured while creating a new user!");
      }
      this.setState({ loading: false });
    });
  };

  render() {
    const { prefix, user, roles=[], fields = [] } = this.props;
    const { loading } = this.state;

    const roleOptions = roles.map((role)=> {
      return (
          <option key={role.slug} value={role.slug}>{role.name}</option>
      )
    })

    const customFields = fields.map(field => {
      return (
          <Col sm="6" key={field.key}>
            <Label for={`${prefix}-${field.key}`}>{field.label}</Label>
            <Input
                type="text"
                name={field.key}
                id={`${prefix}-${field.key}`}
                value={user[field.key]|| ""}
                onChange={this.handleChange}
            />
          </Col>
      )
    })

    return (
      <div className="form-fields">
        {loading && <LoadingScreen />}
        <Form>
          <FormGroup row>
            <Col sm="6">
              <Label for={`${prefix}-first_name`}>First Name</Label>
              <Input
                type="text"
                name="first_name"
                id={`${prefix}-first_name`}
                value={user.first_name || ""}
                onChange={this.handleChange}
              />
            </Col>
            <Col sm="6">
              <Label for={`${prefix}-last_name`}>Last Name</Label>
              <Input
                type="text"
                name="last_name"
                value={user.last_name || ""}
                onChange={this.handleChange}
              />
            </Col>
          </FormGroup>
            <FormGroup row>
              <Col sm={12} className="select-container pr-0">
                <Input
                  onChange={this.handleChange}
                  value={user.role || ""}
                  type="select"
                  name="role"
                  id={`${prefix}-role`}
                >
                  <option value="">Select Role</option>
                  {roleOptions}
                </Input>
              </Col>
            </FormGroup>

          <FormGroup column>
            <Col>
              <Label for={`${prefix}-email`}>Company eMail</Label>
              <Input
                onChange={this.handleChange}
                type="email"
                name="email"
                id={`${prefix}-email`}
                value={user.email || ""}
              />
            </Col>
          </FormGroup>
          <FormGroup column>
            <Col>
              <Label for={`${prefix}-mobile`}>Mobile</Label>
              <Input
                  onChange={this.handleChange}
                  type="number"
                  name="mobile"
                  id={`${prefix}-mobile`}
                  value={user.mobile || ""}
              />
            </Col>
          </FormGroup>
          {fields.length > 0 && <FormGroup row>
            {customFields}
          </FormGroup>}

          {this.props.cta && (
            <Col className=" cta-container d-flex justify-content-between">
              <Button color="secondary" outline className="cancel">
                {" "}
                Close
              </Button>

              <Button color="secondary" onClick={this.createNewUser}>
                {" "}
                Send Invite
              </Button>
            </Col>
          )}
        </Form>
      </div>
    );
  }
}

UserForm.propTypes = {
  user: PropTypes.object,
  prefix: PropTypes.string,
  cta: PropTypes.bool,
  onFieldChange: PropTypes.func,
  onSubmit: PropTypes.func
};

UserForm.defaultProps = {
  user: {
    id: "",
    first_name: "",
    last_name: "",
    username: "",
    email: "",
    mobile: "",
    password: "",
    role: "",
    active: 0
  },
  prefix: "edit",
  cta: false,
  onFieldChange: () => {},
  onSubmit: () => {}
};

export default connect(
  mapStateToProps,
  null
)(UserForm);
function mapStateToProps({ user }) {
  return {
    session: user.session,
    firstName: user.info.first_name,
    lastName: user.info.last_name
  };
}
