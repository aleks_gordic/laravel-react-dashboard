import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from "reactstrap";
class RolePermissions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data:{
        name: '',
      }
    };
  }

  componentDidMount() {
    const {
    data: {
      name
     }
    } = this.props;
    const data = {
      name
    };

    this.setState({data});
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {
      data: {
        name,
      }
    } = nextProps;

    const data = {
      name
    };

    this.setState({data});
  }

  handleChange = (e) => {
    let field = $(e.target).attr("name");
    let val = e.target.value;

    this.setState({
      [field] : val
    })

    let inputs = {...this.state}
    inputs[field] = val
    this.props.onFieldChange(inputs);

  }

  render() {
    const {
      name
    } = this.state.data;

    return (
        <FormGroup className="username--section row mt-5">
          <Label className="col-2 col-form-label">Name</Label>
          <Input
              className="col-10"
              name="name"
              defaultValue={name}
              onChange={this.handleChange}
          />
        </FormGroup>
    );
  }
}

export default RolePermissions;
