import React, { Component } from 'react';
import { Table, Input, Button, Container } from 'reactstrap';
import classnames from 'classnames';
import moment from 'moment'
import Datetime from 'react-datetime'
import { ASSETS_URL } from '@js/config';
import { success, error } from '@js/lib/Tostify';
import { withRouter } from 'react-router-dom';
import Pagination from '@components/Pagination';
import {connect} from "react-redux";
import User from '@services/User'

import Transaction from '@services/Transaction'
import DeletePrompt from '@components/Modals/DeletePrompt'
import { setC, getC } from '@js/lib/Cookie';

import 'react-datetime/css/react-datetime.css';
import './CustomerRepository.style.scss';

/**
 * Customer Review - customer Repository
 */
class CustomerRepository extends Component {

    constructor(props) {
        super(props);
        this.state = {
            transactions: [],
            users: [],
            filter: {
                type: '',
                result: '',
                channel: '',
                username: '',
                dateFrom: '',
                dateTo: '',
            },
            query: {
                term: ''
            },
            export: 'csv',
            page: 1,
            showDeletePrompt: false,
            toDelete: null,
            userPermission: [],
            autoRefresh: null
        };
    }

    viewTrans = (e, id) => {
        e.preventDefault();
        //console.log('viewTrans id',id)
        this.props.history.push(`/portal/transaction/${id}`);
    };

    delete = () => {

        let id = this.state.toDelete
        if(!id) return

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        
        Transaction.delete( id, headers)
            .then(({ status }) => {
                if ( status === 'success') {
                    this.setState({
                        showDeletePrompt: false,
                        toDelete: null
                    });
                    success('Transaction deleted successfully.');
                    this.fetchTransactions();
                } else {
                    error('Error in deleting transaction.');
                }
            })
            .catch(err => {
                console.warn(err);
            });
    };

    debounceHandleUpdate = _.debounce(() => this.fetchTransactions(), 500, { maxWait: 500 });

    validateDate = (field, dateMoment) => {
        const now = moment();
        const { filter: { dateTo, dateFrom } } = this.state

        if(dateMoment > now || (field === 'dateTo' && dateMoment < moment(dateFrom, 'YYYY-MM-DD')) || (field === 'dateFrom' && dateMoment > moment(dateTo, 'YYYY-MM-DD'))){
            return false
        }

        return true
    }

    handleDateChange = (field, val) => {
        const { page, filter, query } = this.state
        let date = val.format('YYYY-MM-DD');

        const newFilter = {...filter}
        newFilter[field] = date

        setC('filter', JSON.stringify(newFilter), 1)

        this.setState({
            filter: newFilter
        })

        const params = { page, query, filter: newFilter}
        this.fetchTransactions(params)

    }

    handleChange = async e => {
        let field = $(e.target).attr('name');
        let val = e.target.value;

        if (field === 'term') {
            let query = { ...this.state.query };
            query[field] = val;

            setC('query', JSON.stringify(query), 1)

            await this.setState({
                query
            });
        }
        if (field === 'export') {
            await this.setState({
                export: val
            });

            this.handleExport();
        } else {
            let filter = { ...this.state.filter };
            filter[field] = val;

            setC('filter', JSON.stringify(filter), 1)

            await this.setState({
                filter
            });
        }

        this.debounceHandleUpdate();
    };

    fetchUsers = () => {
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        let params = {
            show: 'all',
        };

        User.read(params, headers)
            .then(({ status, data }) => {
                let users = data.map(user => user.username);
                this.setState({
                    users: users.sort()
                });
            })
            .catch(err => {
                console.warn(err);
            });
    };

    fetchTransactions = (params = null) => {
        console.log('session==>', this.props.session)
        let headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.props.session
        };

        if(!params){
            params = {
                page: this.state.page || 1,
                filter: this.state.filter,
                query: this.state.query
            }
        }

        Transaction.getCompleted( params, headers )
            .then((res) => {
                this.setState({
                    transactions: res
                });
                //console.log(data)
            })
            .catch(err => {
                console.warn(err);
            });
    };

    componentWillMount() {
        const params = {
            filter: getC('filter') ? JSON.parse(getC('filter')) : {},
            query: getC('query') ? JSON.parse(getC('query')) : {},
            //page: getC('page') ? getC('page') : 1,
        }

        this.setState({...this.state,...params})
        this.fetchTransactions(params);
        this.fetchUsers();
    }

    formatUserPermission = () =>{
        let permissions = this.props.user.permissions || [];
        let item = permissions.find((perm) => {
            return perm.slug === 'customer-repository'
        })

        if(item){
            let sub_permissions = item.sub_permissions
            sub_permissions = sub_permissions.filter(perm => {
                return perm.access
            })

            sub_permissions = sub_permissions.map(perm => perm.slug)

            this.setState({
                userPermission: sub_permissions
            })
        }

    }

    componentDidMount() {
        this.setAutoRefresh();
        this.formatUserPermission()
    }

    componentWillUnmount() {
        const { autoRefresh } = this.state
        clearInterval(autoRefresh)
        this.setState({
            autoRefresh: null
        })
    }

    handleExport = () => {
        $('#exportFormComplete').submit();
    };

    //----------------------------
    handlePageChange = async currentPage => {

        setC('page', currentPage || 1, 1)
        await this.setState({
            page: currentPage || 1
        });

        this.fetchTransactions();
    };
    //----------------------------

    handleDelete = (e, id) => {
        e.preventDefault()
        this.setState({
            showDeletePrompt: true,
            toDelete: id
        })
    }

    renderDeletePrompt = () =>{
        return (
            <DeletePrompt
                show={this.state.showDeletePrompt}
                onClose={() => this.setState({showDeletePrompt: false, toDelete: null})}
                onSubmit={this.delete}
            />
        )
    }

    setAutoRefresh = () => {
        const { MIX_AUTOREFRESH, MIX_AUTOREFRESH_INTERVAL } = process.env
        const enableAutorefresh = MIX_AUTOREFRESH === 'true'

        let autoRefresh;
        if(enableAutorefresh){
            autoRefresh = setInterval(() => {
                this.fetchTransactions();
            }, 1000 * parseInt(MIX_AUTOREFRESH_INTERVAL))
        }

        this.setState({
            autoRefresh
        })
    }

    showClear = () => {
        let filter = getC('filter') ? JSON.parse(getC('filter')) : {}
        let query = getC('query') ? JSON.parse(getC('query')) : {}

        let combined  = {...filter, ...query}

        let overall = [];
        Object.keys(combined).forEach( key => {
            if(key && combined[key]){
                overall.push(combined[key])
            }
        })

        if(overall.length < 1){
            setC('filter', {}, -1)
            setC('query', {}, -1)
            return false
        }

        return true
    }

    clearSearchnFilter = () => {

        const filter = {...this.state.filter}
        filter['result'] = '';
        filter['username'] = '';
        filter['dateFrom'] = '';
        filter['dateTo'] = '';

        const query = {...this.state.query}
        query['term'] = '';

        this.setState({
            filter,
            query
        })
        setC('filter', {}, -1)
        setC('query', {}, -1)
        this.fetchTransactions({
            filter: {},
            query: {}
        })
    }

    render() {

        const { userPermission, users, transactions } = this.state
        const { MIX_ENABLE_CENTRIX='true' } = process.env
        const enableCentrix = MIX_ENABLE_CENTRIX === 'true'

        const showClear = this.showClear()
        //----------
        let options = users.map(user => {
            return (
                <option key={user} value={user}>
                    {user}
                </option>
            );
        });
        //----

        let data = transactions.data || [];
        console.log({data})
        let transactionRows = data.map(trans => {
            let success = `${ASSETS_URL}/icons/green-check-only.png`;
            let failed = `${ASSETS_URL}/icons/failed.png`;

            //-------------
            const { contact_details = {}, user_details = {} } = trans;

            const { email = null, name = null, phone= null } = contact_details;
            const { firstName = null, lastName = null } = user_details;

            let contact;
            if( email ){
                contact = email;
            } else {
                contact = phone;
            }

            let result = '';
            if(enableCentrix){
                if(trans.Data && trans.Face && trans.Live && trans.Doc){
                    result = 'Completed - Pass'
                }else{
                    result = 'Completed - Flagged'
                }
            }else{
                if(trans.Face && trans.Live && trans.Doc){
                    result = 'Completed - Pass'
                }else{
                    result = 'Completed - Flagged'
                }
            }

            let seenCondition = false
            if(enableCentrix){
                if(trans.Data && trans.Face && trans.Live && trans.Doc){
                    seenCondition = true
                }
            }else{
                if(trans.Face && trans.Live && trans.Doc){
                    seenCondition = true
                }
            }


            return (
                <tr key={trans.id}>
                    <td scope="row"><span className={classnames("seen-circle", {
                        'unseen-failed' : (!seenCondition && !trans.seenByUser),
                        'unseen-success' : (seenCondition && !trans.seenByUser),
                        'seen' : (trans.seenByUser),
                    })}>23</span></td>
                    <td>{trans.completed_at}</td>
                    <td>{trans.transactionId}</td>
                    <td>{trans.reference || 'NA'}</td>
                    <td>{result}</td>
                    <td>{(firstName) || 'NA'}</td>
                    <td>{(lastName) || 'NA'}</td>
                    <td>{contact}</td>
                    <td>{trans.username || 'NA'}</td>
                    {enableCentrix && <td>
                        {trans.Data !== 'NA' && <center>
                            <a>
                                {' '}
                                <img
                                    style={{ width: '19px' }}
                                    className="action-imgs"
                                    src={trans.Data ? success : failed}
                                />
                            </a>
                        </center>}
                        {trans.Data === 'NA' && 'NA'}
                    </td>}
                    <td>
                        <center>
                            <a>
                                {' '}
                                <img
                                    style={{ width: '19px' }}
                                    className="action-imgs"
                                    src={trans.Face ? success : failed}
                                />
                            </a>
                        </center>
                    </td>
                    <td>
                        <center>
                            <a>
                                {' '}
                                <img
                                    style={{ width: '19px' }}
                                    className="action-imgs"
                                    src={trans.Live ? success : failed}
                                />
                            </a>
                        </center>
                    </td>
                    <td>
                        <center>
                            <a>
                                {' '}
                                <img style={{ width: '19px' }} className="action-imgs"
                                     src={(trans.Doc) ? success : failed} />
                            </a>
                        </center>
                    </td>
                    <td>
                        <center>
                            <a href="#" onClick={e => this.viewTrans(e, trans.id)}>
                                <img
                                    style={{ width: '19px' }}
                                    className="action-imgs"
                                    src={`${ASSETS_URL}/icons/view.png`}
                                />
                            </a>
                        </center>
                    </td>
                    {userPermission.includes('delete-records') && <td>
                        <center>
                            <a href="#" onClick={(e) => this.handleDelete(e,trans.id)}> <img className="action-imgs"
                                                                                             src={`${ASSETS_URL}/icons/trash.png`} style={{width:"15px"}}/>
                            </a></center>
                    </td>}
                </tr>
            );
        });
        //pagination
        let paginate = transactions || {};
        paginate = { current_page: paginate.current_page, last_page: paginate.last_page };

        return (
            <Container>
                <div className="customerRepository-body">
                    <div className="inputGroup d-flex">
                        <div className="quick-search">
                            <div className="searchPendingClass">
                                <span className="spanPendingSearch">
                                    <img
                                        className="searchimage"
                                        src={`${ASSETS_URL}/icons/search.png`}
                                    />
                                </span>
                                <Input
                                    onChange={this.handleChange}
                                    type="text"
                                    value={this.state.query.term}
                                    className="searcPendinghInput"
                                    name="term"
                                    id="quicksearch"
                                    placeholder="Quick Search"
                                />
                            </div>
                        </div>
                        <div className="Status ml-auto">
                            <div className="selectPendingClass select-container">
                                <select
                                    onChange={this.handleChange}
                                    value={this.state.filter.result}
                                    name="result"
                                    className="selectPendingInput form-control"
                                >
                                    <option value="">All Status</option>
                                    <option value="completed-pass">Completed-Pass</option>
                                    <option value="completed-flagged">Completed-Flagged</option>
                                </select>
                            </div>
                        </div>
                        <div className="User">
                            <div className="selectPendingClass select-container">
                                <select
                                    onChange={this.handleChange}
                                    value={this.state.filter.username}
                                    name="username"
                                    className="selectPendingInput form-control"
                                >
                                    <option value="">User</option>
                                    {options}
                                </select>
                            </div>
                        </div>
                        <div className="date-pick">
                            <Datetime
                                isValidDate={(moment) => this.validateDate('dateFrom', moment)}
                                onChange={(moment) => this.handleDateChange('dateFrom',moment)}
                                inputProps={{'placeholder': 'Date from',name: 'dateFrom'}}
                                dateFormat="YYYY-MM-DD"
                                value={this.state.filter.dateFrom ? moment(this.state.filter.dateFrom, 'YYYY-MM-DD').toDate() : ''}
                                timeFormat={false} />
                            <img src="/assets-portal/images/icons/calendar.svg" alt=""/>
                        </div>
                        <div className="date-pick">
                            <Datetime
                                isValidDate={(moment) => this.validateDate('dateTo', moment)}
                                onChange={(moment) => this.handleDateChange('dateTo',moment)}
                                inputProps={{'placeholder': 'Date to',name: 'dateTo'}}
                                dateFormat="YYYY-MM-DD"
                                value={ this.state.filter.dateTo ? moment(this.state.filter.dateTo, 'YYYY-MM-DD').toDate() : ''}
                                timeFormat={false} />
                            <img src="/assets-portal/images/icons/calendar.svg" alt=""/>
                        </div>
                        {showClear && <div className="clearCookie mr-2">
                            <Button className="clear-filters" onClick={() => {this.clearSearchnFilter()}} color="primary">Clear Filters</Button>
                        </div>}
                        {userPermission.includes('export-records') && <div className="Type">
                            <form
                                action="/api/portal/transaction/download"
                                id="exportFormComplete"
                                method="post"
                            >
                                <div className="selectPendingClass select-container">
                                    <select
                                        onChange={this.handleChange}
                                        name="export"
                                        className="selectPendingInput form-control"
                                    >
                                        <option value="">Export</option>
                                        <option value="csv">CSV</option>
                                        <option value="excel">Excel</option>
                                    </select>
                                </div>
                                <input type="hidden" name="from" value="completed" />
                            </form>
                        </div>}
                    </div>
                    <div className="table-part">
                        <Table className="custom-table">
                            <thead>
                            <tr>
                                <th className="seen"></th>
                                <th className="th-date">Date</th>
                                <th>Transaction ID</th>
                                <th className="th-reference">Reference</th>
                                <th className="th-status">Status</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Contact Details</th>
                                {/* <th>COE</th>*/}
                                <th>User</th>
                                { enableCentrix &&<th>Data</th> }
                                <th className="">Face</th>
                                <th className="">Live</th>
                                <th className="">Doc</th>
                                <th className="">View</th>
                                {userPermission.includes('delete-records') && <th className="th-delete">Delete</th>}
                            </tr>
                            </thead>
                            <tbody>{transactionRows}</tbody>
                        </Table>
                        <div className="float-right">
                            <Pagination onPageChange={this.handlePageChange} data={paginate} />
                        </div>
                    </div>
                </div>
                {this.renderDeletePrompt()}
            </Container>
        );
    }
}

export default withRouter(connect(mapStateToProps, null)(CustomerRepository));
function mapStateToProps({user}) {
    return { session: user.session, user: user.info };
}
