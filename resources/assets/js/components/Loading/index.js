import React from "react";
// import { css } from "@emotion/core";
// First way to import
import { DotLoader } from "react-spinners";
// Another way to import

export default class LoadingComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }
  render() {
    const { MIX_PRIMARY } = process.env
    return (
      <div className="sweet-loading">
        <DotLoader sizeUnit={"px"} size={50} color={MIX_PRIMARY} loading={true} />
      </div>
    );
  }
}
