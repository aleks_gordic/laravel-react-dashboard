import React, {Component} from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Input, Button} from 'reactstrap';
import {Table, Container} from 'reactstrap';
import {ASSETS_URL, API_URL} from '@js/config';
import Pagination from '@components/Pagination'
import BatchCheck from '@services/BatchCheck'
import Invitation from '@services/Invitation'
import  './BatchSmsProcess.style.scss';

/**
 * Batch SMS Process!
 */
class BatchSmsProcess extends Component {

    constructor(props) {
        super(props);
        this.state = this.getInitialState();
    }

    getInitialState = () => {
        return {
            batchName: this.props.batchName,
            filter: {
                batchId: this.props.batchId,
                isSms: 1
            },
            batchSmss: []
        }
    }

    cancel() {
        this.props.cancelCallBack();
    }

    send() {
        let data = this.state.batchSmss.data || []
        var applicants = data.map(function(batch) {
            var applicant = {
                'country_code' : '+' + batch.region,
                'medium' : 'sms',
                'mobile' : batch.contact,
                'full_name' : '',
                'flow_type' : 'batch_check', 
                'batch_id' : batch.id
            };
            return applicant;
        });

        const { session } = this.props
        // let param = this.formatInputs( inputs )

        console.log(applicants);
        let headers = {
            'Authorization': 'Bearer ' + session
        }

        let params = {'module': "remote-verification", 
                      'workflow': "batch-check",
                      'applicants': applicants,
                      'number': applicants.length,
                      'reference' : '',}

        Invitation.send(params, headers)
            .then((response) => {

                const { status, data, message } = response;

                if (status === 'success') {
                    console.log(data);
                    this.props.sendBatchCallBack();

                } else {
                    error('Error in sending invitation')
                }

                console.log('response', response);
            })
            .catch(err => {
                error('Error in sending invitation')
                console.warn(err);
            });

        
    }

    fetchBatchDetails = () => {
        let headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.props.session
        }

        let params = {
            page: this.state.page || 1,
            filter: this.state.filter,
        }

        BatchCheck.getDetails( params, headers )
            .then((res) => {

                console.log(res);

                this.setState({
                    batchSmss: res
                });

            })
            .catch(err => {
                console.warn(err);
            });
    }

    handlePageChange = async (currentPage) => {

        await this.setState({
            page: currentPage || 1
        })

        this.fetchBatchDetails()
        console.log('currentPage', currentPage)
    }

    render(){
        let data = this.state.batchSmss.data || []
        let batchSmsRows = data.map((batchSms) => {
            return (
                <tr key={batchSms.id}>
                    <th scope="row">{batchSms.region}</th>
                    <td>{batchSms.contact}</td>
                    <td>{batchSms.reference}</td>
                    
                    <td>
                        <center>
                            <a href="#" onClick={(e) => this.delete(e,trans.id)}> <img className="action-imgs"
                                                                                       style={{ width: '19px' }}
                                                                           src={`${ASSETS_URL}/icons/trash.png`}/></a>
                        </center>
                    </td>
                </tr>
            )
        });

        let paginate = this.state.batchSmss || {}
        paginate = {current_page: paginate.current_page, last_page: paginate.last_page}

        return (
            <div className="batchsmsprocess-body">
                <h4 className="batch-title">{this.state.batchName} - {this.state.batchSmss.length} Records</h4>
                <h4 className="description-item">Review the records extracted from the batch template and confirm below</h4>
                <div className="table-part">
                    <Table className="custom-table">
                        <thead>
                        <tr>
                            <th className="th-region">Region</th>
                            <th className="th-contact">Contact</th>
                            <th className="th-reference">Reference</th>
                            <th className="th-delete">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        {batchSmsRows}
                        </tbody>
                    </Table>
                    <div className="float-right">
                        <Pagination onPageChange={this.handlePageChange} data={paginate}/>
                    </div>
                </div>

                <div className="button-groups">
                    <div className="first">
                        <Button onClick={this.cancel.bind(this)} className="cancel-button">Cancel</Button>
                    </div>
                    <div onClick={this.send.bind(this)} className="last">
                        <Button className="next-button custom-send">Send</Button>
                    </div>
                </div>
            </div>
                
            );
        }
}

export default withRouter(connect(mapStateToProps, null)(BatchSmsProcess));

function mapStateToProps({user}) {
    return {session: user.session};
}