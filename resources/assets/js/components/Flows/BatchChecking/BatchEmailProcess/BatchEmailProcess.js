import React, {Component} from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Input, Button} from 'reactstrap';
import {Table, Container} from 'reactstrap';
import {ASSETS_URL, API_URL} from '@js/config';
import Pagination from '@components/Pagination'
import BatchCheck from '@services/BatchCheck'
import  './BatchEmailProcess.style.scss';

/**
 * Batch Email Process!
 */
class BatchEmailProcess extends Component {

    constructor(props) {
        super(props);
        this.state = this.getInitialState();
    }

    getInitialState = () => {
        return {
            batchName: this.props.batchName,
            batchEmails: [],
            filter: {
                batchId: this.props.batchId,
                isSms: 2
            },
        }
    }

    cancel() {
        this.props.cancelCallBack();
    }

    send() {
        this.props.sendBatchCallBack();
    }

    fetchBatchDetails = () => {
        let headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.props.session
        }

        let params = {
            page: this.state.page || 1,
            filter: this.state.filter,
        }

        BatchCheck.getDetails( params, headers )
            .then((res) => {

                console.log(res);

                this.setState({
                    batchEmails: res
                });

            })
            .catch(err => {
                console.warn(err);
            });
    }

    handlePageChange = async (currentPage) => {

        await this.setState({
            page: currentPage || 1
        })

        this.fetchBatchDetails()
        console.log('currentPage', currentPage)
    }

    render(){
        let data = this.state.batchEmails.data || []
        let batchEmailRows = data.map((batchEmail) => {
            console.log(batchEmail);
            return (
                <tr key={batchEmail.id}>
                    <th scope="row">{batchEmail.contact}</th>
                    <td>{batchEmail.reference}</td>
                    
                    <td>
                        <center>
                            <a href="#" onClick={(e) => this.delete(e,trans.id)}> <img className="action-imgs"
                                                                                       style={{ width: '19px' }}
                                                                           src={`${ASSETS_URL}/icons/trash.png`}/></a>
                        </center>
                    </td>
                </tr>
            )
        });

        let paginate = this.state.batchEmails || {}
        paginate = {current_page: paginate.current_page, last_page: paginate.last_page}

        return (
                <div className="batchemailprocess-body">
                    <h4>{this.state.batchName} -  {this.state.batchEmails.length} Records</h4>
                    <h4 className="description-item">Review the records extracted from the batch template and confirm below</h4>
                    <div className="table-part">
                        <Table className="custom-table">
                            <thead>
                            <tr>
                                <th className="th-contact">Contact</th>
                                <th className="th-reference">Reference</th>
                                <th className="th-delete">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            {batchEmailRows}
                            </tbody>
                        </Table>
                        <div className="float-right">
                            <Pagination onPageChange={this.handlePageChange} data={paginate}/>
                        </div>
                    </div>

                    <div className="button-groups">
                        <div className="first">
                            <Button onClick={this.cancel.bind(this)} className="cancel-button">Cancel</Button>
                        </div>
                        <div onClick={this.send.bind(this)} className="last">
                            <Button className="next-button custom-send">Send</Button>
                        </div>
                    </div>
                </div>
            );
        }
}

export default withRouter(connect(mapStateToProps, null)(BatchEmailProcess));

function mapStateToProps({user}) {
    return {session: user.session};
}