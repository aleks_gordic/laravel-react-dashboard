import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { withRouter } from 'react-router-dom'
import './NoRecordFoundGeneric.style.scss'

class NoRecordFoundGeneric extends Component {

	/** The component's constructor */
	constructor(props) {
		super(props);
		this.state = {};
	}


    handleClick = () => {
		if(!this.props.onClickCta){
			this.props.history.goBack()
		}
        this.props.onClickCta()
    }


    /**
	 * Render the component's markup
	 * @return {ReactElement}
	 */
	render() {

		const {cta} = this.props;

		return (
            <section className="no-record-404">
                <div className="noo-record-404-inner">
                    <h5>{this.props.message}</h5>
					{cta.show && <Button onClick={this.handleClick} color="primary">{cta.text}</Button>}
                </div>
            </section>
			);
	}
}

NoRecordFoundGeneric.propTypes = {
    message: PropTypes.string.isRequired,
    cta: PropTypes.object.isRequired,
    onClickCta:PropTypes.func

};

NoRecordFoundGeneric.defaultProps = {
    message: 'No records found.',
	cta:{
    	show:true,
		text:'Go Back'
	}
};

export default withRouter(NoRecordFoundGeneric);
