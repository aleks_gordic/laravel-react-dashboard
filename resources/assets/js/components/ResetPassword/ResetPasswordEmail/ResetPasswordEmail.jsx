import React, {Component} from 'react';
import axios from "axios";
import {Button, Form, FormGroup} from "reactstrap";
import { success, error } from "@js/lib/Tostify";
import './ResetPasswordEmail.style.scss';

class ResetPasswordEmail extends Component {

    /** The component's constructor */
    constructor(props) {
        super(props);

        this.state = {
            emailAddress: "",
            token: document.head.querySelector('meta[name="csrf-token"]'),
            loading: false
        };

    }

    handleChange = e => {
        let val = e.target.value;
        this.setState({
            emailAddress: val
        });
    };

    handleClick = e => {
        const { token } = this.state;
        this.setState({ loading: true });
        e.preventDefault();
        let data = {
            email: this.state.emailAddress,
            token: token.content
        };
        const instance = axios.create({
            headers: {
                "Content-Type": "application/json",
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-TOKEN": token.content
            }
        });
        instance
            .post("/portal/password/email", data)
            .then(data => {
                this.setState({ loading: false });
                success("Successfully sent email!");
            })
            .catch(err => error("Error occurred"));
    };

    render() {

        return (
                <div className="reset d-flex justify-content-center">
                    <Form className="reset-form bg-white">
                        <h4 className="form-title text-center border-primary pb-2">
                            Reset password with Email
                        </h4>
                        <FormGroup>
                            <input
                                type="text"
                                className="form-control"
                                value={this.state.emailAddress}
                                placeholder="Email address"
                                onChange={this.handleChange}
                                autoComplete="off"
                            />
                        </FormGroup>
                        <Button
                            type="submit"
                            //id="LoginButton"
                            onClick={this.handleClick}
                            className="p-2 login-cta"
                            color="primary"
                            block={true}
                        >
                            Reset Password
                        </Button>
                    </Form>
                </div>
        );
    }

};
export default ResetPasswordEmail;
