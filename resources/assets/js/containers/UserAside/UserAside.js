import React from 'react';
import { connect } from 'react-redux';
import {
    Container,
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink } from 'reactstrap';
import { Link } from "react-router-dom";
import UserAction from '@js/store/actions/user'

class UserAside extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users:[
                {
                    id:1,
                    name:'',
                }
            ]
        };
    }

    componentWillMount(){

    }

    selctedUser = () => {

    }


    render() {


        return (
            <aside>

            </aside>
        );
    }
}

/*
UserAside.propTypes = {
    total: PropTypes.number.isRequired,
    step: PropTypes.number.isRequired,
    onGoBack: PropTypes.func.isRequired,
    style: PropTypes.object,
};

UserAside.defaultProps = {
	style: {},
};
*/

export default connect(mapStateToProps, mapDispatchToProps)(UserAside);


function mapStateToProps({user}) {
    return {user};
}

function mapDispatchToProps(dispatch) {
    return {
        setCurrentUser: (data) => dispatch(UserAction.setCurrentUser(data)),
    };
}
