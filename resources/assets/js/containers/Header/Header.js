import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { setC } from "@js/lib/Cookie";
import {
    Container,
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import { ASSETS_URL } from '@js/config';
import UserAction from '@js/store/actions/user';
import SubHeader from '@components/SubHeader';

import './Header.style.scss';

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            showSubHeader: true
        };
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    };

    logout = () => {
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        axios.post('/portal/logout',{headers}).then(res => {
            setC('twoStepLogin', false, -1)
            this.props.setCurrentUser(null);
            localStorage.setItem('logout-event', 'logout' + Math.random());
            //this.props.history.push('/portal/login')
            window.location.href = '/portal/login'
        });
    }

    componentWillMount() {

        const { location, match = {} } = this.props,
            { params : { module, workflow} } = match;

        const pathname = location.pathname;

        if (pathname === '/portal/login' ||
            pathname === '/portal/password/reset' ||
            pathname === '/portal/twoStep' ||
            (!module && !workflow && !pathname.includes('transaction'))
        ) {
            this.setState({
                showSubHeader: false
            });
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const { location : { pathname }, match = {} } = nextProps,
            { params : { module, workflow} } = match;

        if (
            pathname === '/portal/login' ||
            pathname === '/portal/password/reset' ||
            pathname === '/portal/twoStep' ||
            (!module && !workflow && !pathname.includes('transaction'))
        ) {
            this.setState({
                showSubHeader: false
            });
        }else{
            this.setState({
                showSubHeader: true
            });
        }
    }

    render() {
        const { user: { info } } = this.props
        let isLoggedIn = !_.isEmpty(info);

        const { MIX_APP_LOGO, MIX_HEADER_GRAY, MIX_PRIMARY } = process.env
        const headerColor = MIX_HEADER_GRAY === 'true' ? '#b6c0d2' : '#fff'
        const headerBG = MIX_HEADER_GRAY === 'true' ? '#fff' : MIX_PRIMARY
        let navStyle = !this.state.showSubHeader ? 'primary-border-bottom' : '';

        return (
            <header className={navStyle}>
                <Navbar expand="md" style={{backgroundColor:headerBG}}>
                    <Container>
                        <NavbarBrand href="/portal">
                            <img src={MIX_APP_LOGO} />
                        </NavbarBrand>
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                                {isLoggedIn && (
                                    <NavItem>
                                        <span className="username" style={{color: headerColor}}>
                                            {info.first_name + ' '+ info.last_name}
                                        </span>
                                    </NavItem>
                                )}
                                {isLoggedIn && (
                                    <NavItem className="ml-4 d-flex align-items-center">
                                        <NavLink
                                            className=""
                                            href="#"
                                            onClick={this.logout}
                                        >
                                            <svg width="24px" height="27px" viewBox="0 0 24 27" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                                                <g id="04-Portal-3.1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                                    <g id="04-01-06-aditional" transform="translate(-1388.000000, -21.000000)" fill={headerColor} fillRule="nonzero">
                                                        <g id="Group" transform="translate(1265.000000, 21.000000)">
                                                            <g id="power" transform="translate(123.000000, 0.000000)">
                                                                <path d="M0.0750950226,15.7737557 C-0.315855204,11.5656109 1.48142986,7.75384615 4.46242534,5.3321267 C5.62441629,4.38733032 7.36197285,5.19638009 7.36197285,6.68959276 L7.36197285,6.68959276 C7.36197285,7.23800905 7.10133937,7.74298643 6.67780995,8.08506787 C4.56559276,9.80633484 3.29500452,12.521267 3.60450679,15.518552 C4.00631674,19.4226244 7.13391855,22.5828054 11.0325611,23.0171946 C16.1203439,23.5873303 20.4425158,19.60181 20.4425158,14.6280543 C20.4425158,11.9891403 19.2207964,9.62714932 17.314914,8.07963801 C16.8913846,7.73755656 16.636181,7.23257919 16.636181,6.68959276 L16.636181,6.68959276 C16.636181,5.21266968 18.3465882,4.37647059 19.4977195,5.29954751 C22.2235113,7.49864253 23.9719276,10.8597285 23.9719276,14.6280543 C23.9719276,21.518552 18.1185339,27.0733032 11.1194389,26.5683258 C5.33120362,26.161086 0.612651584,21.5511312 0.0750950226,15.7737557 Z M11.9936471,0 C11.0162715,0 10.2289412,0.792760181 10.2289412,1.76470588 L10.2289412,9.89321267 C10.2289412,10.8705882 11.0217014,11.6579186 11.9936471,11.6579186 C12.9655928,11.6579186 13.7583529,10.8651584 13.7583529,9.89321267 L13.7583529,1.76470588 C13.7583529,0.792760181 12.9710226,0 11.9936471,0 Z" id="Shape"></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </NavLink>
                                    </NavItem>
                                )}
                            </Nav>
                        </Collapse>
                    </Container>
                </Navbar>
                {this.state.showSubHeader && <SubHeader />}
            </header>
        );
    }

}

/*
Header.propTypes = {
    total: PropTypes.number.isRequired,
    step: PropTypes.number.isRequired,
    onGoBack: PropTypes.func.isRequired,
    style: PropTypes.object,
};

Header.defaultProps = {
	style: {},
};
*/

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Header)
);

function mapStateToProps({ user }) {
    return { user };
}

function mapDispatchToProps(dispatch) {
    return {
        setCurrentUser: data => dispatch(UserAction.setCurrentUser(data))
    };
}
