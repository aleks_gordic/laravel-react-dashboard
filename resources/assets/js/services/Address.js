import axios from 'axios';

/**
 * Addressfinder APIs
 */
export default class Address {
    static API_URL = `https://api.addressfinder.io/api/${process.env.COUNTRY.toLowerCase()}/address`;
    static API_KEY = '7B63XV8AG49JCDURQHMT';

    /**
     * Make a request to verify address.
     * @param {String} address
     * @return {Promise}
     */
    static verify(address) {
        return new Promise((resolve, reject) => {
            axios
                .get(
                    `${this.API_URL}/cleanse?key=${
                        this.API_KEY
                    }&q=${address}&format=json`
                )
                .then(({ data }) => {
                    resolve(data.matched);
                }, 'json')
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });

        });
    }

    /**
     * Make a request to find addresses
     * @param {String} address
     * @return {Promise}
     */
    static find(address) {
        return new Promise((resolve, reject) => {
            axios
                .get(
                    `${this.API_URL}?key=${
                        this.API_KEY
                    }&q=${address}&format=json&strict=2&max=4`
                )
                .then(({ data }) => {
                    resolve(data.completions);
                }, 'json')
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }
}
