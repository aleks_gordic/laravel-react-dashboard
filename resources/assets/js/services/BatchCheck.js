import axios from 'axios';
import {API_URL} from '@js/config';
/**
 * Batch-Check service
 */

export default class BatchCheck {

    /**
     * Make a request to download template file to check batch.
     * @param {Object} isSms
     * @return {Promise}
     */

    static download(isSms) {
        let url = `${API_URL}/batchcheck/download/${isSms}`;

        return new Promise((resolve, reject) => {
            axios
            ({
                url: url,
                method: 'GET',
                responseType: 'blob', // important
              })
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

     /**
     * Make a request to upload batch file to check batch.
     * @param {Object} file
     * @return {Promise}
     */
    static upload(file, headers = {}) {
        let url = `${API_URL}/batchcheck/upload`;
        let formData = new FormData();
        formData.append('excel', file);
        console.log(file);
        return new Promise((resolve, reject) => {
            axios
            .post(url, formData, {headers})
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static processBatchFile( data, headers = {}) {
        let url = `${API_URL}/batchcheck/processfile`;
        return new Promise((resolve, reject) => {
            axios
                .post(url, data, {headers})
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static getDetails( params = {}, headers = {} ) {
        let url = `${API_URL}/batchcheck/get-details`;
        let input = {
            params,
            headers
        }

        return new Promise((resolve, reject) => {
            axios
                .get(url,input)
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static getDetailsWithStatus( params = {}, headers = {} ) {
        let url = `${API_URL}/batchcheck/get-details-status`;
        let input = {
            params,
            headers
        }
        
        return new Promise((resolve, reject) => {
            axios
                .get(url,input)
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static getBatches( params = {}, headers = {} ) {
        let url = `${API_URL}/batchcheck/get-batches`;
        let input = {
            params,
            headers
        }
        
        return new Promise((resolve, reject) => {
            axios
                .get(url,input)
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    

}