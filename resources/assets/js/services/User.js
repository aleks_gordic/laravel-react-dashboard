import axios from 'axios';
import {API_URL} from '@js/config';
/**
 * SMS Service
 */
export default class User {
    /**
     * Make a read users.
     * @param {Object} params
     * @param {Object} headers
     * @return {Promise}
     */
    static read(params={}, headers = {}) {
        let url = `${API_URL}/user/read`;
        let input = {
            params,
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url,input)
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    /**
     * Update seen status of a transaction by the logged in user
     * @param {Object} id
     * @param {Object} headers
     * @return {Promise}
     */
    static updateSeen( id, headers = {}) {
        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/user/seen/${id}`, {}, {headers})
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    /**
     * Fetch custom fields.
     * @param {Object} params
     * @param {Object} headers
     * @return {Promise}
     */
    static getFields(params={}, headers = {}) {
        let url = `${API_URL}/user/fields`;
        let input = {
            params,
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url,input)
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }


    /**
     * Download sample Batch
     * @param {Object} params
     * @param {Object} headers
     * @return {Promise}
     */
    static download() {
        let url = `${API_URL}/user/downloadSample`;

        return new Promise((resolve, reject) => {
            axios
            ({
                url: url,
                method: 'GET',
                responseType: 'blob', // important
            })
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    /**
     * Add Batch Users
     * @param {Object} params
     * @param {Object} headers
     * @return {Promise}
     */
    static addBatchUsers(params, headers = {}) {
        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/user/batch-process`, params, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }
}
