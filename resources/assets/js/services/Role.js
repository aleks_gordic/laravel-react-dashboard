import axios from 'axios';
import {API_URL} from '@js/config';
/**
 * SMS Service
 */
export default class Role {
    /**
     * Make a request to read Roles.
     * @param {Object} params
     * @param {Object} headers
     * @return {Promise}
     */
    static read(params={}, headers = {}) {
        let url = `${API_URL}/role/read`;
        let input = {
            params:params,
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url,input)
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    /**
     * Make a request to create Role.
     * @param {Object} params
     * @param {Object} headers
     * @return {Promise}
     */
    static create(params, headers = {}) {
        const data = new FormData();
        Object.keys(params).forEach(field => data.append(field, params[field]));

        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/role/create`, data, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    /**
     * Make a request to update Role.
     * @param {Object} params
     * @param {Object} headers
     * @return {Promise}
     */
    static update(params, headers = {}) {

        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/role/update`, params, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }


    /**
     * Make a request to Delete Role.
     * @param {Object} pk
     * @param {Object} headers
     * @return {Promise}
     */
    static delete(pk, headers = {}) {

        let url = `${API_URL}/role/delete`;

        return new Promise((resolve, reject) => {
            axios
                .post(url, {pk}, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }


    /**
     * Make a request to Search Roles.
     * @param {Object} query
     * @param {Object} headers
     * @return {Promise}
     */
    static search(query, headers = {}) {

        let url = `${API_URL}/role/search`;

        let params = {
            q: query
        }

        return new Promise((resolve, reject) => {
            axios
                .get(url, {params, headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    /**
     * Make a request to read Roles.
     * @param {int} id
     * @param {Object} params
     * @param {Object} headers
     * @return {Promise}
     */
    static permissions(id, params={}, headers = {}) {
        let url = `${API_URL}/role/permissions/${id}`;
        let input = {
            params:params,
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url,input)
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }
}
