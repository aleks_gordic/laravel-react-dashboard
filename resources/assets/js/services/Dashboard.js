import axios from 'axios';
import {API_URL} from '@js/config';

export default class Report {
  static getSessionReport(headers = {}) {

    return new Promise((resolve, reject) => {
      axios
        .get(`${API_URL}/dashboard/sessionReport`, {headers})
        .then(({data}) => resolve(data))
        .catch(err => {
          if (err.response.status === 401) {
            location.reload()
          }
          reject(err)
        });
    });
  }
  
  static getUsageReport(headers = {}, params={}) {
    const data = new FormData();
    Object.keys(params).forEach(field => data.append(field, params[field]));

    return new Promise((resolve, reject) => {
      axios
        .post(`${API_URL}/dashboard/usageReport`, data, {headers})
        .then(({data}) => resolve(data))
        .catch(err => {
          if (err.response.status === 401) {
            location.reload()
          }
          reject(err)
        });
    });
  }
  static getAnalyticsReport(headers = {}, params={}) {
    const data = new FormData();
    Object.keys(params).forEach(field => data.append(field, params[field]));

    return new Promise((resolve, reject) => {
      axios
        .post(`${API_URL}/dashboard/analyticsReport`, data, {headers})
        .then(({data}) => resolve(data))
        .catch(err => {
          if (err.response.status === 401) {
            location.reload()
          }
          reject(err)
        });
    });
  }
  static getIncompleteReport(headers = {}, params={}) {
    const data = new FormData();
    Object.keys(params).forEach(field => data.append(field, params[field]));

    return new Promise((resolve, reject) => {
      axios
        .post(`${API_URL}/dashboard/incompleteReport`, data, {headers})
        .then(({data}) => resolve(data))
        .catch(err => {
          if (err.response.status === 401) {
            location.reload()
          }
          reject(err)
        });
    });
  }
}
