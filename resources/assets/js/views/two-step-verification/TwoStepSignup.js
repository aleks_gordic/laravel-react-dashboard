import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Instruction from "./sign-up/Instruction";
import GetStarted from "./sign-up/GetStarted";
import Enabled from "./sign-up/Enabled";
import UserAction from "@js/store/actions/user";

import VerificationCode from "./sign-up/Verification";
import API from "@services/APIs";
import CodeManually from "./sign-up/CodeManually";
class TwoStepSignup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 0,
      qr_code: "",
      qr_code_key: "",
      email: ""
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.getQrCode = this.getQrCode.bind(this);
    this.handleManualClick = this.handleManualClick.bind(this);
  }
  componentDidMount() {
    if (this.props.twoStepVerified) this.props.history.push("/portal");
  }
  handleClick = () => {
    const { step } = this.state;
    if (step === 0) this.getQrCode();
    if (step === 2){
      //Login user
      console.log('user 2FA, verified')
      this.props.setTwoStepStatus(true);
      this.props.history.push("/portal")
      return
    }
    if (step === 4) this.setState({ step: 2 });
    else {
      this.setState({
        step: step + 1
      });
    }
  };

  handleManualClick = () => {
    this.setState({ step: 4 });
  };

  handleBack = () => {
    const { step } = this.state;
    if (step <= 0) return;
    else if (step === 4) this.setState({ step: 0 });
    else
      this.setState({
        step: step - 1
      });
  };

  getQrCode = () => {
    let headers = {
      Authorization: "Bearer " + this.props.session
    };
    API.get(`${"/user/2fa-auth"}`, {}, headers)
      .then(({ qr_code, qr_code_key, email }) => {
        this.setState({ qr_code, qr_code_key, email });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    const { step, qr_code, qr_code_key } = this.state;
    switch (step) {
      case 0:
        return <Instruction handleClick={this.handleClick} />;
      case 1:
        return (
          <GetStarted
            qrCode={qr_code}
            handleClick={this.handleClick}
            handleManualClick={this.handleManualClick}
          />
        );
      case 2:
        return (
          <VerificationCode
            handleClick={this.handleClick}
            session={this.props.session}
            handleBack={this.handleBack}
          />
        );
      case 3:
        return <Enabled handleClick={this.handleClick} />;
      case 4:
        return (
          <CodeManually
            qr_code_key={qr_code_key}
            handleBack={this.handleBack}
            handleClick={this.handleClick}
            email={this.state.email}
          />
        );
      default:
        return <Instruction handleClick={this.handleClick} />;
    }
  }
}

function mapStateToProps({ user }) {
  return {
    session: user.session,
    twoStepVerified: user.twoStepVerified
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setTwoStepStatus: data => dispatch(UserAction.setTwoStepStatus(data))
  };
}
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TwoStepSignup)
);
