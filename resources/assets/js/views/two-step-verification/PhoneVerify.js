import React, { Component } from "react"
import { Button, Form, FormGroup, Input } from "reactstrap"
import { connect } from "react-redux"
import API from "@services/APIs"
import { withRouter } from "react-router-dom"
import Verification from "./phone-verify/Verification"
import UserAction from "@js/store/actions/user"
import "react-phone-number-input/style.css"
import Select, { components } from "react-select"
import countries from "@js/lib/countries.json"
import LoadingScreen from "@components/Loading"

const SingleValue = ({ children, ...props }) => {
    return (
        <components.SingleValue {...props}>
            {`${props.data.dial_code}    ${props.data.name}`}
        </components.SingleValue>
    )
}
const Menu = props => {
    return (
        <components.Menu {...props}>
            <div className="select-options">{props.children}</div>
        </components.Menu>
    )
}
const Inputs = props => {
    return <components.Input {...props} className="select-input" />
}
const Option = props => {
    return (
        <components.Option {...props} className="option">
            {props.children}
        </components.Option>
    )
}

const colourStyles = {
    control: styles => ({
        ...styles,
        backgroundColor: "white",
        border: "none",
        borderBottom: "1px solid hsl(0,0%,80%)",
        marginBottom: "10px"
    })
}

class PhoneVerify extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dropdownOpen: false,
            phoneNumber: "",
            countrySelect: null,
            status: 0, // 0: Number input , 1: verification panel
            loading: true
        }
        this.toggle = this.toggle.bind(this)
        this.handlePhoneNumber = this.handlePhoneNumber.bind(this)
        this.handleClick = this.handleClick.bind(this)
    }

    componentDidMount() {

        const { dial_code, mobile } = this.props
        this.setState({
            phoneNumber: mobile,
            loading: false
        })

        if(this.state.countrySelect) return

        if(dial_code){

            for (let i = 0; i < countries.length; i++) {
                if (dial_code === countries[i].dial_code) {
                    this.setState({
                        countrySelect: countries[i],
                    })
                }
            }

        }else{
            if(this.state)
            API.country().then(({ dial_code }) =>{

                for (let i = 0; i < countries.length; i++) {
                    if (dial_code === countries[i].dial_code) {
                        this.setState({
                            countrySelect: countries[i],
                        })
                    }
                }

            });
        }

    }

    handleBack = () => {
        this.setState({ status: 0 })
    }
    handlePhoneNumber = e => {
        this.setState({ phoneNumber: e.target.value })
    }

    handleClick = e => {
        const { status, phoneNumber, countrySelect } = this.state
        console.log(countrySelect.dial_code + phoneNumber)
        if (status === 0) {
            let headers = {
                Authorization: "Bearer " + this.props.session
            }
            API.post(
                "/sms/send/simple",
                { number: countrySelect.dial_code + phoneNumber },
                headers
            ).then(res => {
                if (res["status"] == "success") this.setState({ status: 1 })
            })
        }
    }

    handleChangeSelectBox = value => {
        this.setState({ countrySelect: value })
    }

    toggle() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }))
    }

    render() {
        const { status, phoneNumber, countrySelect, loading } = this.state

        if (status === 1) {
            return (
                <Verification
                    session={this.props.session}
                    setCurrentUser={this.props.setCurrentUser}
                    phoneNumber={countrySelect.dial_code + phoneNumber}
                    handleBack={this.handleBack}
                />
            )
        }
        return (
            <div className="phone--verify d-flex justify-content-center">
                {loading && <LoadingScreen />}
                <Form onSubmit={this.handleClick} className="bg-white mt-5 p-5">
                    <h4 className="form-title text-center border-primary pb-2">
                        Mobile Number Verification
                    </h4>
                    <p className="description">
                        We will need to verify mobile number. Enter your number
                        below and click send.
                    </p>
                    <label className="mb-0 mr-2 phone--verify__code-label mt-4">
                        Country/region
                    </label>
                    <Select
                        options={countries}
                        onChange={this.handleChangeSelectBox}
                        components={{
                            SingleValue,
                            Menu,
                            Input: Inputs,
                            Option
                        }}
                        value={this.state.countrySelect}
                        // value={this.state["country_code_select" + n]}
                        theme={theme => ({
                            ...theme,
                            borderRadius: "4px",
                            colors: {
                                ...theme.colors,
                                primary: "#d2f0f8"
                            }
                        })}
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option.dial_code}
                        className="phone--verify__code-select"
                        styles={colourStyles}
                    />
                    <label className="mt-4">Phone Number</label>
                    <Input
                        onChange={this.handlePhoneNumber}
                        value={this.state.phoneNumber}
                        placeholder="#### ### ###"
                        type="text"
                        className="mobile-input"
                    />
                    <div className="d-flex justify-content-center mt-5">
                        <Button
                            //id="LoginButton"
                            onClick={this.handleClick}
                            className="phone--verify__submit"
                            color="primary"
                        >
                            Send
                        </Button>
                    </div>
                </Form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setCurrentUser: data => dispatch(UserAction.setCurrentUser(data)),
        setCurrentSession: data => dispatch(UserAction.setCurrentSession(data))
    }
}

const mapStateToProps = ({ user }) => {
    return { user: user, session: user.session }
}
export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(PhoneVerify)
)
