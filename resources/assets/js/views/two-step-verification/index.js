import React, { Component } from "react"
import { connect } from "react-redux"
import {Redirect, withRouter} from "react-router-dom";
import { getC, setC} from "@js/lib/Cookie";
import App from "@components/App"
import UserAction from "@js/store/actions/user"

import TwoStepLogin from "./TwoStepLogin"
import TwoStepSignUp from "./TwoStepSignup"
import PasswordSet from "./PasswordSet"
import PhoneVerify from "./PhoneVerify"

import "./two-step-verification.style.scss"
class twoStepVerification extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let { info = {} } = this.props.user

        if(!info){
            info = {}
        }
        const { role, password_status, mobile_verified_at, google_secret } = info
        const { MIX_ENABLE_2FA =  true } = process.env
        const enable2FA = (MIX_ENABLE_2FA === 'true')
        const twoStepLogin = getC('twoStepLogin') === 'true'

        if(parseInt(password_status) && role === 'admin'){
            // do nothing
        }else if(!parseInt(password_status) || (enable2FA && (!google_secret || !mobile_verified_at || (google_secret && !twoStepLogin)))){
            // do nothing
        }else{
            this.props.history.push("/portal");
        }

    }

    render() {
        let { info = {} } = this.props.user

        if(!info){
            info = {}
        }

        let component
        if (!parseInt(info.password_status)) {
            if (this.props.user.subTitleHeader !== "First Time Login Set Up")
                this.props.setSubTitleHeader("First Time Login Set Up")
            component = (
                <PasswordSet
                    session={this.props.user}
                    setCurrentUser={this.props.setCurrentUser}
                    setSubTitleHeader={this.props.setSubTitleHeader}
                />
            )
        } else if (!info.mobile_verified_at) {
            if (this.props.user.subTitleHeader !== "First Time Login Set Up")
                this.props.setSubTitleHeader("First Time Login Set Up")
            component = <PhoneVerify code={info.dial_code} mobile={info.mobile}/>
        } else if (info.google_secret) {
            component = <TwoStepLogin />
        } else {
            if (this.props.user.subTitleHeader !== "First Time Login Set Up")
                this.props.setSubTitleHeader("First Time Login Set Up")
            component = <TwoStepSignUp />
        }

        return (
            <App>
                <div className="two-step d-flex justify-content-center">
                    {component}
                </div>
            </App>
        )
    }
}

const mapStateToProps = ({ user }) => {
    return { user }
}

const mapDispatchToProps = dispatch => {
    return {
        setCurrentUser: data => dispatch(UserAction.setCurrentUser(data)),
        setTwoStepStatus: data => dispatch(UserAction.setTwoStepStatus(data)),
        setSubTitleHeader: data => dispatch(UserAction.setSubTitleHeader(data))
    }
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(twoStepVerification))
