import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import UserAction from "@js/store/actions/user";
import { getC, setC} from "@js/lib/Cookie";


import GoogleVerification from "./sign-in/GoogleVerification";
import SmsVerification from "./sign-in/SmsVerification";
import ChooseAuthenticator from "./sign-in/ChooseAuthenticator";

class twoStepLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: "code"
    }
  }

  handleClick = () => {
    setC('twoStepLogin', true, 1)
    this.props.setSubTitleHeader(false);
    this.props.setTwoStepStatus(true);
    this.props.history.push("/portal");
  }

  changeMethod = () => {
    this.setState({ step: "choose" });
  }

  render() {
    const { step } = this.state;
    switch (step) {
      case "code":
        if (this.props.user.subTitleHeader !== "Log In - 2FA")
          this.props.setSubTitleHeader("Log In - 2FA");

        return (
          <GoogleVerification
            handleClick={this.handleClick}
            session={this.props.session}
            changeMethod={this.changeMethod}
          />
        );
      case "sms":
        return (
          <SmsVerification
            handleClick={this.handleClick}
            session={this.props.session}
            changeMethod={this.changeMethod}
          />
        );
      case "choose":
        return (
          <ChooseAuthenticator
            chooseApp={() => this.setState({ step: "code" })}
            chooseSms={() => this.setState({ step: "sms" })}
          />
        );
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setTwoStepStatus: data => dispatch(UserAction.setTwoStepStatus(data)),
    setSubTitleHeader: data => dispatch(UserAction.setSubTitleHeader(data))
  };
}
function mapStateToProps({ user }) {
  return { user: user, session: user.session };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(twoStepLogin)
);
