import { Button } from "reactstrap";
import React from "react";
import QrCode from "qrcode-react";

const GetStarted = ({ qrCode, handleClick, handleManualClick }) => (
  <div className="two--step__content bg-white mt-5 p-5">
    <h1 className="two--step__header">Get Started</h1>
    <hr />
    <br />
    <p>
      1. Install the{" "}
      <label className="text__enter-manual-key">Google Authenticator App</label>{" "}
      on your phone.
    </p>
    <p>2. Open the app, tap begin setup.</p>
    <p>3. Choose to scan barcode, then scan code below</p>
    <br />
    <br />
    <div className="d-flex justify-content-center align-items-center flex-column mb-3">
      <QrCode value={qrCode} />
      <a
        className="mt-3 text-decoration-none text__enter-manual-key"
        onClick={handleManualClick}
      >
        or enter your key manually
      </a>
    </div>
    <Button
      onClick={handleClick}
      color="primary"
      className="two--step--content__submit float-right"
    >
      Next
    </Button>
  </div>
);

export default GetStarted;
