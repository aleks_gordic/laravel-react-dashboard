import React from "react";
import { Button } from "reactstrap";

const Enabled = ({ handleClick }) => (
  <div className="two--step__content bg-white mt-5 p-5">
    <h1 className="two--step__header">2-Step authentication enabled!</h1>
    <hr />
    <br />
    <p>
      From now on when you sign into your IDKit enterprise account, you will
      need to enter both your password and an authentication code from your
      authenticatior app.
    </p>
    <Button onClick={handleClick} color="primary" block={true}>
      Next
    </Button>
  </div>
);
export default Enabled;
