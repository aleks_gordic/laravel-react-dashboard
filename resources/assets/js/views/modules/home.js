import React, { Component } from "react"
import { connect } from "react-redux"
import App from "@components/App"
import FlowCards from "@components/FlowCards"

import BusinessUnit from "@components/Home/BusinessUnit"

import CustomerReview from "@components/Home/CustomerReview"
import Management from "@components/Home/Management"
import NoRecordFoundGeneric from "@components/NoRecordFoundGeneric"

import Guided from "@components/Flows/Guided"
import UnGuided from "@components/Flows/UnGuided"
import CustomerRepository from '@components/Home/CustomerReview/CustomerRepository';
import PendingTransaction from '@components/Home/CustomerReview/PendingTransaction';
import User from '@components/Home/Management/User';
import Role from '@components/Home/Management/Role';
import Dashboard from '@components/Home/Management/Dashboard';
import BatchChecking from '@components/Flows/BatchChecking';

import { withRouter } from 'react-router-dom';

class Home extends Component {
    /** The component's constructor */
    constructor(props) {
        super(props)
        this.state = {
            allowed: [],
            allowedModules: []
        }
    }

    componentDidMount() {

        let isLoggedIn = !_.isEmpty(this.props.user.info)

        if ( isLoggedIn ) {
            let permissions = this.formatPermission() || []

            let allowedModules = permissions.map((perm) => {
                return perm.modules
            })

            allowedModules = allowedModules.flat().filter(mod => {
                return mod.access
            })

            allowedModules = allowedModules.map(mod => {
                return mod.slug
            })

            let allowed = permissions.map(perm => {
                return perm.slug
            })

            this.setState({
                allowed,
                allowedModules
            })
        }
    }

    formatPermission = () =>{
        let permissions = this.props.user.info.permissions || [];
        let categories = [];

        permissions.forEach(perm => {
            let catExist = categories.findIndex(cat => {
                return cat.id === perm.category.id
            })

            if(catExist === -1){
                categories.push(perm.category)
            }

        });

        permissions.forEach(perm => {
            let catExist = categories.findIndex(cat => {
                return cat.id === perm.category.id
            })

            if(catExist > -1){
                if(!categories[catExist]['modules']){
                    categories[catExist]['modules'] = [];
                }

                let modExists = categories[catExist]['modules'].findIndex(mod => {
                    return mod.id === perm.id
                });

                if(modExists === -1){
                    categories[catExist]['modules'].push({
                        id: perm.id,
                        title: perm.title,
                        slug: perm.slug,
                        access: perm.access,
                    })
                }

            }

        });

        return categories;
    }


    /**
     * Render the component's markup
     * @return {ReactElement}
     */
    render() {
        const { allowed, allowedModules } = this.state

        const { match = {} } = this.props,
            { params = {} } = match
        let permissions = this.formatPermission() || [],
            workFlows = [],
            module = params.module,
            workflow = params.workflow,
            component

        if (module) {
            let category = permissions.filter(perm => {
                return perm.slug === module
            })[0]
            workFlows = category.modules
        }

        if ( module === "remote-verification" && allowed.includes("remote-verification") ) {
            if (workflow === "identity-flow-guided" && allowedModules.includes("identity-flow-guided")) {
                component = <Guided />
            } else if (workflow === "identity-flow-self-guided" && allowedModules.includes("identity-flow-self-guided")) {
                component = <UnGuided />
            }else if (workflow === "batch-checking" && allowedModules.includes("batch-checking")) {
                component = <BatchChecking />
            } else {
                if( !allowedModules.includes("identity-flow-guided") && !allowedModules.includes("identity-flow-self-guided") && !allowedModules.includes("batch-checking")){
                    component = <NoRecordFoundGeneric message="Permission Denied" />
                }else{
                    component = <FlowCards module={module} workFlows={workFlows} />
                }

            }
        } else if ( module === "customer-review" && allowed.includes("customer-review") ) {
            if (workflow === "customer-repository" && allowedModules.includes("customer-repository")) {
                component = <CustomerRepository />
            } else if (workflow === "pending-transactions" && allowedModules.includes("pending-transactions")) {
                component = <PendingTransaction />
            } else {
                if( !allowedModules.includes("pending-transactions") && !allowedModules.includes("customer-repository")){
                    component = <NoRecordFoundGeneric message="Permission Denied" />
                }else{
                    component = <CustomerReview />
                }
            }
        } else if (module === "management" && allowed.includes("management")) {
            if (workflow === "user" && allowedModules.includes("user")) {
                component = <User />
            } else if (workflow === "role" && allowedModules.includes("role")) {
                component = <Role />
            } else if (workflow === "dashboard" && allowedModules.includes("dashboard")) {
                component = <Dashboard />
            } else {
                    if( !allowedModules.includes("role") && !allowedModules.includes("user")){
                        component = <NoRecordFoundGeneric message="Permission Denied" />
                    }else{
                        component = <Management />
                    }

            }
        } else {
            if (!module) {
                component = ""
            } else {
                component = <NoRecordFoundGeneric message="404: Not Found" />
            }
        }

        return (
            <App>
                <div className="home d-flex align-items-center justify-content-center">
                    {allowedModules.length < 1 && <NoRecordFoundGeneric message="You don't have enough Privileges. Please contact Administrator." cta={{}}/>}
                    {!workflow && !module && <BusinessUnit />}
                    {component}
                </div>
            </App>
        )
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        null
    )(Home)
)

function mapStateToProps({ user }) {
    return { user }
}
