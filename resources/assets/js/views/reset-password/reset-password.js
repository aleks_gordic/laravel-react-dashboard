import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Form, FormGroup } from "reactstrap";
import { withRouter } from "react-router-dom";
import { success, error } from "@js/lib/Tostify";
import { ASSETS_URL, API_URL } from "@js/config";

import "./password-reset.scss";
class PasswordSet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      passwordAgain: "",
      moreCharacter: false,
      fullCharacter: false,
      oneNumber: false,
      passwordMismatch: false
    };

  }

  handlePassword = e => {
    const { value } = e.target;
    this.setState({ password: value });

    if (this.state.passwordAgain) {
      if (value !== this.state.passwordAgain)
        this.setState({ passwordMismatch: true });
      else this.setState({ passwordMismatch: false });
    }

    if (value.length > 7) this.setState({ moreCharacter: true });
    else this.setState({ moreCharacter: false });

    if (/[a-z]/.test(value) && /[A-Z]/.test(value)) {
      this.setState({ fullCharacter: true });
    } else this.setState({ fullCharacter: false });

    if (/[0-9]/.test(value)) this.setState({ oneNumber: true });
    else this.setState({ oneNumber: false });
  };

  handlePasswordAgain = e => {
    const { value } = e.target;
    this.setState({ passwordAgain: value });
    if (value != this.state.password) this.setState({ passwordMismatch: true });
    else this.setState({ passwordMismatch: false });
  };

  handleClick = e => {
    const {
      passwordMismatch,
      moreCharacter,
      oneNumber,
      fullCharacter,
      password,
      passwordAgain
    } = this.state;
    if (
      !passwordMismatch &&
      passwordAgain &&
      moreCharacter &&
      oneNumber &&
      fullCharacter
    ) {
      let data = {
        token: this.props.match.params.token,
        email: this.props.match.params.email,
        password: password,
        password_confirmation: password
      };
      axios
        .post("/portal/password/reset", data)
        .then(res => {
          this.props.history.push("/portal");
        })
        .catch(e => console.log(e));
    }
  };
  handleCancel = e => {
    this.props.history.push("/portal");
  };
  render() {
    const {
      moreCharacter,
      fullCharacter,
      oneNumber,
      passwordMismatch
    } = this.state;
    return (
      <div className="password-reset d-flex justify-content-center ml-auto mr-auto">
        <Form onSubmit={this.handleClick} className="bg-white mt-5 p-5 w-100">
          <h4 className="form-title text-center border-primary pb-2">
            Choose Your Password
          </h4>
          <div className="d-flex justify-content-center align-item-center">
            <div>
              <div className="mb-1 d-block">
                <img
                  src={`${ASSETS_URL}/icons/${
                    moreCharacter ? "Icon-Check" : "Icon-Uncheck"
                  }.png`}
                  alt=""
                  className="check mr-1 align-middle"
                  width="20"
                />
                <span className="d-inline-block align-middle">
                  8 or more characters
                </span>
              </div>
              <div className="mb-1 d-block">
                <img
                  src={`${ASSETS_URL}/icons/${
                    fullCharacter ? "Icon-Check" : "Icon-Uncheck"
                  }.png`}
                  alt=""
                  className="check mr-1"
                  width="20"
                />
                <span className="d-inline-block align-middle">
                  Upper & Lowercase letters
                </span>
              </div>
              <div className="mb-4 d-block">
                <img
                  src={`${ASSETS_URL}/icons/${
                    oneNumber ? "Icon-Check" : "Icon-Uncheck"
                  }.png`}
                  alt=""
                  className="check mr-1 align-middle"
                  width="20"
                />
                <span className="d-inline-block align-middle">
                  At least one number
                </span>
              </div>
            </div>
          </div>
          <FormGroup>
            <input
              type="password"
              className="form-control"
              name="username"
              id="username"
              placeholder="Enter password"
              onChange={this.handlePassword}
              autoComplete="off"
            />
          </FormGroup>
          <FormGroup>
            <input
              type="password"
              name="password"
              id="password"
              className={`form-control ${passwordMismatch ? "border-red" : ""}`}
              placeholder="Enter password again"
              onChange={this.handlePasswordAgain}
            />
          </FormGroup>
          <div className="d-flex justify-content-between pt-2">
            <Button
              onClick={this.handleCancel}
              className="login-cta mr-5"
              color="primary"
              outline
              block
            >
              Cancel
            </Button>

            <Button
              id="LoginButton"
              onClick={this.handleClick}
              className="login-cta ml-5 d-block w-100"
              color="primary"
            >
              Next
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

export default withRouter(PasswordSet);
