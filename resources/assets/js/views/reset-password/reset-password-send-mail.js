import React, { Component } from "react";
import { Button, ButtonGroup, Form, FormGroup } from "reactstrap";
import { connect } from "react-redux";
import UserAction from "@js/store/actions/user";
import App from "@components/App";
import { success, error } from "@js/lib/Tostify";
import axios from "axios";
import LoadingScreen from "@components/Loading";
import './password-resetsend-email.scss'

class Login extends Component {
  /** The component's constructor */
  constructor(props) {
    super(props);

    this.state = {
      emailAddress: "",
      token: document.head.querySelector('meta[name="csrf-token"]'),
      loading: false
    };
  }

  componentDidMount() {
    let isLoggedIn = !_.isEmpty(this.props.user.info);
    if (isLoggedIn && this.props.user.twoStepVerified)
      this.props.history.push("/portal");
    else if (isLoggedIn) {
      this.props.history.push("/portal/twoStep");
    }
  }

  handleChange = e => {
    let val = e.target.value;
    this.setState({
      emailAddress: val
    });
  };

  handleClick = e => {
    const { token } = this.state;
    this.setState({ loading: true });
    e.preventDefault();
    let data = {
      email: this.state.emailAddress,
      token: token.content
    };
    const instance = axios.create({
      headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest",
        "X-CSRF-TOKEN": token.content
      }
    });
    instance
      .post("/portal/password/email", data)
      .then(data => {
        this.setState({ loading: false, emailAddress: '' });
        success("Successfully sent email!");
      })
      .catch(({response: {data: {errors}}})=> {
        const msg = errors.email[0]
        error(msg)
        window.setTimeout(()=>{
          this.setState({ loading: false });
        }, 1300)
      });
  };

  /**
   * Render the component's markup
   * @return {ReactElement}
   */
  render() {
    const { token, loading } = this.state;

    return (
      <App>
        {loading && <LoadingScreen />}
        <div className="reset d-flex justify-content-center">
          <Form className="reset-form bg-white mt-5">
            <input type="hidden" name="token" value={token.content} />
            <h4 className="form-title text-center border-primary pb-2">
              Reset password
            </h4>
            <FormGroup>
              <input
                type="text"
                className="form-control"
                value={this.state.emailAddress}
                placeholder="Email address"
                onChange={this.handleChange}
                autoComplete="off"
              />
            </FormGroup>
            <div className="d-flex justify-content-between">
              <Button
                type="submit"
                outline={true}
                onClick={() => {this.props.history.push("/portal/login")}}
                className="p-2 login-cta"
                color="primary"
              >
                Cancel
              </Button>
              <Button
                  type="submit"
                  //id="LoginButton"
                  onClick={this.handleClick}
                  className="p-2 login-cta"
                  color="primary"
              >
                Reset
              </Button>
            </div>
          </Form>
        </div>
      </App>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

function mapDispatchToProps(dispatch) {
  return {
    setCurrentUser: data => dispatch(UserAction.setCurrentUser(data)),
    setCurrentSession: data => dispatch(UserAction.setCurrentSession(data))
  };
}

function mapStateToProps({ user }) {
  return { user };
}
