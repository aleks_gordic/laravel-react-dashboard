import React, { Component } from "react"
import { Link } from 'react-router-dom'
import { Button, Form, FormGroup } from "reactstrap"
import { connect } from "react-redux"
import UserAction from "@js/store/actions/user"
import App from "@components/App"
import ResetPassword from '@components/Modals/ResetPassword';
import { success, error } from "@js/lib/Tostify"
import { ASSETS_URL } from "@js/config"
import './login.scss'
class Login extends Component {
    /** The component's constructor */
    constructor(props) {
        super(props)

        this.state = {
            username: "",
            password: "",
            showModal: false
        }
    }

    componentDidMount() {
        let isLoggedIn = !_.isEmpty(this.props.user.info)
        if (isLoggedIn){
            this.props.history.push("/portal")
        }

    }

    handleChange = e => {
        let val = e.target.value
        let type = $(e.target).attr("name")
        this.setState({
            [type]: val
        })
    }

    handleClick = e => {
        e.preventDefault()

        let data = {
            email: this.state.username,
            password: this.state.password
        }
        axios
            .post("/portal/login", data)
            .then(({ data }) => {
                if (data.status === "success") {
                    this.props.setCurrentUser(data.data)
                    this.props.setCurrentSession(data.session)
                    this.props.history.push("/portal")
                    //this.props.history.goBack()
                } else {
                    let message = data.message
                        ? data.message
                        : "Username and Password, both are required."

                    error(message)
                    this.setState({
                        password: ""
                    })
                }
            })
            .catch(err => {
                console.log(err.response.data.errors.email)
                let msg = err.response.data.errors.email[0]
                error(msg)
            })
    }

    renderResetPasswordModal = () => {
        return <ResetPassword show={this.state.showModal} onClose={()=>this.setState({showModal: false})}/>
    }

    /**
     * Render the component's markup
     * @return {ReactElement}
     */
    render() {
        return (
            <App>
                <div className="login d-flex justify-content-center">
                    <Form onSubmit={this.handleClick} className="login-form bg-white mt-5">
                        <h4 className="form-title text-center border-primary pb-2">Please login to Identity Portal</h4>
                        <FormGroup>
                            <input
                                type="text"
                                className="form-control"
                                name="username"
                                id="username"
                                value={this.state.username}
                                placeholder="Email/Username"
                                onChange={this.handleChange}
                                autoComplete="off"
                            />
                        </FormGroup>
                        <FormGroup>
                            <input
                                type="password"
                                className="form-control"
                                value={this.state.password}
                                name="password"
                                id="password"
                                placeholder="Password"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <Link
                            to="/portal/password/reset"
                            className="text-right w-100 d-block"
                        >
                            {" "}
                            Forgot Password
                        </Link>
                        <Button
                            type="submit"
                            id="LoginButton"
                            onClick={this.handleClick}
                            className="p-2 login-cta"
                            color="primary"
                            block={true}
                        >
                            Login
                        </Button>
                    </Form>
                </div>
                <div className="build-version">
                    <span>Build V3.1.100919</span>
                </div>
            </App>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)

function mapDispatchToProps(dispatch) {
    return {
        setCurrentUser: data => dispatch(UserAction.setCurrentUser(data)),
        setCurrentSession: data => dispatch(UserAction.setCurrentSession(data))
    }
}

function mapStateToProps({ user }) {
    return { user }
}
