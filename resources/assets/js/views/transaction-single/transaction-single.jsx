import React, { Component } from 'react';
import App from '@components/App';
import TransactionEntity from '@components/TransactionSingle';

import './transaction-single.style.scss'

class TransactionSinglePage extends Component {

    /** The component's constructor */
    constructor(props) {
        super(props);

        this.state = {};

    }

    /**
     * Render the component's markup
     * @return {ReactElement}
     */
    render() {
        return (
            <App>
                <TransactionEntity/>
            </App>
        );
    }
}

export default TransactionSinglePage;
