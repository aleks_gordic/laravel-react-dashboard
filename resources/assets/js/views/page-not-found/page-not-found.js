import React, { Component } from 'react';
import App from '@components/App';
import NotFound from '@components/NotFound';

import './page-not-found.style.scss'

class PageNotFound extends Component {

	/** The component's constructor */
	constructor(props) {
		super(props);

		this.state = {};

	}


    handleBack = () => {
        this.props.history.goBack()
    }


    /**
	 * Render the component's markup
	 * @return {ReactElement}
	 */
	render() {
		return (
            <App>
				<NotFound/>
			</App>
			);
	}
}

export default PageNotFound;
