@extends('layouts.master')

@section('content')

    <div class="login d-flex justify-content-center">
        <form class="login-form bg-white mt-5" method="POST" action="{{ route('postLogin') }}">
            @csrf
            <h4 class="form-title text-center border-primary pb-2">Please login to Identity Portal</h4>
            <div class="position-relative form-group">
                <input type="text" class="form-control" name="username" id="username" placeholder="Username"
                       autocomplete="off" value="{{ old('username') }}" required autofocus>
            </div>
            <div class="position-relative form-group">
                <input type="password" class="form-control" name="password"
                       id="password" placeholder="Password" required></div>
            <button type="submit" id="LoginButton" class="p-2 login-cta btn btn-primary btn-block">Login</button>
        </form>
    </div>

@endsection
